package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.Tenant;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface TenantApi {

    @GET("de.pakos.lifecycle.Tenant")
    Observable<List<Tenant>> getTenantList();

    @POST("de.pakos.lifecycle.Tenant")
    Completable addTenant(@Body Tenant tenant);

    @GET("de.pakos.lifecycle.Tenant/{id}")
    Single<Tenant> getTenantById(@Path("id") String id);

    @HEAD("de.pakos.lifecycle.Tenant/{id}")
    Completable checkTenant(@Path("id") String id);

    @PUT("de.pakos.lifecycle.Tenant/{id}")
    Completable updateTenant(@Path("id") String id, @Body Tenant tenant);

    @DELETE("de.pakos.lifecycle.Tenant/{id}")
    Completable deleteTenant(@Path("id") String id);
}
