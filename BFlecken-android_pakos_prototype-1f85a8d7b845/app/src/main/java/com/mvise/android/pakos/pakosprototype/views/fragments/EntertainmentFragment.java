package com.mvise.android.pakos.pakosprototype.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.views.base.JsonBasedFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EntertainmentFragment extends JsonBasedFragment {

    private static final String ARG_JSON_PARAMS = "JSON_PARAMS";
    private static final String ARG_PROFILE_ID = "PROFILE_ID";

    private ArrayList<JsonParameter> mJsonParams = new ArrayList<>();
    private String mProfileId;

    private LinearLayout mContainerRootViews;
    private Toolbar mToolbar;

    public EntertainmentFragment() {
        // Required empty public constructor
    }

    public static EntertainmentFragment newInstance(ArrayList<JsonParameter> cp, String profileId) {
        EntertainmentFragment fragment = new EntertainmentFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_JSON_PARAMS, cp);
        args.putString(ARG_PROFILE_ID, profileId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mJsonParams = getArguments().getParcelableArrayList(ARG_JSON_PARAMS);
            mProfileId = getArguments().getString(ARG_PROFILE_ID);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_entertainment, container, false);
        findViews(rootView);

        mToolbar.setTitle("");

        if (!TextUtils.isEmpty(mProfileId)){
            generateViewsFromJson(mProfileId);
            populateUi(mProfileId);
        }

        if (getActivity() != null){
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
            Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        restoreData(savedInstanceState);

        return rootView;
    }

    private void findViews(View rootView) {
        mContainerRootViews = rootView.findViewById(R.id.container_rootviews);
        mToolbar = rootView.findViewById(R.id.toolbar_entertainment);
    }

    @Override
    public List<JsonParameter> getJsonParams() {
        return mJsonParams;
    }

    @Override
    public void setProfileId(String id) {
    }

    @Override
    public String getProfileId() {
        return mProfileId;
    }

    @Override
    public void onSaveProfile(View v, String profileId) {
        saveChanges(profileId);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_entertainment;
    }

    @Override
    public LinearLayout getRootViewsContainer() {
        return mContainerRootViews;
    }
}
