package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Address extends BasePOJO {

    @Nullable
    @SerializedName("city")
    private String city;

    @Nullable
    @SerializedName("country")
    private String country;

    @Nullable
    @SerializedName("locality")
    private String locality;

    @Nullable
    @SerializedName("region")
    private String region;

    @Nullable
    @SerializedName("street")
    private String street;

    @Nullable
    @SerializedName("street2")
    private String street2;

    @Nullable
    @SerializedName("street3")
    private String street3;

    @Nullable
    @SerializedName("postalCode")
    private String postalCode;

    @Nullable
    @SerializedName("postOfficeBoxNumber")
    private String postOfficeBoxNumber;

    @Nullable
    @SerializedName("id")
    private String id;

    public Address(){}

    public Address(@Nullable String city,
                   @Nullable String country,
                   @Nullable String locality,
                   @Nullable String region,
                   @Nullable String street,
                   @Nullable String street2,
                   @Nullable String street3,
                   @Nullable String postalCode,
                   @Nullable String postOfficeBoxNumber,
                   @Nullable String id) {
        this.city = city;
        this.country = country;
        this.locality = locality;
        this.region = region;
        this.street = street;
        this.street2 = street2;
        this.street3 = street3;
        this.postalCode = postalCode;
        this.postOfficeBoxNumber = postOfficeBoxNumber;
        this.id = id;
    }
}
