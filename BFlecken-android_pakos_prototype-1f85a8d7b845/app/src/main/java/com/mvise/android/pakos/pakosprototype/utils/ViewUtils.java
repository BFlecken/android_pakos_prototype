package com.mvise.android.pakos.pakosprototype.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class ViewUtils {
    public static int dpToPx(Context context, int dip){
        Resources r = context.getResources();
        return (int)TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dip,
                r.getDisplayMetrics()
        );
    }

    public static String getDateTimeFormated() {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.GERMAN);
        sdf.setTimeZone(tz);//set time zone.
        return sdf.format(new Date(cal.getTimeInMillis()));

    }
}
