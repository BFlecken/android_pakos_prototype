package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.FixDamageReport;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FixDamageReportApi {

    @GET("de.vda.fixDamageReport")
    Observable<List<FixDamageReport>> getFixDamageReportList();

    @POST("de.vda.fixDamageReport")
    Completable addFixDamageReport(@Body FixDamageReport fixDamageReport);

    @GET("de.vda.fixDamageReport/{id}")
    Single<FixDamageReport> getFixDamageReportById(@Path("id")String id);

}
