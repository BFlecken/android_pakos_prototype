package com.mvise.android.pakos.pakosprototype.views.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.FloatUtils;
import com.mvise.android.pakos.pakosprototype.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment.personaParams;

public abstract class JsonBasedFragment extends Fragment {

    private static final String TAG_EDITTEXT = "edittext";
    private static final String TAG_HEADLINE = "headline";
    private static final String TAG_SEEKBAR = "seekbar";
    private static final String TAG_TEXT_SEEK = "textseek";
    private static final String TAG_CHECKBOX = "checkbox";
    private static final String TAG_RADIO_BUTTON = "radiobutton";
    private static final String TAG_TEXT_VIEW_GENERAL = "text";
    private static final String TAG_BUTTON_MINUS = "minus";
    private static final String TAG_BUTTON_PLUS = "plus";
    private static final String BUNDLE_BASE_PARAM_VALUE = "BASE_PARAM_VALUE";
    private static final String BUNDLE_BASE_ROOT_PARAM_VALUE = "BASE_ROOT_PARAM_VALUE";
    private static final String BUNDLE_BASE_RADIO_PARAM_VALUE = "BASE_RADIO_PARAM_VALUE";
    private static final int ID_SEEKBAR = 103;
    private static final String TAG_MINUS_SEEKBAR = "MINUS_SEEK";
    private static final String TAG_PLUS_SEEKBAR = "PLUS_SEEK";
    private static final String TAG = JsonBasedFragment.class.getSimpleName();

    private LinearLayout mContainerRootViews;
    private LinearLayout mLinearLayoutButtons;
    private EditText mCurrentlyFocusedEditText;
    private List<View> generatedViews;
    private List<View> generatedRootViews;
    private List<RadioGroup> radioGroups;
    private List<ComponentParameter> mComponentParameters = new ArrayList<>();
    private List<String> mHeadlinesTexts = new ArrayList<>();
    private HashMap<String, List<ComponentParameter>> mPagersData = new HashMap<>();

    private boolean isChanged;
    private MainFragmentsNavigationCallback mNavigationCallback;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof MainFragmentsNavigationCallback) {
            mNavigationCallback = (MainFragmentsNavigationCallback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mNavigationCallback = null;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        for (int i = 0; i < generatedViews.size(); i++) {
            if (generatedViews.get(i) instanceof CheckBox) {
                outState.putBoolean(BUNDLE_BASE_PARAM_VALUE + i, ((CheckBox) generatedViews.get(i)).isChecked());
            } else if (generatedViews.get(i) instanceof TextView) {
                outState.putString(BUNDLE_BASE_PARAM_VALUE + i, ((TextView) generatedViews.get(i)).getText().toString());
            } else if (generatedViews.get(i) instanceof EditText) {
                outState.putString(BUNDLE_BASE_PARAM_VALUE + i, ((EditText) generatedViews.get(i)).getText().toString());
            } else if (generatedViews.get(i) instanceof RadioButton) {
                outState.putBoolean(BUNDLE_BASE_PARAM_VALUE + i, ((RadioButton) generatedViews.get(i)).isChecked());
            } else if (generatedViews.get(i) instanceof SeekBar) {
                outState.putInt(BUNDLE_BASE_PARAM_VALUE + i, ((SeekBar) generatedViews.get(i)).getProgress());
            }
        }
        for (int i = 0; i < generatedRootViews.size(); i++) {
            if (generatedRootViews.get(i) instanceof CheckBox) {
                outState.putBoolean(BUNDLE_BASE_ROOT_PARAM_VALUE + i, ((CheckBox) generatedRootViews.get(i)).isChecked());
            } else if (generatedRootViews.get(i) instanceof TextView) {
                outState.putString(BUNDLE_BASE_ROOT_PARAM_VALUE + i, ((TextView) generatedRootViews.get(i)).getText().toString());
            } else if (generatedRootViews.get(i) instanceof EditText) {
                outState.putString(BUNDLE_BASE_ROOT_PARAM_VALUE + i, ((EditText) generatedRootViews.get(i)).getText().toString());
            } else if (generatedRootViews.get(i) instanceof RadioButton) {
                outState.putBoolean(BUNDLE_BASE_ROOT_PARAM_VALUE + i, ((RadioButton) generatedRootViews.get(i)).isChecked());
            } else if (generatedRootViews.get(i) instanceof SeekBar) {
                outState.putInt(BUNDLE_BASE_ROOT_PARAM_VALUE + i, ((SeekBar) generatedRootViews.get(i)).getProgress());
            }
        }

        if (radioGroups != null) {
            int j = 0;
            for (RadioGroup group : radioGroups) {
                int count = group.getChildCount();
                for (int i = 0; i < count; i++) {
                    RadioButton button = (RadioButton) group.getChildAt(i);
                    outState.putBoolean(BUNDLE_BASE_RADIO_PARAM_VALUE + j, button.isChecked());
                    j++;
                }
            }
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public abstract void setProfileId(String id);

    public abstract String getProfileId();

    public abstract void onSaveProfile(View v, String profileId);

    public abstract int getLayoutResourceId();

    public abstract LinearLayout getRootViewsContainer();

    public abstract List<JsonParameter> getJsonParams();

    public void populateUi(String profileId) {
        List<ComponentParameter> allParams = RealmController.with().getParamValues();
        mComponentParameters = new ArrayList<>();
        for (ComponentParameter cp : allParams) {
            if (cp.getProfile() == Integer.parseInt(profileId)) {
                mComponentParameters.add(cp);
            }
        }
        if (mComponentParameters == null || mComponentParameters.size() == 0) return;

        for (int i = 0; i < generatedViews.size(); i++) {
            if (generatedViews.get(i) instanceof Button
                    && !(generatedViews.get(i) instanceof RadioButton)
                    && !(generatedViews.get(i) instanceof CheckBox)) {
                continue;
            }
            ComponentParameter param = findParamWithTag(generatedViews.get(i).getTag().toString());
            if (param == null) {
                continue;
            }
            Log.wtf(TAG, "populateUi: parameter found: "+param.getName() + " " + param.getType() + " " + param.getValue());
            if (generatedViews.get(i) instanceof CheckBox) {
                ((CheckBox) generatedViews.get(i)).setChecked(Boolean.parseBoolean(param.getValue()));
            } else if (generatedViews.get(i) instanceof EditText) {
                if ((int) Double.parseDouble(param.getValue()) == Double.parseDouble(param.getValue())) {
                    ((EditText) generatedViews.get(i)).setText(String.valueOf((int) Double.parseDouble(param.getValue())));
                } else {
                    ((EditText) generatedViews.get(i)).setText(param.getValue());
                }

            } else if (generatedViews.get(i) instanceof RadioButton) {
                ((RadioButton) generatedViews.get(i)).setChecked(Boolean.parseBoolean(param.getValue()));
            } else if (generatedViews.get(i) instanceof TextView) {
                if ((int) Double.parseDouble(param.getValue()) == Double.parseDouble(param.getValue())) {
                    ((TextView) generatedViews.get(i)).setText(String.valueOf((int) Double.parseDouble(param.getValue())));
                } else {
                    ((TextView) generatedViews.get(i)).setText(param.getValue());
                }
            } else if (generatedViews.get(i) instanceof SeekBar) {
                ((SeekBar) generatedViews.get(i)).setProgress((int) Double.parseDouble(param.getValue()));
            }
        }
    }

    public ComponentParameter findParamWithTag(String tag) {
        tag = tag.replace(TAG_TEXT_SEEK, "");
        tag = tag.replace(TAG_SEEKBAR, "");
        tag = tag.replace(TAG_RADIO_BUTTON, "");
        tag = tag.replace(TAG_EDITTEXT, "");
        tag = tag.replace(TAG_TEXT_VIEW_GENERAL, "");
        tag = tag.replace(TAG_CHECKBOX, "");

        for (int i = 0; i < mComponentParameters.size(); i++) {
            if ((mComponentParameters.get(i).getParameter()).equals(tag)) {
                return mComponentParameters.get(i);
            }
        }
        return null;
    }

    public void generateViewsFromJson(String profileId) {
        mContainerRootViews = getRootViewsContainer();

        setProfileId(profileId);
        generatedViews = new ArrayList<>();
        generatedRootViews = new ArrayList<>();
        radioGroups = new ArrayList<>();
        if (personaParams == null) {
            Toast.makeText(mContext, "JSON not available", Toast.LENGTH_SHORT).show();
        } else {
            mHeadlinesTexts = new ArrayList<>();
        }
        List<JsonParameter> paramsList = getJsonParams();
        if (paramsList != null) {
            for (JsonParameter param : paramsList) {
                if (param.isJoystick() && !TextUtils.isEmpty(param.getHeadline()) && isNewHeadline(param.getHeadline())) {
                    generateJoystickHeadline(param.getHeadline(), paramsList);
                }
                if (param.getScreen() != null && param.getScreen().equals(Constants.SCREEN_SEAT) && isNewHeadline(param.getScreen())) {
                    generateSeatHeadline(param.getScreen());
                } else if (!TextUtils.isEmpty(param.getHeadline()) && isNewHeadline(param.getHeadline()) && param.getScreen() == null) {
                    generateHeadlineTextView(param.getHeadline());
                } else if (param.getScreen() != null && param.getScreen().equals(Constants.SCREEN_ENTERTAINMENT) && isNewHeadline(param.getScreen())) {
                    generateEntertainmentHeadline(param.getScreen());
                }

                if (!param.isJoystick() && !(param.getScreen() != null && param.getScreen().equals(Constants.SCREEN_SEAT))
                        && !(param.getScreen() != null && param.getScreen().equals(Constants.SCREEN_ENTERTAINMENT))) {
                    if (param.getType().equals(Constants.PARAM_TYPE_BOOLEAN)) {
                        generateRadioButtonInGroup(param);
                    } else if (param.getType().equals(Constants.PARAM_TYPE_INT)) {
                        if (param.getRange() == null) {
                            generateEditTexts(param);
                        } else if ((int) param.getRange().getIncrement() == param.getRange().getIncrement()) {
                            generateLanechange(param);

                        } else {
                            generateButtonsStructure(param);
                        }
                    } else if (param.getType().equals(Constants.PARAM_TYPE_DOUBLE) && param.getRange() != null) {
                        generateButtonsStructure(param);
                    } else if (param.getType().equals(Constants.PARAM_TYPE_STRING)) {
                        generateStringEditTexts(param);
                    }
                } else if (param.isJoystick()) {
                    updateJoystickHashMap(param);
                }

            }
            replaceRadioGroupsWithCheckboxes();
            addRootsToLayout(profileId);
        }
    }

    private void generateEntertainmentHeadline(String screen) {
        mHeadlinesTexts.add(screen);

        LinearLayout navigationLayout = new LinearLayout(mContext);
        navigationLayout.setOrientation(LinearLayout.VERTICAL);

        Button mNavigationButton = new Button(mContext);
        mNavigationButton.setBackground(null);
        mNavigationButton.setText(screen);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        mNavigationButton.setLayoutParams(layoutParams);
        mNavigationButton.setPadding(0, ViewUtils.dpToPx(mContext, 4), 0, 0);
        Drawable favoriteDrawable = getResources().getDrawable(R.drawable.music);
        favoriteDrawable.setTint(getResources().getColor(R.color.colorPrimary));
        mNavigationButton.setCompoundDrawablesWithIntrinsicBounds(
                favoriteDrawable,
                null,
                null,
                null);
        mNavigationButton.setCompoundDrawablePadding(ViewUtils.dpToPx(mContext, 12));

        mNavigationButton.setGravity(Gravity.CENTER_VERTICAL);
        mNavigationButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        mNavigationButton.setTextColor(getResources().getColor(android.R.color.black));
        mNavigationButton.setAllCaps(false);
        mNavigationButton.setPadding(0, ViewUtils.dpToPx(mContext, 2), 0,
                ViewUtils.dpToPx(mContext, 2));
        mNavigationButton.setOnClickListener(view -> {

            ArrayList<JsonParameter> cp = new ArrayList<>();
            List<JsonParameter> paramsList1 = getJsonParams();
            String screen1 = ((Button) view).getText().toString();
            for (JsonParameter parameter : paramsList1) {
                if (!TextUtils.isEmpty(parameter.getScreen()) && parameter.getScreen().equals(screen1)) {
                    JsonParameter aux = parameter.clone();

                    aux.setScreen(null);
                    cp.add(aux);
                }
            }
            mNavigationCallback.onEntertainmentScreenLaunch(cp, getProfileId());
        });
        mNavigationButton.setTag(screen);
        generatedViews.add(mNavigationButton);

        View divider = new View(mContext);
        divider.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        LinearLayout.LayoutParams layoutParamsDivider = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewUtils.dpToPx(mContext, 1));

        divider.setLayoutParams(layoutParamsDivider);
        divider.setPadding(0,
                ViewUtils.dpToPx(mContext, 4),
                0,
                0);

        navigationLayout.addView(mNavigationButton);
        navigationLayout.addView(divider);
        mNavigationButton.setBackground(getResources().getDrawable(R.drawable.selector_list_item_profile));

        generatedRootViews.add(navigationLayout);
    }

    private void generateSeatHeadline(String screen) {
        mHeadlinesTexts.add(screen);

        LinearLayout navigationLayout = new LinearLayout(mContext);
        navigationLayout.setOrientation(LinearLayout.VERTICAL);

        Button mNavigationButton = new Button(mContext);
        mNavigationButton.setBackground(null);
        mNavigationButton.setText(screen);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        mNavigationButton.setLayoutParams(layoutParams);
        mNavigationButton.setPadding(0, ViewUtils.dpToPx(mContext, 4), 0, 0);
        Drawable favoriteDrawable = getResources().getDrawable(R.drawable.seats);
        favoriteDrawable.setTint(getResources().getColor(R.color.colorPrimary));
        mNavigationButton.setCompoundDrawablesWithIntrinsicBounds(
                favoriteDrawable,
                null,
                null,
                null);
        mNavigationButton.setCompoundDrawablePadding(ViewUtils.dpToPx(mContext, 12));
        mNavigationButton.setGravity(Gravity.CENTER_VERTICAL);
        mNavigationButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        mNavigationButton.setTextColor(getResources().getColor(android.R.color.black));
        mNavigationButton.setAllCaps(false);
        mNavigationButton.setPadding(0, ViewUtils.dpToPx(mContext, 2), 0,
                ViewUtils.dpToPx(mContext, 2));
        mNavigationButton.setOnClickListener(view -> {
            //code to get mirror / joystick screen values
            ArrayList<JsonParameter> cp = new ArrayList<>();
            List<JsonParameter> paramsList1 = getJsonParams();
            String screen1 = ((Button) view).getText().toString();
            for (JsonParameter parameter : paramsList1) {
                if (!TextUtils.isEmpty(parameter.getScreen()) && parameter.getScreen().equals(screen1)) {
                    JsonParameter aux = parameter.clone();
                    aux.setScreen(null);
                    cp.add(aux);
                }
            }
            mNavigationCallback.onSeatScreenLaunch(cp, getProfileId());
        });
        mNavigationButton.setTag(screen);
        generatedViews.add(mNavigationButton);

        View divider = new View(mContext);
        divider.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        LinearLayout.LayoutParams layoutParamsDivider = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewUtils.dpToPx(mContext, 1));
        divider.setLayoutParams(layoutParamsDivider);
        divider.setPadding(0,
                ViewUtils.dpToPx(mContext, 4),
                0,
                0);

        navigationLayout.addView(mNavigationButton);
        navigationLayout.addView(divider);
        mNavigationButton.setBackground(getResources().getDrawable(R.drawable.selector_list_item_profile));

        generatedRootViews.add(navigationLayout);
    }

    private void updateJoystickHashMap(JsonParameter param) {
        if (checkPagerGroupExists(param.getPagerGroup())) {
            for (Map.Entry<String, List<ComponentParameter>> entry : mPagersData.entrySet()) {
                if (entry.getKey().equals(param.getPagerGroup())) {
                    List<ComponentParameter> list = entry.getValue();
                    ComponentParameter componentParameter = new ComponentParameter();
                    componentParameter.setProfile(Integer.parseInt(getProfileId()));
                    componentParameter.setName(param.getName());
                    componentParameter.setType(param.getType());
                    componentParameter.setParameter(param.getParameter());

                    list.add(componentParameter);
                    entry.setValue(list);
                }
            }
        } else {
            List<ComponentParameter> list = new ArrayList<>();
            ComponentParameter componentParameter = new ComponentParameter();
            componentParameter.setProfile(Integer.parseInt(getProfileId()));
            componentParameter.setName(param.getName());
            componentParameter.setType(param.getType());
            componentParameter.setParameter(param.getParameter());

            list.add(componentParameter);
            mPagersData.put(param.getPagerGroup(), list);
        }

    }

    protected void generateJoystickHeadline(String headline, List<JsonParameter> paramsList) {
        mHeadlinesTexts.add(headline);
        LinearLayout navigationLayout = new LinearLayout(mContext);
        navigationLayout.setOrientation(LinearLayout.VERTICAL);

        Button mNavigationButton = new Button(mContext);
        mNavigationButton.setBackground(null);
        mNavigationButton.setText(headline);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mNavigationButton.setLayoutParams(layoutParams);
        mNavigationButton.setPadding(0, ViewUtils.dpToPx(mContext, 4),
                0, 0);
        Drawable favoriteDrawable = getResources().getDrawable(R.drawable.mirror);
        favoriteDrawable.setTint(getResources().getColor(R.color.colorPrimary));
        mNavigationButton.setCompoundDrawablesWithIntrinsicBounds(
                favoriteDrawable,
                null,
                null,
                null);
        mNavigationButton.setCompoundDrawablePadding(ViewUtils.dpToPx(mContext, 12));
        mNavigationButton.setGravity(Gravity.CENTER_VERTICAL);
        mNavigationButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        mNavigationButton.setTextColor(getResources().getColor(android.R.color.black));
        mNavigationButton.setAllCaps(false);
        mNavigationButton.setPadding(0, ViewUtils.dpToPx(mContext, 2), 0,
                ViewUtils.dpToPx(mContext, 2));
        mNavigationButton.setOnClickListener(view -> {
            //code to get mirror / joystick screen values
            ArrayList<JsonParameter> cp = new ArrayList<>();
            List<JsonParameter> paramsList1 = getJsonParams();
            String headline1 = ((Button) view).getText().toString();
            for (JsonParameter parameter : paramsList1) {
                if (!TextUtils.isEmpty(parameter.getHeadline()) && parameter.getHeadline().equals(headline1)) {
                    cp.add(parameter);
                }
            }
            mNavigationCallback.onJoystickScreenLaunch(cp, headline1, getProfileId());
        });
        StringBuilder tag = new StringBuilder(headline);
        for (JsonParameter parameter : paramsList) {
            if (parameter.getHeadline() != null && parameter.getHeadline().equals(headline)) {
                tag.append(parameter.getPagerGroup());
            }
        }
        mNavigationButton.setTag(tag.toString());
        generatedViews.add(mNavigationButton);

        View divider = new View(mContext);
        divider.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        LinearLayout.LayoutParams layoutParamsDivider = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewUtils.dpToPx(mContext, 1));
        divider.setLayoutParams(layoutParamsDivider);
        divider.setPadding(0,
                ViewUtils.dpToPx(mContext, 4),
                0,
                0);
        navigationLayout.addView(mNavigationButton);
        navigationLayout.addView(divider);
        mNavigationButton.setBackground(getResources().getDrawable(R.drawable.selector_list_item_profile));

        generatedRootViews.add(navigationLayout);
    }

    private boolean checkPagerGroupExists(String pagerGroup) {
        for (Map.Entry<String, List<ComponentParameter>> entry : mPagersData.entrySet()) {
            if (entry.getKey().equals(pagerGroup)) {
                return true;
            }
        }
        return false;
    }

    private void generateButtonsStructure(JsonParameter param) {
        mLinearLayoutButtons = new LinearLayout(mContext);
        mLinearLayoutButtons.setOrientation(LinearLayout.HORIZONTAL);
        mLinearLayoutButtons.setGravity(Gravity.CENTER_HORIZONTAL);
        mLinearLayoutButtons.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mLinearLayoutButtons.setPadding(0, ViewUtils.dpToPx(mContext, 12), 0, 0);

        LinearLayout linearLayoutLabel = new LinearLayout(mContext);
        LinearLayout.LayoutParams paramsLabel = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLabel.setMargins(0,
                ViewUtils.dpToPx(mContext, 12),
                0,
                0);
        linearLayoutLabel.setLayoutParams(paramsLabel);
        linearLayoutLabel.setOrientation(LinearLayout.HORIZONTAL);

        generatedRootViews.add(linearLayoutLabel);
        generatedRootViews.add(mLinearLayoutButtons);

        TextView textView = new TextView(mContext);
        textView.setText(param.getName());
        textView.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayoutLabel.addView(textView);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(getResources().getColor(R.color.black));

        generateMinusButton(param);

        TextView textView1 = new TextView(mContext);
        textView1.setText(String.valueOf(param.getRange().getStart()));
        textView1.setTextColor(getResources().getColor(R.color.black));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        params.setMargins(ViewUtils.dpToPx(mContext, 40), 0, ViewUtils.dpToPx(mContext, 40), 0);
        textView1.setLayoutParams(params);
        mLinearLayoutButtons.addView(textView1);
        textView1.setTag(param.getParameter() + TAG_TEXT_VIEW_GENERAL);
        textView1.setGravity(Gravity.CENTER_VERTICAL);
        generatedViews.add(textView1);
        textView.setTextColor(getResources().getColor(R.color.black));

        generatePlusButton(param);
    }

    private void generatePlusButton(JsonParameter param) {
        ImageButton plus = new ImageButton(mContext);
        Drawable drawable = getResources().getDrawable(R.drawable.selector_blue_button);
        plus.setBackground(drawable);
        plus.setLayoutParams(new LinearLayout.LayoutParams(ViewUtils.dpToPx(mContext, 44),
                ViewUtils.dpToPx(mContext, 36)));
        mLinearLayoutButtons.addView(plus);
        plus.setTag(TAG_BUTTON_PLUS + param.getParameter() + "," + param.getRange().getMin() + ","
                + param.getRange().getMax() + "," + param.getRange().getIncrement());
        plus.setOnClickListener(v -> {
            for (View view : generatedViews) {
                if (view.hasFocus()) {
                    view.clearFocus();
                }
                String[] tagParts = v.getTag().toString().split(",");
                if (tagParts.length > 3) {
                    String tag = v.getTag().toString();
                    tag = tag.replace("," + tagParts[tagParts.length - 3] + "," + tagParts[tagParts.length - 2] + "," + tagParts[tagParts.length - 1],
                            "");
                    try {
                        double max = Double.parseDouble(tagParts[tagParts.length - 2]);
                        double increment = Double.parseDouble(tagParts[tagParts.length - 1]);
                        if (tag.replace(TAG_BUTTON_PLUS, "").equals(view.getTag().toString().replace(TAG_TEXT_VIEW_GENERAL, ""))) {
                            TextView textValue = (TextView) view;

                            double value = Double.parseDouble(textValue.getText().toString());
                            if (value < max) {
                                value = FloatUtils.addDecimals(value + "", increment + "");
                            }

                            if ((int) value == value) {
                                textValue.setTextKeepState(((int) value) + "");
                            } else {
                                textValue.setTextKeepState(String.format(Locale.ENGLISH, "%.1f", value));
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
            v.clearFocus();
            v.requestFocus();
        });
    }

    private void generateMinusButton(JsonParameter param) {
        ImageButton minus = new ImageButton(mContext);
        Drawable drawable = getResources().getDrawable(R.drawable.selector_blue_button_minus);
        minus.setBackground(drawable);
        minus.setFocusable(false);
        minus.setLayoutParams(new LinearLayout.LayoutParams(ViewUtils.dpToPx(mContext, 44),
                ViewUtils.dpToPx(mContext, 36)));
        mLinearLayoutButtons.addView(minus);
        minus.setTag(TAG_BUTTON_MINUS + param.getParameter() + "," + param.getRange().getMin() + ","
                + param.getRange().getMax() + "," + param.getRange().getIncrement());
        minus.setOnClickListener(v -> {
            for (View view : generatedViews) {
                if (view.hasFocus()) {
                    view.clearFocus();
                }
                String[] tagParts = v.getTag().toString().split(",");
                if (tagParts.length > 3) {
                    String tag = v.getTag().toString();
                    tag = tag.replace("," + tagParts[tagParts.length - 3] + "," + tagParts[tagParts.length - 2] + "," + tagParts[tagParts.length - 1], "");

                    try {
                        double min = Double.parseDouble(tagParts[tagParts.length - 3]);
                        double increment = Double.parseDouble(tagParts[tagParts.length - 1]);
                        if (tag.replace(TAG_BUTTON_MINUS, "").equals(view.getTag().toString().replace(TAG_TEXT_VIEW_GENERAL, ""))) {
                            TextView textValue = (TextView) view;
                            double value = Double.parseDouble(textValue.getText().toString());
                            if (value > min) {
                                value = FloatUtils.subtractDecimals(value + "", increment + "");
                            }
                            if ((int) value == value) {
                                textValue.setTextKeepState(((int) value) + "");
                            } else {
                                textValue.setTextKeepState(String.format(Locale.ENGLISH, "%.1f", value));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            v.clearFocus();
            v.requestFocus();
        });
    }

    private void generateLanechange(JsonParameter param) {
        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        linearLayout.setLayoutParams(params);
        linearLayout.setWeightSum(1.0f);
        generatedRootViews.add(linearLayout);

        TextView textView = new TextView(mContext);
        linearLayout.addView(textView);
        textView.setText(param.getName());
        LinearLayout.LayoutParams labelParams = new LinearLayout.LayoutParams(0,
                ViewGroup.LayoutParams.WRAP_CONTENT, 0.9f);
        labelParams.setMargins(0,
                ViewUtils.dpToPx(mContext, 12),
                0,
                0);
        textView.setLayoutParams(labelParams);
        textView.setTextColor(getResources().getColor(R.color.black));

        TextView textView1 = new TextView(mContext);
        linearLayout.addView(textView1);
        textView1.setText(String.valueOf((int) param.getRange().getStart()));
        textView1.setTag(param.getParameter() + TAG_TEXT_SEEK);
        textView1.setGravity(Gravity.CENTER_HORIZONTAL);
        LinearLayout.LayoutParams valueParams = new LinearLayout.LayoutParams(0,
                ViewGroup.LayoutParams.WRAP_CONTENT, 0.1f);
        valueParams.setMargins(0, ViewUtils.dpToPx(mContext, 10), 0, 0);
        textView1.setLayoutParams(valueParams);
        textView1.setTextColor(getResources().getColor(R.color.black));
        generatedViews.add(textView1);

        LinearLayout linearLayoutSeek = new LinearLayout(mContext);
        linearLayout.setWeightSum(1f);
        LinearLayout.LayoutParams paramsSeek = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsSeek.setMargins(0, ViewUtils.dpToPx(mContext, 12),
                0, 0);
        linearLayoutSeek.setLayoutParams(paramsSeek);
        generatedRootViews.add(linearLayoutSeek);

        FloatingActionButton minusFab = (FloatingActionButton) ((LayoutInflater) Objects.requireNonNull(mContext.getSystemService(LAYOUT_INFLATER_SERVICE)))
                .inflate(R.layout.custom_minus_fab, linearLayoutSeek, false);
        LinearLayout.LayoutParams minusLp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.1f);
        minusLp.setMargins(ViewUtils.dpToPx(mContext, 4),
                ViewUtils.dpToPx(mContext, 6),
                ViewUtils.dpToPx(mContext, 4),
                ViewUtils.dpToPx(mContext, 6));
        minusFab.setLayoutParams(minusLp);
        linearLayoutSeek.addView(minusFab);
        minusFab.setTag(param.getParameter() + TAG_MINUS_SEEKBAR);
        minusFab.setOnClickListener(v -> {
            String tag = v.getTag().toString();
            tag = tag.replace(TAG_MINUS_SEEKBAR, "");
            int val = -1;

            JsonParameter currentParam = null;
            for (JsonParameter jsonParameter : getJsonParams()) {
                if (jsonParameter.getParameter().equals(tag)) {
                    currentParam = jsonParameter;
                }
            }
            if (currentParam == null) {
                return;
            }

            for (View view : generatedViews) {
                if (view.getTag().toString().replace(TAG_TEXT_SEEK, "").equals(tag)) {
                    TextView progressText = (TextView) view;
                    val = Integer.valueOf(progressText.getText().toString()) - 1;
                    if (currentParam.getRange().getMin() > val) {
                        return;
                    }
                    progressText.setText(String.valueOf(val));
                }
            }

            if (val != -1) {
                for (View view : generatedViews) {
                    if (view.getTag().toString().replace(TAG_SEEKBAR, "").equals(tag)) {
                        SeekBar seekBar = (SeekBar) view;
                        seekBar.setProgress(val);
                    }
                }
            }

        });

        SeekBar seekBar = (SeekBar) ((LayoutInflater) Objects.requireNonNull(mContext.getSystemService(LAYOUT_INFLATER_SERVICE)))
                .inflate(R.layout.primary_seekbar, linearLayoutSeek, false);
        seekBar.setProgress((int) param.getRange().getStart());
        seekBar.setTag(param.getParameter() + TAG_SEEKBAR);
        LinearLayout.LayoutParams seekLp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.8f);
        seekBar.setId(ID_SEEKBAR);
        seekBar.setLayoutParams(seekLp);
        seekBar.setMax((int) param.getRange().getMax());
        linearLayoutSeek.addView(seekBar);
        linearLayoutSeek.setGravity(Gravity.CENTER_VERTICAL);

        FloatingActionButton plusFab = (FloatingActionButton) ((LayoutInflater) Objects.requireNonNull(mContext.getSystemService(LAYOUT_INFLATER_SERVICE)))
                .inflate(R.layout.custom_plus_fab, linearLayoutSeek, false);
        LinearLayout.LayoutParams plusLp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.1f);
        plusLp.setMargins(ViewUtils.dpToPx(mContext, 4),
                ViewUtils.dpToPx(mContext, 6),
                ViewUtils.dpToPx(mContext, 4),
                ViewUtils.dpToPx(mContext, 6));
        plusFab.setLayoutParams(plusLp);
        linearLayoutSeek.addView(plusFab);
        plusFab.setTag(param.getParameter() + TAG_PLUS_SEEKBAR);
        plusFab.setOnClickListener(v -> {
            String tag = v.getTag().toString();
            tag = tag.replace(TAG_PLUS_SEEKBAR, "");
            int val = -1;

            JsonParameter currentParam = null;
            for (JsonParameter jsonParameter : getJsonParams()) {
                if (jsonParameter.getParameter().equals(tag)) {
                    currentParam = jsonParameter;
                }
            }
            if (currentParam == null) {
                return;
            }

            for (View view : generatedViews) {
                if (view.getTag().toString().replace(TAG_TEXT_SEEK, "").equals(tag)) {
                    TextView progressText = (TextView) view;
                    val = Integer.valueOf(progressText.getText().toString()) + 1;
                    if (currentParam.getRange().getMax() < val) {
                        return;
                    }
                    progressText.setText(String.valueOf(val));
                }
            }

            if (val != -1) {
                for (View view : generatedViews) {
                    if (view.getTag().toString().replace(TAG_SEEKBAR, "").equals(tag)) {
                        SeekBar seekBar1 = (SeekBar) view;
                        seekBar1.setProgress(val);
                    }
                }
            }
        });

        generatedViews.add(seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                for (View view : generatedViews) {
                    if (view.getTag().toString().replace(TAG_TEXT_SEEK, "").equals(seekBar.getTag().toString().replace(TAG_SEEKBAR, ""))) {
                        TextView progressText = (TextView) view;
                        progressText.setText(String.valueOf(progress));
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    private void generateRadioButtonInGroup(JsonParameter param) {
        String[] parameterParts = param.getParameter().split("\\.");
        String groupName = param.getParameter();

        int start = groupName.lastIndexOf(parameterParts[parameterParts.length - 1]);
        groupName = groupName.substring(0, start);

        RadioGroup radioGroup = getRadioGroup(groupName);
        RadioButton radioButton;
        if (radioGroup != null) {
            radioButton = setupRadioButton(radioGroup, param);
            radioGroup.addView(radioButton, radioGroup.getChildCount(),
                    new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));

        } else {
            radioGroup = new RadioGroup(mContext);
            radioGroup.setLayoutParams(new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            radioGroup.setOrientation(LinearLayout.VERTICAL);
            radioGroup.setGravity(Gravity.CENTER_VERTICAL);
            radioGroup.setTag(groupName);

            radioButton = setupRadioButton(radioGroup, param);

            radioGroups.add(radioGroup);
            radioGroup.addView(radioButton, radioGroup.getChildCount(),
                    new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
            generatedRootViews.add(radioGroup);
        }
    }

    private RadioButton setupRadioButton(RadioGroup radioGroup, JsonParameter param) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, ViewUtils.dpToPx(mContext, 12), 0, 0);

        RadioButton radioButton = (RadioButton) ((LayoutInflater) Objects.requireNonNull(mContext.getSystemService(LAYOUT_INFLATER_SERVICE)))
                .inflate(R.layout.right_check_radiobutton, radioGroup, false);

        generatedViews.add(radioButton);
        radioButton.setLayoutParams(params);
        radioButton.setText(param.getName());
        radioButton.setTag(param.getParameter() + TAG_RADIO_BUTTON);
        radioButton.setGravity(Gravity.CENTER_VERTICAL);
        radioButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        radioButton.setTextColor(getResources().getColor(R.color.black));

        return radioButton;
    }

    public void addRootsToLayout(final String profileId) {
        for (View view : generatedRootViews) {
            mContainerRootViews.addView(view);
        }
        Button save = new Button(mContext);
        save.setText(R.string.action_save);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(ViewUtils.dpToPx(mContext, 16), ViewUtils.dpToPx(mContext, 20),
                ViewUtils.dpToPx(mContext, 16), ViewUtils.dpToPx(mContext, 10));
        save.setLayoutParams(params);
        save.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        save.setTextColor(getResources().getColor(android.R.color.white));
        Drawable drawable = getResources().getDrawable(R.drawable.selector_blue_button_simple);
        save.setBackground(drawable);
        mContainerRootViews.addView(save, mContainerRootViews.getChildCount());
        save.setOnClickListener(v -> onSaveProfile(v, profileId));
    }

    private RadioGroup getRadioGroup(String tag) {
        if (radioGroups == null) return null;
        for (RadioGroup radioGroup : radioGroups) {
            if (radioGroup.getTag().equals(tag)) return radioGroup;
        }
        return null;
    }

    private void replaceRadioGroupsWithCheckboxes() {
        int generatedRootViewIndex;
        for (RadioGroup radioGroup : radioGroups) {
            if (radioGroup.getChildCount() < 2) {
                generatedRootViewIndex = generatedRootViews.indexOf(radioGroup);
                for (int i = 0; i < radioGroup.getChildCount(); i++) {
                    RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i);
                    int generatedViewIndex = generatedViews.indexOf(radioButton);
                    generatedViews.remove(radioButton);

                    CheckBox checkBox = (CheckBox) ((LayoutInflater) Objects.requireNonNull(mContext.getSystemService(LAYOUT_INFLATER_SERVICE)))
                            .inflate(R.layout.right_check_checkbox, mContainerRootViews, false);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(ViewUtils.dpToPx(mContext, 0), 0,
                            0, 0);
                    checkBox.setLayoutParams(params);
                    checkBox.setText(radioButton.getText().toString());
                    String tag = radioButton.getTag().toString().replace(TAG_RADIO_BUTTON, "");
                    checkBox.setTag(tag + TAG_CHECKBOX);
                    checkBox.setGravity(Gravity.CENTER_VERTICAL);
                    checkBox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                    checkBox.setTextColor(getResources().getColor(R.color.black));
                    generatedViews.add(generatedViewIndex, checkBox);
                    generatedRootViews.add(generatedRootViewIndex, checkBox);
                }
                generatedRootViews.remove(radioGroup);
            }
        }
    }

    private void generateHeadlineTextView(String headline) {
        TextView textView = new TextView(mContext);
        textView.setText(headline);
        textView.setTextColor(getResources().getColor(R.color.colorPrimary));
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        textView.setTag(TAG_HEADLINE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0,
                ViewUtils.dpToPx(mContext, 24),
                0,
                0);
        textView.setLayoutParams(params);
        generatedRootViews.add(textView);
        mHeadlinesTexts.add(headline);
    }

    private boolean isNewHeadline(String headline) {
        for (String head : mHeadlinesTexts) {
            if (head.equals(headline)) {
                return false;
            }
        }
        return true;
    }

    private void generateEditTexts(JsonParameter param) {
        TextView textView = new TextView(mContext);
        textView.setText(param.getName());
        LinearLayout.LayoutParams paramsLabel = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLabel.setMargins(0, ViewUtils.dpToPx(mContext, 10), 0, 0);
        textView.setLayoutParams(paramsLabel);
        textView.setTextColor(getResources().getColor(R.color.black));
        generatedRootViews.add(textView);

        EditText editText = (EditText) ((LayoutInflater) Objects.requireNonNull(mContext.getSystemService(LAYOUT_INFLATER_SERVICE)))
                .inflate(R.layout.custom_color_edittext, mContainerRootViews, false);
        editText.setEnabled(true);
        editText.clearFocus();
        editText.requestFocus();
        mCurrentlyFocusedEditText = editText;
        editText.setTag(param.getParameter() + TAG_EDITTEXT);
        editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        generatedViews.add(editText);
        generatedRootViews.add(editText);
    }

    private void generateStringEditTexts(JsonParameter param) {
        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        generatedRootViews.add(linearLayout);

        TextView textView = new TextView(mContext);
        textView.setText(param.getName());
        LinearLayout.LayoutParams paramsLabel = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLabel.setMargins(0, ViewUtils.dpToPx(mContext, 10), 0, 0);
        textView.setLayoutParams(paramsLabel);
        textView.setTextColor(getResources().getColor(R.color.black));
        linearLayout.addView(textView);

        EditText editText = (EditText) ((LayoutInflater) Objects.requireNonNull(mContext.getSystemService(LAYOUT_INFLATER_SERVICE)))
                .inflate(R.layout.custom_color_edittext, linearLayout);
        editText.setEnabled(true);
        editText.clearFocus();
        editText.requestFocus();
        mCurrentlyFocusedEditText = editText;
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setTag(param.getParameter() + TAG_EDITTEXT);
        editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.addView(editText);
        generatedViews.add(editText);
    }

    public EditText getmCurrentlyFocusedEditText() {
        return mCurrentlyFocusedEditText;
    }

    public void restoreData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (generatedViews != null) {
                for (int i = 0; i < generatedViews.size(); i++) {
                    if (generatedViews.get(i) instanceof CheckBox) {
                        boolean chosen = savedInstanceState.getBoolean(BUNDLE_BASE_PARAM_VALUE + i);
                        ((CheckBox) generatedViews.get(i)).setChecked(chosen);
                    } else if (generatedViews.get(i) instanceof TextView) {
                        String text = savedInstanceState.getString(BUNDLE_BASE_PARAM_VALUE + i, "");
                        ((TextView) generatedViews.get(i)).setText(text);
                    } else if (generatedViews.get(i) instanceof EditText) {
                        String text = savedInstanceState.getString(BUNDLE_BASE_PARAM_VALUE + i, "");
                        ((EditText) generatedViews.get(i)).setText(text);
                    } else if (generatedViews.get(i) instanceof SeekBar) {
                        int progress = savedInstanceState.getInt(BUNDLE_BASE_PARAM_VALUE + i);
                        ((SeekBar) generatedViews.get(i)).setProgress(progress);
                    }
                }
            }

            if (generatedRootViews != null) {
                for (int i = 0; i < generatedRootViews.size(); i++) {
                    if (generatedRootViews.get(i) instanceof CheckBox) {
                        boolean chosen = savedInstanceState.getBoolean(BUNDLE_BASE_ROOT_PARAM_VALUE + i);
                        ((CheckBox) generatedRootViews.get(i)).setChecked(chosen);
                    } else if (generatedRootViews.get(i) instanceof TextView) {
                        String text = savedInstanceState.getString(BUNDLE_BASE_ROOT_PARAM_VALUE + i, "");
                        ((TextView) generatedRootViews.get(i)).setText(text);
                    } else if (generatedRootViews.get(i) instanceof EditText) {
                        String text = savedInstanceState.getString(BUNDLE_BASE_ROOT_PARAM_VALUE + i, "");
                        ((EditText) generatedRootViews.get(i)).setText(text);
                    } else if (generatedRootViews.get(i) instanceof SeekBar) {
                        int progress = savedInstanceState.getInt(BUNDLE_BASE_ROOT_PARAM_VALUE + i);
                        ((SeekBar) generatedRootViews.get(i)).setProgress(progress);
                    }
                }
            }

            if (radioGroups != null) {
                int j = 0;
                for (RadioGroup group : radioGroups) {
                    int count = group.getChildCount();
                    for (int i = 0; i < count; i++) {
                        RadioButton button = (RadioButton) group.getChildAt(i);
                        button.setChecked(savedInstanceState.getBoolean(BUNDLE_BASE_RADIO_PARAM_VALUE + j));
                        j++;
                    }
                }
            }

        }
    }

    public void saveChanges(String profileId) {
        if (RealmController.with().getParamValuesForProfile(Integer.parseInt(profileId)) == null ||
                RealmController.with().getParamValuesForProfile(Integer.parseInt(profileId)).size() == 0)
            isChanged = true;

        int j = 0;
        List<JsonParameter> usedParamsList = getJsonParams();
        for (int i = 0; i < usedParamsList.size() && j < generatedViews.size(); i++) {
            ComponentParameter componentParameter = new ComponentParameter();
            componentParameter.setProfile(Integer.parseInt(profileId));
            componentParameter.setName(usedParamsList.get(i).getName());
            componentParameter.setType(usedParamsList.get(i).getType());
            componentParameter.setParameter(usedParamsList.get(i).getParameter());

            JsonParameter paramList = usedParamsList.get(i);
            if (!paramList.isJoystick() && !(paramList.getScreen() != null && paramList.getScreen().equals(Constants.SCREEN_SEAT))) {
                if (paramList.getType().equals(Constants.PARAM_TYPE_STRING) && generatedViews.get(j) instanceof EditText) {
                    componentParameter.setValue(((EditText) generatedViews.get(j)).getText().toString());
                    updateComponentDbValue(componentParameter, TAG_EDITTEXT, null);
                } else if (paramList.getType().equals(Constants.PARAM_TYPE_INT) && paramList.getRange() != null &&
                        paramList.getRange().getIncrement() == (int) paramList.getRange().getIncrement()
                        && generatedViews.get(j) instanceof TextView && !(generatedViews.get(j) instanceof Button)) {
                    componentParameter.setValue(((TextView) generatedViews.get(j)).getText().toString());
                    updateComponentDbValue(componentParameter, TAG_SEEKBAR, TAG_TEXT_SEEK);
                    j++;//omit seekbar with the same value from generated views

                } else if (paramList.getType().equals(Constants.PARAM_TYPE_INT) && paramList.getRange() == null
                        && generatedViews.get(j) instanceof EditText) {
                    if (!(componentParameter.getParameter() + TAG_EDITTEXT).equals(generatedViews.get(j).getTag().toString())) {
                        continue;
                    }
                    componentParameter.setValue(((EditText) generatedViews.get(j)).getText().toString());
                    updateComponentDbValue(componentParameter, TAG_EDITTEXT, null);

                } else {
                    if (generatedViews.get(j) instanceof CheckBox) {
                        componentParameter.setValue(((CheckBox) generatedViews.get(j)).isChecked() + "");
                        updateComponentDbValue(componentParameter, TAG_CHECKBOX, null);
                    } else if (generatedViews.get(j) instanceof RadioButton) {
                        componentParameter.setValue(((RadioButton) generatedViews.get(j)).isChecked() + "");
                        updateComponentDbValue(componentParameter, TAG_RADIO_BUTTON, null);
                    } else if (generatedViews.get(j) instanceof TextView && !(generatedViews.get(j) instanceof Button)) {
                        componentParameter.setValue(((TextView) generatedViews.get(j)).getText().toString());
                        updateComponentDbValue(componentParameter, TAG_TEXT_VIEW_GENERAL, null);
                    }
                }
                j++;
            } else if (paramList.isJoystick()) {
                if (j == 0 || !generatedViews.get(j - 1).getTag().toString().contains(usedParamsList.get(i).getHeadline()) &&
                        generatedViews.get(j).getTag().toString().contains(usedParamsList.get(i).getHeadline())) {
                    j++;
                }
            } else if (paramList.getScreen() != null && paramList.getScreen().equals(Constants.SCREEN_SEAT)) {
                if (j == 0 || !generatedViews.get(j - 1).getTag().toString().contains(usedParamsList.get(i).getScreen())) {
                    j++;
                }
            }

        }
    }

    private void updateComponentDbValue(ComponentParameter componentParameter, String label,
                                        String secondLabel) {
        String old, newValue;
        if (!isChanged) {
            ComponentParameter param = RealmController.with().getComponentParameter(
                    componentParameter.getParameter(),
                    Integer.parseInt(getProfileId()));
            if (param == null) {
                isChanged = true;
            } else {
                old = param.getValue();
                newValue = componentParameter.getValue();
                if (old == null || (newValue != null && !old.equals(newValue))) isChanged = true;
            }
        }
        RealmController.with().updateComponentParameter(
                componentParameter.getName() + label,
                componentParameter.getValue(), componentParameter.getType(),
                componentParameter.getParameter(),
                Integer.parseInt(getProfileId()));
        if (secondLabel != null) {
            RealmController.with().updateComponentParameter(
                    componentParameter.getName() + secondLabel,
                    componentParameter.getValue(), componentParameter.getType(),
                    componentParameter.getParameter(),
                    Integer.parseInt(getProfileId()));
        }
    }

    public List<View> getGeneratedViews() {
        return generatedViews;
    }
}
