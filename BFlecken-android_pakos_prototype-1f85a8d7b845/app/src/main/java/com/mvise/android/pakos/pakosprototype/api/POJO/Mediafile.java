package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Mediafile extends BasePOJO {

    @NonNull
    @SerializedName("dataFormat")
    private String dataFormat;

    @NonNull
    @SerializedName("location")
    private String location;

    @NonNull
    @SerializedName("hash")
    private String hash;

    @Nullable
    @SerializedName("id")
    private String id;

    public Mediafile(@NonNull String dataFormat, @NonNull String location, @NonNull String hash) {
        this.dataFormat = dataFormat;
        this.location = location;
        this.hash = hash;
    }

    public Mediafile() {
    }

    @Nullable
    public String getId() {
        return id;
    }

    @NonNull
    public String getLocation() {
        return location;
    }

    @NonNull
    public String getHash() {
        return hash;
    }



    public static Mediafile createMediaFile(@NonNull String completePathFile) throws Exception {
        return new Mediafile(
                completePathFile.substring(completePathFile.lastIndexOf(".")),
                completePathFile,
                ""
        );

    }

}
