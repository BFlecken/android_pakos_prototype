package com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mvise.android.pakos.pakosprototype.App;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Mediafile;
import com.mvise.android.pakos.pakosprototype.api.POJO.Report;
import com.mvise.android.pakos.pakosprototype.api.blockchain.ReportApi;
import com.mvise.android.pakos.pakosprototype.utils.MediaUtils;
import com.mvise.android.pakos.pakosprototype.utils.TakePictureUtil;
import com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;
import static com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment.CAMERA_REQUEST_CODE;
import static com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment.GALLERY_REQUEST_CODE;

public class ReportCreateFragment extends Fragment {

    private TextView titleTextView;
    private TextView descriptionEditText;
    private Button uploadButton;
    private Button createReportButton;
    private Context context;
    private ImageView cancelCreate;
    private ReportPictureFragment reportPictureFragment;
    private ImageButton clearTextImageButton;
    private TakePictureUtil takePictureUtil;

    @Inject
    Retrofit retrofitInstance;

    public static ReportCreateFragment newInstance(){
        return new ReportCreateFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.report_fragment_create, container, false);
        findViews(rootView);
        reportPictureFragment = new ReportPictureFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.report_picture_list, reportPictureFragment);
        transaction.commit();
        context = getContext();

        App.getDaggerComponent().inject(this);
        cancelCreate.setOnClickListener(v -> MainNavigationActivity.getInstance().onBackPressed());
        takePictureUtil = new TakePictureUtil(this, getContext());
        uploadButton.setOnClickListener(v -> takePictureUtil.onTakePhoto());
        setClearTextVisibility(!descriptionEditText.getText().toString().isEmpty());
        clearTextImageButton.setOnClickListener(v-> descriptionEditText.setText(""));
        createReportButton.setOnClickListener(v -> createReport());
        descriptionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setClearTextVisibility(!s.toString().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        return rootView;
    }

    private void setClearTextVisibility(boolean isVisible){
        clearTextImageButton.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @SuppressLint("CheckResult")
    private void createReport(){
        try{
            String title = titleTextView.getText().toString();
            String description = descriptionEditText.getText().toString();

            if(StringUtils.isEmpty(title)){
                Toast.makeText(getContext(), R.string.report_create_title_error, Toast.LENGTH_SHORT).show();
                return;
            }

            if(StringUtils.isEmpty(description)){
                Toast.makeText(getContext(), R.string.report_create_description_error, Toast.LENGTH_SHORT).show();
                return;
            }

            List<Mediafile> listFiles = reportPictureFragment.getMediaFiles();
            Report report = listFiles.size() > 0
                    ? new Report(UUID.randomUUID().toString(),"OPEN", description, title, listFiles.get(0))
                    : new Report(UUID.randomUUID().toString(),"OPEN", description, title);
            System.out.println(report.toString());

            createReportButton.setEnabled(false);

            retrofitInstance
                    .create(ReportApi.class)
                    .addReport(report)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() ->{
                                Toast.makeText(getContext(), R.string.success, Toast.LENGTH_SHORT).show();
                                MainNavigationActivity.getInstance().onBackPressed(true);
                            },
                            throwable ->{
                                throwable.printStackTrace();
                                Toast.makeText(getContext(), R.string.fail, Toast.LENGTH_SHORT).show();
                            } );
        }
        catch (Exception ex){
            Toast.makeText(getContext(), R.string.general_error, Toast.LENGTH_SHORT).show();
        }
    }


    private void findViews(View rootView) {
        titleTextView = rootView.findViewById(R.id.report_fragment_create_title);
        uploadButton = rootView.findViewById(R.id.upload_button);
        createReportButton = rootView.findViewById(R.id.create_report_button);
        descriptionEditText = rootView.findViewById(R.id.description_edit_text);
        cancelCreate = rootView.findViewById(R.id.cancelCreate);
        clearTextImageButton = rootView.findViewById(R.id.icon_clear_description);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:{
                if (resultCode == RESULT_OK) {
                    try{
                        String path = takePictureUtil.getPicUri().getPath();
                        if(path != null){
                            Mediafile mediafile = Mediafile.createMediaFile(path);
                            reportPictureFragment.addMediafile(mediafile);
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                break;
            }
            case GALLERY_REQUEST_CODE:{
                if (resultCode == RESULT_OK) {
                    try{
                        String path = MediaUtils.getPath(context, data.getData());
                        if(path != null){
                            Mediafile mediafile = Mediafile.createMediaFile(path);
                            reportPictureFragment.addMediafile(mediafile);
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                break;
            }

        }
    }

}
