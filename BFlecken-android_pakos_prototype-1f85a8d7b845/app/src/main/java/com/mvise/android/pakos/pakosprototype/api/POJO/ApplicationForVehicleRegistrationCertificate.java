package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class ApplicationForVehicleRegistrationCertificate extends BasePOJO {

    @NonNull
    @SerializedName("vehicleDetails")
    private VehicleDetails vehicleDetails;

    @NonNull
    @SerializedName("keeper")
    private Object keeper;

    @Nullable
    @SerializedName("dvlaFleetNumber")
    private String dvlaFleetNumber;

    @Nullable
    @SerializedName("driversLicenseNumber")
    private String driversLicenseNumber;

    @SerializedName("mileage")
    private int mileage;

    @Nullable
    @SerializedName("previousPostCode")
    private String previousPostCode;

    @Nullable
    @SerializedName("transactionId")
    private String transactionId;

    @Nullable
    @SerializedName("timestamp")
    private String timestamp;
}
