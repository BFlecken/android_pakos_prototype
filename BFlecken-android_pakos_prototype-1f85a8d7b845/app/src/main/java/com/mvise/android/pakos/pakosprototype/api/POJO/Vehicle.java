package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Vehicle extends BasePOJO {

    @NonNull
    @SerializedName("vin")
    private String vin;

    @NonNull
    @SerializedName("vehicleDetails")
    private VehicleDetails  vehicleDetails;

    @NonNull
    @SerializedName("vehicleStatus")
    private String vehicleStatus;

    @Nullable
    @SerializedName("tenant")
    private Object tenant;

    @Nullable
    @SerializedName("owner")
    private Object owner;

    @Nullable
    @SerializedName("mileage")
    private int mileage;

    @Nullable
    @SerializedName("suspiciousMessage")
    private String suspiciousMessage;

    @SerializedName("HAD")
    private boolean HAD;

    @Nullable
    @SerializedName("logEntries")
    private ArrayList<VehicleTransferLogEntry> logEntries ;

    @Nullable
    @SerializedName("rentLogEntries")
    private ArrayList<VehicleRentLogEntry> rentLogEntries;

    @Nullable
    @SerializedName("reportLogEntries")
    private ArrayList<ReportLogEntry> reportLogEntries;

    @Nullable
    @SerializedName("driveLogEntries")
    private ArrayList<DriveLogEntry> driveLogEntries;

}
