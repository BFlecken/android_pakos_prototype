package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;


public class Had extends BasePOJO{

    @NonNull
    @SerializedName("tenant")
    private Object tenant;

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @Nullable
    @SerializedName("transactionId")
    private String transactionId;

    @Nullable
    @SerializedName("timestamp")
    private String timestamp;

    public Had(@NonNull Object tenant, @NonNull Object vehicle) {
        this.tenant = tenant;
        this.vehicle = vehicle;
    }
}
