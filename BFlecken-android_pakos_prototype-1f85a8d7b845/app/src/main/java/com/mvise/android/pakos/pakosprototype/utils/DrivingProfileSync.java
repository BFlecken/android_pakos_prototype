package com.mvise.android.pakos.pakosprototype.utils;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.DrivingProfile;
import com.mvise.android.pakos.pakosprototype.api.POJO.DrivingProfileRegister;
import com.mvise.android.pakos.pakosprototype.api.POJO.Parameter;
import com.mvise.android.pakos.pakosprototype.api.blockchain.DrivingProfileRegisterApi;
import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;
import com.mvise.android.pakos.pakosprototype.model.Profile;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.views.fragments.SettingsFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Retrofit;

public class DrivingProfileSync {
    // This id is only for demo, must be removed when the tenant is implemented
    private String DRIVING_PROFILE_ID = "6";

    private String PARAMETER_PATH = "parameter_path";
    private String PARAMETER_TYPE = "parameter_type";
    private String PROFILE_NAME = "name";

    private String PROFILE_DETAILS = "detail";
    private String PROFILE_ORIGINAL_NAME = "original_name";
    private String PROFILE_IMAGE_PATH = "image_path";

    private Retrofit retrofitInstance;
    private DialogInterface dialog;
    private Context context;
    private JsonParser parser;
    private List<String> userSettings;
    private SettingsFragment parent;

    /**
     *
     * @param retrofit need to make calls to api
     * @param context application need resources and where to place dialog
     */
    public  DrivingProfileSync(Retrofit retrofit, Context context, SettingsFragment parent){
        this.retrofitInstance = retrofit;
        this.context = context;
        this.parser = new JsonParser();
        this.parent = parent;
        initializeUserSettingsList();
    }

    private void initializeUserSettingsList(){
        userSettings = new ArrayList<>();

        // What keys to be saved in cloud if the is modified by user
        userSettings.add(SharedPrefsUtil.KEY_MODEL);
        userSettings.add(SharedPrefsUtil.KEY_POSITIVE_VALUE);
        userSettings.add(SharedPrefsUtil.KEY_USER_VERIFIED);
        userSettings.add(SharedPrefsUtil.KEY_COUNTER_VERIFICATION_FAILED);
        userSettings.add(SharedPrefsUtil.KEY_ADAPTION_ENABLED);
        userSettings.add(SharedPrefsUtil.KEY_PERIODIC_VERIFICATION);
        userSettings.add(SharedPrefsUtil.KEY_VERIFICATION_INTERVAL);
        userSettings.add(SharedPrefsUtil.KEY_CAMERA_IP_ADDRESS);
        userSettings.add(SharedPrefsUtil.KEY_CAMERA_PORT);
        userSettings.add(SharedPrefsUtil.KEY_MEASUREMENT_PC_IP_ADDRESS);
        userSettings.add(SharedPrefsUtil.KEY_MEASUREMENT_PC_PORT);
        userSettings.add(SharedPrefsUtil.KEY_AUTOMATIC_SYNC_TO_CLOUD);
        userSettings.add(SharedPrefsUtil.KEY_VERIFICATION);
        userSettings.add(SharedPrefsUtil.MANUAL_VERIFICATION);
        userSettings.add(SharedPrefsUtil.CAM_VERIFICATION);
        userSettings.add(SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID);
        userSettings.add(SharedPrefsUtil.KEY_CAM_URL);
        userSettings.add(SharedPrefsUtil.KEY_PERSONA_PIC);
        userSettings.add(SharedPrefsUtil.KEY_RECEIVING_PORT);
        userSettings.add(SharedPrefsUtil.KEY_LAST_ACTIVATION_SOURCE);
        userSettings.add(SharedPrefsUtil.KEY_TEMP_ACTIVATED_PROFILE);
        userSettings.add(SharedPrefsUtil.KEY_CAR_ID);
        userSettings.add(SharedPrefsUtil.KEY_MILEAGE);
        userSettings.add(SharedPrefsUtil.KEY_HAD_ACTIVE);

    }

    /**
     *
     * @param componentParameter component from application that is parse into api object
     */
    private Parameter createParameter(ComponentParameter componentParameter){
        JsonObject parameterDetails = new JsonObject();

        parameterDetails.addProperty(PARAMETER_PATH, componentParameter.getParameter());
        parameterDetails.addProperty(PARAMETER_TYPE, componentParameter.getType());

        return  new Parameter(
                componentParameter.getId() + "",
                componentParameter.getName(),
                componentParameter.getValue() + "",
                parameterDetails.toString()
        );
    }

    /**
     *
     * @param id is identifier of profile, must be unique
     * @return if the profile is the default profile
     */
    private boolean isDefaultProfile(int id){
        return id == SharedPrefsUtil.getIntegerPreference(context, SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, 1);
    }

    private DrivingProfile createDrivingProfile(Profile profile, List<ComponentParameter> componentParameters ){
        List<Parameter> parameters = new ArrayList<>();

        JsonObject profileDetails = new JsonObject();

        profileDetails.addProperty(PROFILE_NAME, profile.getName());
        profileDetails.addProperty(PROFILE_DETAILS, profile.getDetails());
        profileDetails.addProperty(PROFILE_ORIGINAL_NAME, profile.getOriginalName());
        profileDetails.addProperty(PROFILE_IMAGE_PATH, profile.getImagePath());

        for(ComponentParameter componentParameter : componentParameters){
            parameters.add(createParameter(componentParameter));
        }

        return new DrivingProfile(
                profile.getId() + "",
                profileDetails.toString(),
                isDefaultProfile(profile.getId()),
                parameters,
                "drivingProfileRegister");
    }

    /**
     *
     * @param dialog the original dialog
     */
    public void syncToCloud(DialogInterface dialog){
        prepareDialog(dialog);

        List<DrivingProfile> divingProfiles = new ArrayList<>();

        // Get application all profiles
        RealmResults<Profile> profilesList = RealmController.with().getProfiles();

        // Extract profiles from application
        for(Profile profile: profilesList){

            // Get parameters of current profile
            List<ComponentParameter>  componentList = Realm
                    .getDefaultInstance()
                    .where(ComponentParameter.class)
                    .equalTo("profile", profile.getId())
                    .findAll();
            divingProfiles.add(createDrivingProfile(profile, componentList));

        }

        JSONArray userDetails = new JSONArray();

        for(String userNameSetting: userSettings){
            JSONObject value = SharedPrefsUtil.getPreferenceValue(context, userNameSetting);
            if(value != null){
                userDetails.put(value);
            }
        }

        saveOrUpdateDrivingProfileRegister( new DrivingProfileRegister(
                "resource:de.pakos.lifecycle.Tenant#8677",
                DRIVING_PROFILE_ID,
                null,
                userDetails.toString(),
                divingProfiles
                ));

    }

    /**
     *
     * @param drivingProfileRegister The created object that needs to be updated
     */
    @SuppressLint("CheckResult")
    private void saveOrUpdateDrivingProfileRegister(DrivingProfileRegister drivingProfileRegister){

        // First check if the object already exists
        retrofitInstance
                .create(DrivingProfileRegisterApi.class)
                .checkDrivingProfileRegister(DRIVING_PROFILE_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() ->{

                    // Can update this value
                    drivingProfileRegister.setMasterProfileId(null);

                    // This mean that the data must be updated
                    retrofitInstance
                        .create(DrivingProfileRegisterApi.class)
                        .updateDrivingProfileRegister(DRIVING_PROFILE_ID, drivingProfileRegister)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() ->{

                            // Operation is done
                            closeDialog(true);
                            System.out.println("Done");
                        },
                        throwable ->{

                            // In this section will reach only if the server is down
                            closeDialog(false);
                            throwable.printStackTrace();
                        } );
                    },
                    throwable ->{

                        // This mean that the data must be inserted
                        retrofitInstance
                            .create(DrivingProfileRegisterApi.class)
                            .addDrivingProfileRegister(drivingProfileRegister)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    () ->{

                                    // Operation is done
                                    closeDialog(true);
                                    System.out.println("Done");
                                    },
                                throwable2 ->{

                                    // In this section will reach only if the server is do
                                    closeDialog(false);
                                    throwable2.printStackTrace();
                                });
                    } );

    }

    /**
     *
     * @param list of existing profiles
     * @param id what to find
     * @return boolean if the element is not in profile
     */
    private boolean isNotProfileAdded(List<Profile> list, String id){
        for(Profile item: list){
            if(item.getId() == Integer.parseInt(id)){
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param jsonObject from where to extract string
     * @param name the parameter name tas is possible to exist ant to be string
     * @return must return a String object
     */
    private String getSafeString(JsonObject jsonObject, String name){
        try {
            return jsonObject.get(name).getAsString();
        }
        catch (Exception e){
            return "";
        }
    }

    /**
     *
     * @param drivingProfile object from cloud
     * @return profile that is unique by id
     */
    private Profile createProfile(DrivingProfile drivingProfile){
        JsonElement profileDetails = parser.parse(drivingProfile.getProfileName());
        JsonObject jsonObject = profileDetails.getAsJsonObject();

        // Set default profile
        if(drivingProfile.isDefaultProfile()){
            SharedPrefsUtil.setIntegerPreference(context, SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, Integer.parseInt(drivingProfile.getDrivingProfileId()));
        }

        return new Profile(
                Integer.parseInt(drivingProfile.getDrivingProfileId()),
                getSafeString(jsonObject, PROFILE_NAME),
                getSafeString(jsonObject, PROFILE_DETAILS),
                getSafeString(jsonObject, PROFILE_ORIGINAL_NAME),
                getSafeString(jsonObject, PROFILE_IMAGE_PATH)
        );
    }

    /**
     *
     * @param parameter object from could
     * @return component parameter that represent the element from application
     */
    private ComponentParameter createComponentParameter (Parameter parameter, int profileId){
        JsonElement parameterDetails = parser.parse(parameter.getParameterInfo());
        JsonObject jsonObject = parameterDetails.getAsJsonObject();

        return new ComponentParameter(
                Integer.parseInt(parameter.getParameterId()),
                parameter.getParameterName(),
                parameter.getParameterValue(),
                getSafeString(jsonObject, PARAMETER_TYPE),
                profileId,
                getSafeString(jsonObject, PARAMETER_PATH)
        );
    }

    private void saveUserSettings(JsonElement setting){
        JsonObject jsonObject = setting.getAsJsonObject();
        String type = getSafeString(jsonObject, SharedPrefsUtil.TYPE);
        String value = getSafeString(jsonObject, SharedPrefsUtil.STRING_VALUE);
        String name = getSafeString(jsonObject, SharedPrefsUtil.NAME);

        switch (type.toLowerCase()){
            case "integer":
                SharedPrefsUtil.setIntegerPreference(context, name, Integer.parseInt(value));
                break;
            case "float":
                SharedPrefsUtil.setFloatPreference(context, name, Float.parseFloat(value));
                break;
            case "string":
                SharedPrefsUtil.setStringPreference(context, name, value);
                break;
            case "boolean":
                SharedPrefsUtil.setBooleanPreference(context, name, Boolean.valueOf(value));
                break;
        }
    }

    @SuppressLint("CheckResult")
    public void restoreFromCloud(DialogInterface dialog){
        prepareDialog(dialog);

        // Get specific id
        retrofitInstance
                .create(DrivingProfileRegisterApi.class)
                .getDrivingProfileRegisterById(DRIVING_PROFILE_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((drivingProfileRegister) -> {
                    List<Profile> profiles = new ArrayList<>();
                    List<ComponentParameter> componentParameters = new ArrayList<>();

                    JsonElement userObject = parser.parse(drivingProfileRegister.getUserSettings());
                    JsonArray userList = userObject.getAsJsonArray();
                    int length = userList.size();

                    for(int index = 0; index < length; index++){
                        saveUserSettings(userList.get(index));
                    }

                    for(DrivingProfile drivingProfile : drivingProfileRegister.getDrivingProfile()) {

                        // Add each profile if is not in the profile list
                        if (isNotProfileAdded(profiles, drivingProfile.getDrivingProfileId())) {
                            profiles.add(createProfile(drivingProfile));
                        }

                        // Add every component parameter
                        for(Parameter parameter: drivingProfile.getParameters()){
                            componentParameters.add(createComponentParameter(parameter, Integer.parseInt(drivingProfile.getDrivingProfileId())));
                        }
                    }

                    // Replace ... just delete all then restore from cloud
                    RealmController.with().replaceProfiles(profiles);
                    RealmController.with().replaceComponentParameters(componentParameters);

                    // Operation is done
                    closeDialog(true, true);
                    },
                throwable ->{

                    // In this section will reach only if the server is down (I hope)
                    closeDialog(false);
                    throwable.printStackTrace();
                } );
    }

    private void prepareDialog(DialogInterface dialog){
        dialog.cancel();

        Dialog dialogLoading  = new Dialog(context);
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setCancelable(false);
        dialogLoading.setContentView(R.layout.loading_fragment);

        ImageView gifImageView = dialogLoading.findViewById(R.id.custom_loading_imageView);

        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gifImageView);

        Glide.with(context)
                .load(R.drawable.loading_image)
                .placeholder(R.drawable.loading_image)
                .centerCrop()
                .crossFade()
                .into(imageViewTarget);
        dialogLoading.show();

        this.dialog = dialogLoading;

    }

    private void closeDialog(boolean success, boolean refreshSettings){
        if(refreshSettings && parent != null){
            parent.refreshFragment();
        }
        closeDialog(success);
    }

    private void closeDialog(boolean success){

        Toast.makeText(context, success ? R.string.success : R.string.fail, Toast.LENGTH_SHORT).show();

        if(dialog != null){
            dialog.cancel();
        }
    }
}
