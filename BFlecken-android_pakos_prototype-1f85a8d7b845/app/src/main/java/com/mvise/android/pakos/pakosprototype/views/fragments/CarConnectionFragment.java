package com.mvise.android.pakos.pakosprototype.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.FaceIDApiClient;
import com.mvise.android.pakos.pakosprototype.api.FaceIDApiInterface;
import com.mvise.android.pakos.pakosprototype.model.Model;
import com.mvise.android.pakos.pakosprototype.model.VerificationModel;
import com.mvise.android.pakos.pakosprototype.utils.NotificationUtils;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;
import com.mvise.android.pakos.pakosprototype.views.base.CarConnectionListener;
import com.mvise.android.pakos.pakosprototype.views.base.MainFragmentsNavigationCallback;
import com.mvise.android.pakos.pakosprototype.views.base.ProfilesActivationListener;

import java.util.Arrays;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarConnectionFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = CarConnectionFragment.class.getSimpleName();

    private RelativeLayout mLayoutVerificationInfo;
    private TextView mTvStatus;
    private TextView mTvScore;
    private TextView mThreshold;
    private SeekBar mThresholdSeekBar;
    private Button mFaceIdVerificationBtn;
    private Button mEnrollBtn;
    private Model mCurrentModel;
    private VerificationModel mCurrentVerificationModel;
    private float mPositiveLimit;
    private boolean isAdaptionEnabled;
    private DriverVerificationListener mListener;
    private RelativeLayout mLoadingContainer;
    private Button manualVerificationBtn;
    private Context mContext;
    private Toolbar mToolbar;
    private LinearLayout mReportsButton;
    private ProfilesActivationListener mConnectionCallback;
    private CarConnectionListener mCarConnectionListener;
    private MainFragmentsNavigationCallback mNavigationCallback;

    public CarConnectionFragment() {
        // Required empty public constructor
    }

    public static CarConnectionFragment newInstance() {
        CarConnectionFragment fragment = new CarConnectionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void setNavigationCallback(MainFragmentsNavigationCallback callback){
        mNavigationCallback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_driver_verification, container, false);
        findViews(view);

        if (getActivity() != null){
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
            Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        mFaceIdVerificationBtn.setOnClickListener(this);
        manualVerificationBtn.setOnClickListener(this);
        mEnrollBtn.setOnClickListener(this);
        mReportsButton.setOnClickListener(this);

        mThresholdSeekBar.setMax(10);
        mPositiveLimit = SharedPrefsUtil.getFloatPreference(getActivity(),
                SharedPrefsUtil.KEY_POSITIVE_VALUE, VerificationModel.DEFAULT_POSITIVE_SCORE);
        isAdaptionEnabled = SharedPrefsUtil.getBooleanPreference(getActivity(),
                SharedPrefsUtil.KEY_ADAPTION_ENABLED, false);
        mThresholdSeekBar.setProgress((int) mPositiveLimit * 10);

        mThresholdSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                mPositiveLimit = getConvertedValue(progress);
                SharedPrefsUtil.setFloatPreference(getActivity(), SharedPrefsUtil.KEY_POSITIVE_VALUE, mPositiveLimit);
                displayVerificationInfo(mCurrentVerificationModel);
            }
        });

        String modelString = SharedPrefsUtil.getStringPreference(getActivity(), SharedPrefsUtil.KEY_MODEL);
        mCurrentModel = new Gson().fromJson(modelString, Model.class);
        if (mCurrentModel != null && !TextUtils.isEmpty(mCurrentModel.getModel())) {
            mFaceIdVerificationBtn.setVisibility(View.VISIBLE);
            mEnrollBtn.setVisibility(View.INVISIBLE);
            //Start the authentication automatically
            mFaceIdVerificationBtn.performClick();
        } else {
            mFaceIdVerificationBtn.setVisibility(View.INVISIBLE);
            mEnrollBtn.setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof  DriverVerificationListener){
            mListener = (DriverVerificationListener) context;
        }
        if (context instanceof ProfilesActivationListener){
            mConnectionCallback = (ProfilesActivationListener) context;
        }
        if (context instanceof CarConnectionListener){
            mCarConnectionListener = (CarConnectionListener) context;
        }

        mContext = context;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.button_enroll:
                enrollForModel();
                break;

            case R.id.button_faceid_verification:
                verifyModel();
                break;

            case R.id.button_manual_verification:
                setUserVerified(true);
                SharedPrefsUtil.setStringPreference(mContext, SharedPrefsUtil.KEY_VERIFICATION,
                        SharedPrefsUtil.MANUAL_VERIFICATION);
                if (mConnectionCallback != null){
                    mConnectionCallback.onRefreshProfilesActivation();
                }

                if (mCarConnectionListener != null){
                    mCarConnectionListener.onSignIn();
                }

                break;
            case R.id.reportsButton:
                mNavigationCallback.onReportsScreenLaunch();
                break;

            default:
                break;
        }

    }

    private void findViews(View view) {
        mFaceIdVerificationBtn = view.findViewById(R.id.button_faceid_verification);
        manualVerificationBtn = view.findViewById(R.id.button_manual_verification);
        mLayoutVerificationInfo = view.findViewById(R.id.layout_info_verification);
        mTvStatus = view.findViewById(R.id.tv_verification_status);
        mTvScore = view.findViewById(R.id.tv_verification_score);
        mThreshold = view.findViewById(R.id.tv_verification_threshold);
        mThresholdSeekBar = view.findViewById(R.id.seekbar_verification_threshold);
        mEnrollBtn = view.findViewById(R.id.button_enroll);
        mLoadingContainer = view.findViewById(R.id.loading_container);
        mToolbar = view.findViewById(R.id.toolbar);
        mReportsButton = view.findViewById(R.id.reportsButton);
    }

    public float getConvertedValue(int intVal) {
        float floatVal;
        floatVal = (float) intVal / 10;
        return floatVal;
    }

    private void verifyModel() {
        String baseUrl = makeBaseUrl(getContext());

        if (mCurrentModel == null || mCurrentModel.getModel() == null) {
            return;
        }
        FaceIDApiInterface faceIDApiService =
                FaceIDApiClient.getClient(baseUrl).create(FaceIDApiInterface.class);

        VerificationModel verificationModelPOJO = new VerificationModel();
        verificationModelPOJO.addModel(mCurrentModel.getModel());
        verificationModelPOJO.setAdapt(isAdaptionEnabled);

        showLoadingIndicator(true);
        Call<VerificationModel> call = faceIDApiService.verifyModel(FaceIDApiClient.getApiKey(), verificationModelPOJO);
        call.enqueue(new Callback<VerificationModel>() {
            @Override
            public void onResponse(@NonNull Call<VerificationModel> call, @NonNull Response<VerificationModel> response) {
                VerificationModel modelResponse = response.body();
                Log.wtf(TAG, "face id - verify_model response: " + modelResponse);
                showLoadingIndicator(false);
                if (modelResponse != null) {
                    displayVerificationInfo(modelResponse);
                    if (mConnectionCallback != null){
                        mConnectionCallback.onRefreshProfilesActivation();
                    }

                    if (mCarConnectionListener != null){
                        mCarConnectionListener.onSignIn();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<VerificationModel> call, @NonNull Throwable error) {
                Log.wtf(TAG, error.toString());
                setUserVerified(false);
                showLoadingIndicator(false);
            }
        });

    }

    private void displayVerificationInfo(VerificationModel verificationModel) {
        if (verificationModel == null || !isAdded()) {
            return;
        }

        String error = verificationModel.getError();
        if (!TextUtils.isEmpty(error)) {
            mLayoutVerificationInfo.setVisibility(View.VISIBLE);
            mTvStatus.setText(getString(R.string.faceid_verification_status, error));
            mThresholdSeekBar.setVisibility(View.INVISIBLE);
        } else if (verificationModel.getScores() != null && verificationModel.getScores().size() > 0) {
            mCurrentVerificationModel = verificationModel;
            String score = verificationModel.getScores().get(0);
            float scoreFloat = Float.parseFloat(score);
            float toCompare = mPositiveLimit > 0 ? mPositiveLimit : VerificationModel.DEFAULT_POSITIVE_SCORE;
            if (scoreFloat < toCompare) {
                setUserVerified(false);
                mLayoutVerificationInfo.setVisibility(View.VISIBLE);
                mThresholdSeekBar.setVisibility(View.VISIBLE);
                mTvStatus.setText(getString(R.string.faceid_verification_status, getString(R.string.failed)));
                mTvScore.setText(getString(R.string.faceid_verification_score, score));
                mThreshold.setText(getString(R.string.faceid_verification_threshold, Float.toString(mPositiveLimit)));

            } else {
                setUserVerified(true);
                SharedPrefsUtil.setStringPreference(getContext(), SharedPrefsUtil.KEY_VERIFICATION,
                        SharedPrefsUtil.CAM_VERIFICATION);
            }
        }
    }

    private void enrollForModel() {
        String baseUrl;

        baseUrl = makeBaseUrl(getContext());
        FaceIDApiInterface faceIDApiService =
                FaceIDApiClient.getClient(baseUrl).create(FaceIDApiInterface.class);

        showLoadingIndicator(true);
        Call<Model> call = faceIDApiService.enroll(FaceIDApiClient.getApiKey());
        call.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(@NonNull Call<Model> call, @NonNull Response<Model> response) {
                if (getActivity() == null) return;
                Model modelResponse = response.body();
                if (modelResponse == null){
                    Toast.makeText(getContext(),
                            getString(R.string.error_retrieving_model_null), Toast.LENGTH_LONG).show();
                    return;
                }
                Log.wtf(TAG, "verify_model response: " + modelResponse.getError() + " " + modelResponse.getModel() + " " + modelResponse.getThumbnail() +
                        " " + Arrays.toString(modelResponse.getDecodedThumbnail()));
                showLoadingIndicator(false);
                if (!TextUtils.isEmpty(modelResponse.getModel())) {
                    Toast.makeText(getContext(),
                            R.string.message_success_face_model_enroll, Toast.LENGTH_LONG).show();
                    mCurrentModel = modelResponse;
                    mEnrollBtn.setVisibility(View.INVISIBLE);
                    mFaceIdVerificationBtn.setVisibility(View.VISIBLE);
                    SharedPrefsUtil.setStringPreference(getActivity(),
                            SharedPrefsUtil.KEY_MODEL,
                            new Gson().toJson(modelResponse));
                    if (mConnectionCallback != null){
                        mConnectionCallback.onRefreshProfilesActivation();
                    }

                    if (mCarConnectionListener != null){
                        mCarConnectionListener.onSignIn();
                    }

                } else if (!TextUtils.isEmpty(modelResponse.getError())) {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.error_retrieving_model)
                            + modelResponse.getError(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(@NonNull Call<Model> call, @NonNull Throwable error) {
                Log.wtf(TAG, error.toString());
                Toast.makeText(getContext(),
                        getString(R.string.error_retrieving_model) + error.toString(), Toast.LENGTH_LONG).show();

                showLoadingIndicator(false);
            }
        });

    }

    private void showLoadingIndicator(boolean show) {
        mLoadingContainer.setVisibility(show ? ProgressBar.VISIBLE : ProgressBar.GONE);
    }

    public interface DriverVerificationListener {
        void onVerificationCompleted();
    }

    private void setUserVerified(boolean verified) {
        int failedCounter;
        failedCounter =  SharedPrefsUtil.getIntegerPreference(mContext,
                SharedPrefsUtil.KEY_COUNTER_VERIFICATION_FAILED, 0);

        failedCounter = verified ? 0 : ++failedCounter;

        if (failedCounter >= VerificationModel.VERIFICATION_FAILS_LIMIT) {
            failedCounter = 0;
            if (getActivity() != null)
                NotificationUtils.notifyUserToLookIntoCamera(getActivity());
        }
        SharedPrefsUtil.setBooleanPreference(mContext,
                SharedPrefsUtil.KEY_USER_VERIFIED, verified);
        SharedPrefsUtil.setIntegerPreference(mContext,
                SharedPrefsUtil.KEY_COUNTER_VERIFICATION_FAILED, failedCounter);
        if (verified) {
            if (mListener != null) {
                mListener.onVerificationCompleted();
            }
        }
    }

    public static String makeBaseUrl(Context context) {
        String ip = SharedPrefsUtil.getStringPreference(context, SharedPrefsUtil.KEY_CAMERA_IP_ADDRESS);
        String port = SharedPrefsUtil.getStringPreference(context, SharedPrefsUtil.KEY_CAMERA_PORT);
        String url = SharedPrefsUtil.getStringPreference(context, SharedPrefsUtil.KEY_CAM_URL);
        if (!TextUtils.isEmpty(ip) && !TextUtils.isEmpty(port)) {
            return "http://"
                    + SharedPrefsUtil.getStringPreference(context, SharedPrefsUtil.KEY_CAMERA_IP_ADDRESS)
                    + ":" + SharedPrefsUtil.getStringPreference(context, SharedPrefsUtil.KEY_CAMERA_PORT)
                    + "/";
        } else if (!TextUtils.isEmpty(url)) {
            if (!url.startsWith("http://")) {
                return "http://" + url;
            }
            return url;
        } else {
            return FaceIDApiClient.BASE_URL;
        }
    }

}
