package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class BirthDetails extends BasePOJO {

    @Nullable
    @SerializedName("dateOfBirth")
    private String dateOfBirth;

    @Nullable
    @SerializedName("placeOfBirth")
    private String placeOfBirth;

    @Nullable
    @SerializedName("id")
    private String id;

}
