package com.mvise.android.pakos.pakosprototype.utils;

public class Constants {
    public static final String ID_PERSONA  = "0";
    public static final String ID_BASIC_PROFILE = "1";
    public static final String ID_SNOW_PROFILE = "2";
    public static final String ID_WORK_PROFILE  = "3";

    public static final String ID_EMPTY_PACKAGE_SIGN_IN = "999";
    public static final String ID_EMPTY_PACKAGE_SIGN_OUT = "998";

    public static final String PROFILE_ID = "id";
    public static final String PROFILE_NAME = "name";
    public static final String PROFILE_ORIGINAL_NAME = "originalName";

    public static final String COMPONENT_PARAM_PROFILE = "profile";
    public static final String COMPONENT_PARAM_PARAMETER= "parameter";
    public static final String COMPONENT_PARAM_ID = "id";

    static final String JSON_KEY_SUBPROFILE = "subprofile";
    static final String JSON_FILENAME = "pakos_driving_profile.json";
    static final String JSON_KEY_PERSONA = "persona";
    static final String JSON_KEY_PARAM_TYPE = "dataType";

    public static final String PARAM_TYPE_STRING = "string";
    public static final String PARAM_TYPE_INT = "integer";
    public static final String PARAM_TYPE_BOOLEAN = "boolean";
    public static final String PARAM_TYPE_DOUBLE  = "double";

    public static final int ACTIVATION_SOURCE_CAR = 1;
    public static final int ACTIVATION_SOURCE_USER = 2;
    public static final int ID_PROFILE_ACTIVATION_PACKAGE = 993;
    public static final int ID_REQUEST_FEEDBACK_PACKAGE = 997;
    public static final int ID_ANSWER_FEEDBACK_PACKAGE = 996;
    public static final int ID_ACTIVE_PROFILE_SELECTION_REQUEST_PACKAGE = 994;

    public static final int FEEDBACK_LANECHANGE = 1;
    public static final int FEEDBACK_OVERTAKE = 2;
    public static final int FEEDBACK_ANSWER_SLOWER = 1;
    public static final int FEEDBACK_ANSWER_KEEP = 0;
    public static final int FEEDBACK_ANSWER_FASTER = 2;

    public static final int ID_CAR_PARAMS_PACKAGE = 992;
    public static final int ID_HAD_ACTIVATION_PACKAGE = 995;
    public static final int ID_HAD_ACTIVITY_PACKAGE = 991;

    public static final int HAD_ACTIVITY_1 = 1;
    public static final int HAD_ACTIVITY_2 = 2;
    public static final int HAD_ACTIVITY_3 = 3;
    public static final int HAD_ACTIVITY_4 = 4;
    public static final int HAD_ACTIVITY_5 = 5;
    public static final int HAD_ACTIVITY_6 = 6;
    public static final int HAD_ACTIVITY_7 = 7;
    public static final int HAD_ACTIVITY_8 = 8;
    public static final int HAD_ACTIVITY_9 = 9;
    public static final int HAD_ACTIVITY_10 = 10;
    public static final int HAD_ACTIVITY_11 = 11;
    public static final int HAD_ACTIVITY_12 = 12;
    public static final int HAD_ACTIVITY_13 = 13;
    public static final int HAD_ACTIVITY_14 = 14;
    public static final int HAD_ACTIVITY_15 = 15;
    public static final int HAD_ACTIVITY_16 = 16;
    public static final int HAD_ACTIVITY_17 = 17;
    public static final int HAD_ACTIVITY_18 = 18;
    public static final int HAD_ACTIVITY_19 = 19;
    public static final int HAD_ACTIVITY_20 = 20;
    public static final int HAD_ACTIVITY_21 = 21;
    public static final int HAD_ACTIVITY_22 = 22;
    public static final int HAD_ACTIVITY_23 = 23;
    public static final int HAD_ACTIVITY_24 = 24;
    public static final int HAD_ACTIVITY_25 = 25;
    public static final int HAD_ACTIVITY_26 = 26;
    public static final int HAD_ACTIVITY_27 = 27;
    public static final int HAD_ACTIVITY_28 = 28;
    public static final int HAD_ACTIVITY_29 = 29;
    public static final int HAD_ACTIVITY_30 = 30;
    public static final int HAD_ACTIVITY_31 = 31;
    public static final int HAD_ACTIVITY_32 = 32;
    public static final int HAD_ACTIVITY_33 = 33;
    public static final int HAD_ACTIVITY_34 = 34;
    public static final int HAD_TOR = 100;

    public static final String SCREEN_SEAT = "Seat";
    public static final String SCREEN_ENTERTAINMENT = "Entertainment";

    public static final String NOTIFICATION_CHANNEL_1 = "Channel_1";
    public static final String NOTIFICATION_CHANNEL_2 = "Channel_2";

    public static final String GENDER_MALE = "MALE";
    public static final String GENDER_FEMALE = "FEMALE";
    public static final String GENDER_OTHER = "OTHER";

    public static final String VEHICLE_STATUS_ACTIVE = "ACTIVE";
    public static final String VEHICLE_STATUS_OFF_THE_ROAD = "OFF_THE_ROAD";
    public static final String VEHICLE_STATUS_IN_USE = "IN_USE";

    public static final String REPORT_STATUS_OPEN = "OPEN";
    public static final String REPORT_STATUS_IN_PROGRESS = "IN_PROGRESS";
    public static final String REPORT_STATUS_DONE = "DONE";

}
