package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class RegisterDrivingProfile extends BasePOJO{

    @NonNull
    @SerializedName("drivingProfileRegister")
    private Object drivingProfileRegister;

    @NonNull
    @SerializedName("tenant")
    private Object tenant;

    @NonNull
    @SerializedName("drivingProfile")
    private Object drivingProfile;

    @NonNull
    @SerializedName("profileId")
    private String profileId;

    @NonNull
    @SerializedName("profileName")
    private String profileName;

    @NonNull
    @SerializedName("defaultProfile")
    private boolean defaultProfile;

    @Nullable
    @SerializedName("transactionId")
    private String transactionId;

    @Nullable
    @SerializedName("timestamp")
    private String timestamp;

}
