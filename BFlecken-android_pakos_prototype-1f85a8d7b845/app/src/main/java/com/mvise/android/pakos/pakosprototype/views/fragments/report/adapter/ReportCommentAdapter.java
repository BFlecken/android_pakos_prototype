package com.mvise.android.pakos.pakosprototype.views.fragments.report.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Mediafile;
import com.mvise.android.pakos.pakosprototype.api.POJO.ReportLogEntry;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ReportCommentAdapter extends RecyclerView.Adapter<ReportCommentAdapter.ViewHolder> {

    private final List<ReportLogEntry> comments;
    private Context context;

    public ReportCommentAdapter(Context context, List<ReportLogEntry> items) {
        comments = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_comment_fragment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = comments.get(position);
        String dateString = holder.mItem.getTimestamp();
        System.out.println(holder.mItem.getTimestamp());
        try{
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            Date date = parser.parse(holder.mItem.getTimestamp());
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy, h:m a");
            dateString = formatter.format(date);
        }
        catch (Exception e){
            // Normal error
        }
        holder.dateTextView.setText(dateString);
        holder.commentTextView.setText(holder.mItem.getReportMessage());

        List<Mediafile> fileList = new ArrayList<>();
            try {
                Gson gson = new Gson();
                JSONArray list = new JSONArray( holder.mItem.getEvent());
                for(int index = 0; index < list.length(); index++){
                    JSONObject jsonObject = list.getJSONObject(index);
                    Mediafile mediafile = gson.fromJson(jsonObject.toString(), Mediafile.class);
                    fileList.add(mediafile);
                }
            }
            catch (Exception e){
                // Normal exception
            }

        if(fileList.size() > 0){
            holder.commentPicturesRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.commentPicturesRecyclerView.setAdapter(new ReportPictureRecyclerViewAdapter(fileList, true));
        }
        else{
            holder.commentAttachTitleTextView.setVisibility(View.GONE);
        }

        holder.mView.setOnClickListener((v) -> {

        });
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView dateTextView;
        public final TextView commentTextView;
        public final TextView commentAttachTitleTextView;
        public final RecyclerView commentPicturesRecyclerView;
        public ReportLogEntry mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            dateTextView = view.findViewById(R.id.tenant_date);
            commentTextView = view.findViewById(R.id.tenant_comment);
            commentAttachTitleTextView = view.findViewById(R.id.tenant_attach_title);
            commentPicturesRecyclerView = view.findViewById(R.id.tenant_comment_images);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + commentTextView.getText() + "'";
        }
    }
}
