package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class LicencePlate extends BasePOJO{

    @NonNull
    @SerializedName("owner")
    private Object owner;

    @NonNull
    @SerializedName("licencePlate")
    private String licencePlate;

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @Nullable
    @SerializedName("transactionId")
    private String transactionId;

    @Nullable
    @SerializedName("timestamp")
    private String timestamp;

}
