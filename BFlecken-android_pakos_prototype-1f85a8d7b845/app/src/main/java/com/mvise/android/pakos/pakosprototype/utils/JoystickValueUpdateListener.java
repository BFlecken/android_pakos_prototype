package com.mvise.android.pakos.pakosprototype.utils;

import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;

public interface JoystickValueUpdateListener {
    void onValueChanged(ComponentParameter value);
}
