package com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Report;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.adapter.ReportCommentAdapter;

public class ReportCommentFragment extends Fragment {

    // TODO: Customize parameter argument names
    public static final String REPORT = "REPORT";
    // TODO: Customize parameters
    Report report;


    public ReportCommentFragment() {
    }

    public static ReportCommentFragment newInstance(Report report) {
        ReportCommentFragment fragment = new ReportCommentFragment();
        Bundle args = new Bundle();
        args.putSerializable(REPORT, report);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            report = (Report) getArguments().getSerializable(REPORT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.report_comment_fragment_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new ReportCommentAdapter(context ,report.getReportLogEntries()));
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
