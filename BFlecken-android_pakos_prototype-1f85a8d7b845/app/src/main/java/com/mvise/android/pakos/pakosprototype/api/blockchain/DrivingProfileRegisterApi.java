package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.DrivingProfileRegister;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DrivingProfileRegisterApi {

    @GET("de.vda.DrivingProfileRegister")
    Observable<List<DrivingProfileRegister>> getDrivingProfileRegisterList();

    @POST("de.vda.DrivingProfileRegister")
    Completable addDrivingProfileRegister(@Body DrivingProfileRegister drivingProfileRegister);

    @GET("de.vda.DrivingProfileRegister/{id}")
    Single<DrivingProfileRegister> getDrivingProfileRegisterById(@Path("id") String id);

    @HEAD("de.vda.DrivingProfileRegister/{id}")
    Completable checkDrivingProfileRegister(@Path("id") String id);

    @PUT("de.vda.DrivingProfileRegister/{id}")
    Completable updateDrivingProfileRegister(@Path("id") String id, @Body DrivingProfileRegister drivingProfileRegister);

    @DELETE("de.vda.DrivingProfileRegister/{id}")
    Completable deleteDrivingProfileRegister(@Path("id") String id);
}
