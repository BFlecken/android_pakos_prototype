package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class LogVehicle extends BasePOJO{

    @NonNull
    @SerializedName("tenant")
    private Object tenant;

    @NonNull
    @SerializedName("mileage")
    private int mileage;

    @Nullable
    @SerializedName("specialNotes")
    private String specialNotes;

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @Nullable
    @SerializedName("transactionId")
    private String transactionId;

    @Nullable
    @SerializedName("timestamp")
    private String timestamp;


}
