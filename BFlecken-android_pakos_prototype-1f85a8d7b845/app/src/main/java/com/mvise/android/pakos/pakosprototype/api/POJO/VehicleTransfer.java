package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class VehicleTransfer extends BasePOJO {

    @NonNull
    @SerializedName("seller")
    private Object seller;

    @NonNull
    @SerializedName("buyer")
    private Object buyer;

    @Nullable
    @SerializedName("specialNotes")
    private String specialNotes;

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @Nullable
    @SerializedName("transactionId")
    private String transactionId;

    @Nullable
    @SerializedName("timestamp")
    private String timestamp;

}
