package com.mvise.android.pakos.pakosprototype.model;

import android.text.TextUtils;
import android.util.Base64;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model {

    @SerializedName("model")
    @Expose
    private String model;

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    @SerializedName("error")
    @Expose
    private String error;


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getModel() {
        return model;
    }

    public String getThumbnail(){
        return thumbnail;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public byte[] getDecodedThumbnail(){
        if(!TextUtils.isEmpty(thumbnail)) {

            return Base64.decode(thumbnail, Base64.DEFAULT);
        }
        else return null;
    }
}
