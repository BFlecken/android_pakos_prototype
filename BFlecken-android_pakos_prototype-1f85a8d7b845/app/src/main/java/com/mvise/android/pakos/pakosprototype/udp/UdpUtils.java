package com.mvise.android.pakos.pakosprototype.udp;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;
import com.mvise.android.pakos.pakosprototype.utils.TempPersistedSignIn;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.List;

public class UdpUtils {

    private UdpReceiverUtils mReceiverUtils;

    private static final String TAG = UdpUtils.class.getSimpleName();

    //-----sending-----
    public static void sendBroadcast(List<String> messagesStr, final Context context, final Activity activity) {
        StrictMode.ThreadPolicy policy = new   StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        byte[] fullMessage;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        for (String messageStr: messagesStr){
            double value;
            try {
                value = Double.parseDouble(messageStr);
            }catch (NumberFormatException e){
                continue;
            }

            messageStr = getBinFromFloat(value);
            Log.wtf(TAG,"binary representation: "+messageStr+", double value:"+value);
            for (int i=messageStr.length(); i>=8; i-=8){
                String partialStr = messageStr.substring(i-8, i);
                String hex = getHexFromBin(partialStr);
                final ByteArrayOutputStream data = new ByteArrayOutputStream();
                final DataOutputStream stream = new DataOutputStream(data);
                try {
                    stream.writeByte((byte) (Integer.parseInt(hex,16) & 0xff));
                    stream.flush();
                    byte[] bytes = data.toByteArray();
                    Log.wtf(TAG,"bytes: " + new String(bytes)+" "+bytes.length);
                    outputStream.write(bytes);
                    outputStream.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        // ---- sending the final full content
        fullMessage = outputStream.toByteArray();
        String ipAddress=getIpAddress(context);
        final int port = getPort(context);
        try {
            final DatagramSocket sendSocket = new DatagramSocket(null);
            sendSocket.setReuseAddress(true);
            sendSocket.bind(new InetSocketAddress(port));
            sendSocket.setBroadcast(true);
            final byte[] finalBytes = fullMessage;
            final String finalIpAddress = ipAddress;
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        DatagramPacket sendPacket = new DatagramPacket(finalBytes,
                                finalBytes.length, InetAddress.getByName(finalIpAddress), port);
                        sendSocket.send(sendPacket);
                        Log.wtf(TAG, "request packet sent to: "+ finalIpAddress+" ip, port: "+port);
                    } catch (final Exception e) {
                        Log.wtf(TAG,e.toString());
                        e.printStackTrace();
                        activity.runOnUiThread(() -> Toast.makeText(context,"Error: sending data over UDP failed: "+e.toString(),
                                Toast.LENGTH_SHORT).show());
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    sendSocket.close();
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, "Error: binding socket failed: "+e.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    private static int getPort(Context context) {
        String portstring = SharedPrefsUtil.getStringPreference(context,
                SharedPrefsUtil.KEY_MEASUREMENT_PC_PORT);
        if (portstring==null||portstring.length()==0) return 9000;
        else return Integer.valueOf(portstring);
    }

    static int getReceivingPort(Context context) {
        String portstring = SharedPrefsUtil.getStringPreference(context,
                SharedPrefsUtil.KEY_RECEIVING_PORT);
        if (portstring==null||portstring.length()==0) return 5939;
        else return Integer.valueOf(portstring);
    }

    private static String getBinFromFloat(double f){
        long longBits = Double.doubleToLongBits(f);
        String result = Long.toBinaryString(longBits);
        StringBuilder builder = new StringBuilder();
        for (int i = result.length(); i < 64; i++) {
            builder.append('0');
        }
        return builder.append(result).toString();
    }


    private static String getIpAddress(Context context) {
        String ipAddress = SharedPrefsUtil.getStringPreference(context,SharedPrefsUtil.KEY_MEASUREMENT_PC_IP_ADDRESS);
        if (ipAddress==null || ipAddress.equals("")) return "255.255.255.255";
        else return ipAddress;
    }

    private static String getHexFromBin(String binaryText){
        int digitNumber = 1;
        int sum = 0;
        StringBuilder resultBuilder = new StringBuilder();
        for(int i = 0; i < binaryText.length(); i++){
            if(digitNumber == 1)
                sum+=Integer.parseInt(binaryText.charAt(i) + "")*8;
            else if(digitNumber == 2)
                sum+=Integer.parseInt(binaryText.charAt(i) + "")*4;
            else if(digitNumber == 3)
                sum+=Integer.parseInt(binaryText.charAt(i) + "")*2;
            else if(digitNumber == 4 || i < binaryText.length()+1){
                sum+=Integer.parseInt(binaryText.charAt(i) + "");
                digitNumber = 0;
                if(sum < 10)
                    resultBuilder.append(sum);
                else if(sum == 10)
                    resultBuilder.append("a");
                else if(sum == 11)
                    resultBuilder.append("b");
                else if(sum == 12)
                    resultBuilder.append("c");
                else if(sum == 13)
                    resultBuilder.append("d");
                else if(sum == 14)
                    resultBuilder.append("e");
                else if(sum == 15)
                    resultBuilder.append("f");
                sum=0;
            }
            digitNumber++;
        }
        return resultBuilder.toString();
    }

    //-----receiving-----
    public void startReceivingUdp(Context context, DataReceivedListener dataReceivedListener) {
        Handler handler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    if (!TempPersistedSignIn.getInstance().isSignedIn()
                            && !SharedPrefsUtil.getBooleanPreference(context, SharedPrefsUtil.KEY_SIGNED_IN, false)) {
                        Log.wtf(TAG, "receiving");
                        if (SharedPrefsUtil.getBooleanPreference(context, SharedPrefsUtil.KEY_RECEIVED_DETAILS_AFTER_SIGNOUT, false)) {
                            stopReceivingUdp();
                            Log.wtf(TAG, "stopped receiving");
                        }
                    }
                    Bundle res = msg.getData();
                    String actualResult = res.getString(UdpReceiveAsync.BUNDLE_RESULT_RECEIVED);
                    if ((actualResult == null || actualResult.length() == 0)
                            && (SharedPrefsUtil.getBooleanPreference(context, SharedPrefsUtil.KEY_SIGNED_IN, false)
                            || TempPersistedSignIn.getInstance().isSignedIn())) {
                        Log.wtf(TAG, "receiving");
                        mReceiverUtils = new UdpReceiverUtils(this, context);
                        mReceiverUtils.execute();
                    } else if (actualResult != null && actualResult.startsWith("exception")) {
                        Log.wtf(TAG, "receiving");
                        dataReceivedListener.onDataReceived(actualResult);
                        mReceiverUtils = new UdpReceiverUtils(this, context);
                        mReceiverUtils.execute();
                    } else {
                        if (actualResult == null) {
                            Log.wtf(TAG, "receiving");
                            return;
                        }
                        dataReceivedListener.onDataReceived(actualResult);
                    }
                }
            }
        };
        mReceiverUtils = new UdpReceiverUtils(handler, context);
        mReceiverUtils.execute();
    }

    private void stopReceivingUdp() {
        if (mReceiverUtils != null){
            mReceiverUtils.stop();
        }
    }
}
