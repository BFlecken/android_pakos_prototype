package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class SendDamageReport extends BasePOJO{

    @NonNull
    @SerializedName("tenant")
    private Object tenant;

    @NonNull
    @SerializedName("report")
    private Object report;

    @Nullable
    @SerializedName("reportMessage")
    private String reportMessage;

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @Nullable
    @SerializedName("transactionId")
    private String transactionId;

    @Nullable
    @SerializedName("timestamp")
    private String timestamp;
}
