package com.mvise.android.pakos.pakosprototype.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.Car;

import java.util.List;


public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.ViewHolder> {

    private List<Car> mCarsDataset;
    private Context mContext;

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView carTitleText, carSubtitleText;
        ImageView favImage;
        View itemView;

        ViewHolder(View v) {
            super(v);
            itemView = v;
            carTitleText = v.findViewById(R.id.text_car_title);
            carSubtitleText = v.findViewById(R.id.text_car_subtitle);
            favImage = v.findViewById(R.id.image_fav);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CarsAdapter(List<Car> data, Context context) {
        mCarsDataset = data;
        mContext = context;
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_car, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.carSubtitleText.setText(mCarsDataset.get(position).getSubtitle());
        holder.carTitleText.setText(mCarsDataset.get(position).getTitle());
        Glide.with(mContext)
                .load(mCarsDataset.get(position))
                .into(holder.favImage);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (mCarsDataset == null) {
            return 0;
        }
        return mCarsDataset.size();
    }
}