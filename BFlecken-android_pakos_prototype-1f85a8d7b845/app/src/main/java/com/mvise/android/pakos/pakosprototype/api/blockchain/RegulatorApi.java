package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.Company;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RegulatorApi {

    @GET("de.pakos.lifecycle.Regulator")
    Observable<List<Company>> getRegulatorList();

    @POST("de.pakos.lifecycle.Regulator")
    Completable addRegulator(@Body Company company);

    @GET("de.pakos.lifecycle.Regulator/{id}")
    Single<Company> getRegulatorById(@Path("id") String id);

    @HEAD("de.pakos.lifecycle.Regulator/{id}")
    Completable checkRegulator(@Path("id") String id);

    @PUT("de.pakos.lifecycle.Regulator/{id}")
    Completable updateRegulator(@Path("id") String id, @Body Company company);

    @DELETE("de.pakos.lifecycle.Regulator/{id}")
    Completable deleteRegulator(@Path("id") String id);
}
