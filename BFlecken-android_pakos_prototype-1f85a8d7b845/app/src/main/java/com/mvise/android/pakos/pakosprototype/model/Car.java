package com.mvise.android.pakos.pakosprototype.model;

public class Car {
    private String title, subtitle;
    private boolean isFavorite;

    public Car() {
    }

    public Car(String title, String subtitle, boolean isFavorite) {
        this.title = title;
        this.subtitle = subtitle;
        this.isFavorite = isFavorite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}
