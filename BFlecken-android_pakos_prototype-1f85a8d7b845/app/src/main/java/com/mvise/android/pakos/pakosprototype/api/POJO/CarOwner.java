package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CarOwner extends BasePOJO {

    @NonNull
    @SerializedName("personID")
    private String personID;

    @Nullable
    @SerializedName("title")
    private String title;

    @Nullable
    @SerializedName("firstName")
    private String firstName;

    @Nullable
    @SerializedName("lastName")
    private String lastName;

    @Nullable
    @SerializedName("middleName")
    private ArrayList<String> middleName;

    @Nullable
    @SerializedName("gender")
    private String gender;

    @Nullable
    @SerializedName("nationalities")
    private ArrayList<String> nationalities ;

    @Nullable
    @SerializedName("contactDetails")
    private ContactDetails contactDetails;

    @Nullable
    @SerializedName("birthDetails")
    private BirthDetails birthDetails;

    @Nullable
    @SerializedName("id")
    private String id;

    public CarOwner(@NonNull String personId) {
        this.personID = personId;
    }

    public CarOwner(@NonNull String personID,
                  @Nullable String title,
                  @Nullable String firstName,
                  @Nullable String lastName,
                  @Nullable ArrayList<String> middleName,
                  @Nullable String gender,
                  @Nullable ArrayList<String> nationalities,
                  @Nullable ContactDetails contactDetails,
                  @Nullable BirthDetails birthDetails,
                  @Nullable String id) {
        this.personID = personID;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.gender = gender;
        this.nationalities = nationalities;
        this.contactDetails = contactDetails;
        this.birthDetails = birthDetails;
        this.id = id;
    }
}
