package com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Report;
import com.mvise.android.pakos.pakosprototype.utils.ReportUtils;
import com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity;

public class ReportDetailsFragment extends Fragment {
    private TextView TitleTextView;
    private ImageView editReportButton;
    private TextView descriptionEditText;
    private ImageView backButton;
    private EditText statusButton;
    private TextView commentTitle;
    private ReportUtils reportUtils = new ReportUtils();


    private static final String REPORT = "report";
    private Report report;

    public ReportDetailsFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static ReportDetailsFragment newInstance(Report report) {
        ReportDetailsFragment fragment = new ReportDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(REPORT, report);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            report = (Report) getArguments().getSerializable(REPORT);
        }

        // Add pictureFragment to show pictures
        ReportPictureFragment pictureFragment = new ReportPictureFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(pictureFragment.MEDIA_FILE, report.getMediafile());
        bundle.putSerializable(pictureFragment.VIEW_MODE, true);
        pictureFragment.setArguments(bundle);

        ReportCommentFragment reportCommentFragment = ReportCommentFragment.newInstance(report);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.details_pictures_list, pictureFragment);
        transaction.add(R.id.comments_section, reportCommentFragment);
        transaction.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.report_fragment_details, container, false);
        // Inflate the layout for this pictureFragment
        findViews(rootView);

        if(report != null){
            TitleTextView.setText("Report - " + report.getReportTitle());
            descriptionEditText.setText(report.getReportMessage());
        }
        backButton.setOnClickListener(v -> MainNavigationActivity.getInstance().onBackPressed());
        editReportButton.setOnClickListener(v -> MainNavigationActivity.getInstance().onReportUpdateScreenLaunch(report));

        statusButton.setText(reportUtils.getStatusList().get(report.getReportStatus()));

        if(report.getReportLogEntries() == null || report.getReportLogEntries().size() == 0){
            commentTitle.setVisibility(View.GONE);
        }

        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void findViews(View rootView) {
        TitleTextView = rootView.findViewById(R.id.text_toolbar);
        editReportButton = rootView.findViewById(R.id.edit_report_button);
        descriptionEditText = rootView.findViewById(R.id.description_edit_text);
        backButton = rootView.findViewById(R.id.back_button);
        statusButton = rootView.findViewById(R.id.change_status);
        commentTitle = rootView.findViewById(R.id.comment_title);
    }
}
