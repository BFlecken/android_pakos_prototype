package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class ExhaustEmissions extends BasePOJO{

    @SerializedName("co")
    private int co;

    @SerializedName("hc")
    private int hc;

    @SerializedName("nox")
    private int nox;

    @SerializedName("hc_plus_nox")
    private int hc_plus_nox;

    @SerializedName("particulates")
    private int particulates;

    @Nullable
    @SerializedName("id")
    private String id;

}
