package com.mvise.android.pakos.pakosprototype.jobs;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;

public class JobSchedulerUtil {

    private static boolean sInitialized;

    synchronized public static void enableCyclicJob(Context context, boolean enable) {
        if(enable) {
            scheduleModuleVerificationJob(context);
        } else {
            cancelJob(context);
        }
    }

    synchronized private static void scheduleModuleVerificationJob(final Context context) {
        if(sInitialized) {
            return;
        }
        sInitialized = true;
        long intervalInMillis = getIntervalInMillis(context);

        ComponentName serviceName = new ComponentName(context, JobSchedulerService.class);
        JobInfo jobInfo;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            jobInfo = new JobInfo.Builder(JobSchedulerService.JOB_ID_VERIFY_MODULE, serviceName)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setMinimumLatency(intervalInMillis)
                    .build();
        } else {
            jobInfo = new JobInfo.Builder(JobSchedulerService.JOB_ID_VERIFY_MODULE, serviceName)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPeriodic(intervalInMillis)
                    .build();
        }

        if(jobInfo != null) {
            scheduleJob(jobInfo, context);
        }
    }

    synchronized static void rescheduleJobForAndroidN(final Context context) {
        boolean isCyclicVerificationEnabled = SharedPrefsUtil.getBooleanPreference(context, SharedPrefsUtil.KEY_PERIODIC_VERIFICATION, false);

        if(!sInitialized || !isCyclicVerificationEnabled)
            return;

        long intervalInMillis = getIntervalInMillis(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ComponentName serviceName = new ComponentName(context, JobSchedulerService.class);
            JobInfo jobInfo = new JobInfo.Builder(JobSchedulerService.JOB_ID_VERIFY_MODULE, serviceName)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setMinimumLatency(intervalInMillis)
                    .build();

            scheduleJob(jobInfo, context);
        }
    }

    synchronized private static void scheduleJob(final JobInfo jobInfo, final Context context) {
        JobScheduler jobScheduler = (JobScheduler)
                context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler != null) {
            jobScheduler.schedule(jobInfo);
        }
    }

    synchronized private static long getIntervalInMillis(final Context context) {
        int intervalInSeconds = SharedPrefsUtil.getIntegerPreference(context, SharedPrefsUtil.KEY_VERIFICATION_INTERVAL,
                context.getResources().getInteger(R.integer.interval_default));
         return  intervalInSeconds * 1000;
    }

    synchronized private static void cancelJob(final Context context){
        sInitialized = false;
        JobScheduler jobScheduler = (JobScheduler)
                context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler != null) {
            jobScheduler.cancelAll();
        }
    }
}
