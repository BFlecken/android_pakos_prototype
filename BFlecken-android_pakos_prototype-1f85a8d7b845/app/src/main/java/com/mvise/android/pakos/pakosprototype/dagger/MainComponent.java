package com.mvise.android.pakos.pakosprototype.dagger;

import com.mvise.android.pakos.pakosprototype.views.LoginActivity;
import com.mvise.android.pakos.pakosprototype.views.fragments.SettingsFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment.ReportCreateFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment.ReportMainFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment.ReportUpdateFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetModule.class})
public interface MainComponent {
    void inject(LoginActivity loginActivity);
    void inject(ReportMainFragment reportMainFragment);
    void inject(ReportCreateFragment reportCreateFragment);
    void inject(ReportUpdateFragment reportUpdateFragment);
    void inject(SettingsFragment settingsFragment);
}
