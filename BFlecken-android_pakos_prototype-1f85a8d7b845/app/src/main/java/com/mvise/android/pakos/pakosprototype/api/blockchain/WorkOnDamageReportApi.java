package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.FixDamageReport;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface WorkOnDamageReportApi {

    @GET("de.vda.workOnDamageReport")
    Observable<List<FixDamageReport>> getWorkOnDamageReportList();

    @POST("de.vda.workOnDamageReport")
    Completable addWorkOnDamageReport(@Body FixDamageReport damageReport);

    @GET("de.vda.workOnDamageReport/{id}")
    Single<FixDamageReport> getWorkOnDamageReportById(@Path("id") String id);
}
