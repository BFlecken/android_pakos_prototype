package com.mvise.android.pakos.pakosprototype.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.utils.JoystickValueUpdateListener;
import com.mvise.android.pakos.pakosprototype.views.fragments.JoystickFragment;

import java.util.ArrayList;
import java.util.List;

public class JoystickPagerAdapter extends FragmentPagerAdapter {

    private List<JsonParameter> mParams;
    private List<String> mTitles;
    private JoystickValueUpdateListener mListener;
    private String mProfileId;
    private List<ComponentParameter> mComponentParams;

    public JoystickPagerAdapter(FragmentManager fm, List<String> titles, List<JsonParameter> parameters,
                                String profileId,
                                JoystickValueUpdateListener listener, List<ComponentParameter> componentParameters) {
        super(fm);
        mTitles = titles;
        mParams = parameters;
        mListener = listener;
        mProfileId = profileId;
        mComponentParams = componentParameters;
        notifyDataSetChanged();
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        List<JsonParameter> paramsForTitle = new ArrayList<>();
        String currentTitle = mTitles.get(position);
        for (JsonParameter parameter: mParams){
            if (parameter.getPagerGroup().equals(currentTitle)){
                paramsForTitle.add(parameter);
            }
        }
        List<ComponentParameter> currentCompParams = new ArrayList<>();
        for (JsonParameter param: paramsForTitle){
            for (ComponentParameter cp: mComponentParams){
                if (cp.getParameter().equals(param.getParameter())){
                    currentCompParams.add(cp);
                    break;
                }
            }
        }

        JoystickFragment fragment = JoystickFragment.newInstance(paramsForTitle, mProfileId, currentCompParams);
        fragment.setmListener(mListener);
        return fragment;
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        if (mTitles == null) {
            return 0;
        }
        return mTitles.size();
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return mTitles.get(position);
    }

}