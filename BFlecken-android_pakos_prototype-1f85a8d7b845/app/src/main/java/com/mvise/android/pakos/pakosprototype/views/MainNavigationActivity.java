package com.mvise.android.pakos.pakosprototype.views;

import android.app.Notification;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.adapter.ActivateProfileAdapter;
import com.mvise.android.pakos.pakosprototype.api.POJO.Report;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.model.Profile;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.service.NotificationForegroundService;
import com.mvise.android.pakos.pakosprototype.udp.UdpUtils;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.NotificationUtils;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;
import com.mvise.android.pakos.pakosprototype.utils.TempPersistedSignIn;
import com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment;
import com.mvise.android.pakos.pakosprototype.views.base.MainFragmentsNavigationCallback;
import com.mvise.android.pakos.pakosprototype.views.base.ProfilesActivationListener;
import com.mvise.android.pakos.pakosprototype.views.base.UdpCommunicationActivity;
import com.mvise.android.pakos.pakosprototype.views.fragments.CarConnectionFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.EntertainmentFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.MirrorFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.SeatFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.SettingsFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.profiles.BasicProfileFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.profiles.NewProfileFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.profiles.PersonaFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.profiles.SnowProfileFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.profiles.WorkProfileFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment.ReportCreateFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment.ReportDetailsFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment.ReportMainFragment;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment.ReportUpdateFragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment.setLcsParamIds;
import static com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment.setPersonaParams;

public class MainNavigationActivity extends UdpCommunicationActivity implements BottomNavigationView.OnNavigationItemSelectedListener, MainFragmentsNavigationCallback,
        CarConnectionFragment.DriverVerificationListener,
        ProfilesActivationListener{

    private static final String PROFILES_LIST_FRAGMENT_TAG = "PROFILES_LIST_FRAGMENT_TAG";
    private static final String BASIC_PROFILE_FRAGMENT_TAG = "BASIC_PROFILE_FRAGMENT_TAG";
    private static final String SNOW_PROFILE_FRAGMENT_TAG = "SNOW_PROFILE_FRAGMENT_TAG";
    private static final String WORK_PROFILE_FRAGMENT_TAG = "WORK_PROFILE_FRAGMENT_TAG";
    private static final String NEW_PROFILE_FRAGMENT_TAG = "NEW_PROFILE_FRAGMENT_TAG";
    private static final String PERSONA_FRAGMENT_TAG = "PERSONA_FRAGMENT_TAG";
    public static final String SETTINGS_FRAGMENT_TAG = "SETTINGS_FRAGMENT_TAG";
    private static final String MIRROR_FRAGMENT_TAG = "MIRROR_FRAGMENT_TAG";
    public static final String DRIVER_VERIFICATION_FRAGMENT_TAG = "DRIVER_VERIFICATION_FRAGMENT_TAG";
    private static final String SEAT_FRAGMENT_TAG = "SEAT_FRAGMENT_TAG";
    private static final String REPORT_FRAGMENT_TAG = "REPORT_FRAGMENT_TAG";
    private static final String REPORT_CREATE_FRAGMENT_TAG = "REPORT_CREATE_FRAGMENT_TAG";
    private static final String REPORT_DETAILS_FRAGMENT_TAG = "REPORT_DETAILS_FRAGMENT_TAG";
    private static final String REPORT_UPDATE_FRAGMENT_TAG = "REPORT_UPDATE_FRAGMENT_TAG";
    private static final String ENTERTAINMENT_FRAGMENT_TAG = "ENTERTAINMENT_FRAGMENT_TAG" ;
    private static final String TAG = MainNavigationActivity.class.getSimpleName();

    private BottomNavigationView mNavigationView;
    private RelativeLayout mFeedbackBanner;
    private TextView mFeedbackTextView;
    private RelativeLayout mBaseContainer;
    private ConstraintLayout mHadScreenView;
    private ImageView mHadCircleImageView;
    private ImageView mHadActivityImageView;

    private DrivingProfilesListFragment mDrivingProfilesListFragment;

    private List<String> fragments = new ArrayList<>();
    private String currentTagDisplayed;
    private int currentFragmentIndex;
    private boolean isProfileActivationDialogShown;
    private static MainNavigationActivity instance;

    public static MainNavigationActivity getInstance(){
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drv_list);
        instance = this;
        findViews();
        invalidateOptionsMenu();

        mFeedbackBanner.setVisibility(View.GONE);
        mNavigationView.setOnNavigationItemSelectedListener(this);

        mDrivingProfilesListFragment = new DrivingProfilesListFragment();
        mDrivingProfilesListFragment.setNavigationCallback(this);

        if (getIntent().hasExtra(NotificationUtils.EXTRA_FRAGMENT) && getIntent().getStringExtra(NotificationUtils.EXTRA_FRAGMENT).equals(DRIVER_VERIFICATION_FRAGMENT_TAG)){
            if (!fragments.contains(PROFILES_LIST_FRAGMENT_TAG)){
                fragments.clear();
                fragments.add(PROFILES_LIST_FRAGMENT_TAG);
                currentFragmentIndex = 0;
            }
            loadDriverVerification();
        }else{
            clearFragmentsStack();
            createOrShowFragment(mDrivingProfilesListFragment, PROFILES_LIST_FRAGMENT_TAG);
        }

       // sendTestPackage();
    }

    /*private void sendTestPackage() {
        List<String> testPack = new ArrayList<>();
        testPack.add("995");
        UdpUtils.sendBroadcast(testPack, this, this);
    }*/

    public void createOrShowFragment(Fragment fragment, String tag) {
        synchronized(this) {
            if (fragments.indexOf(tag) >= 0) {
                String s = fragments.get(currentFragmentIndex);
                fragments.set(fragments.size() - 1, tag);
                fragments.set(currentFragmentIndex, s);
                currentFragmentIndex = fragments.size() - 1;
                bringFragmentToTop(tag);
                return;
            }
            fragments.add(tag);
            currentFragmentIndex = fragments.size() - 1;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.frame_container, fragment, tag);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out,android.R.anim.fade_in,android.R.anim.fade_out);
            transaction.commit();
            getSupportFragmentManager().executePendingTransactions();
            bringFragmentToTop(tag);
        }
    }

    private Fragment getFragmentByTag(String tag){
        return  getSupportFragmentManager().findFragmentByTag(tag);
    }

    public void removeFragment(String tag) {
        synchronized(this) {
            if (!fragments.contains(tag)) {
                return;
            }

            Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                getSupportFragmentManager().executePendingTransactions();
            }
            fragments.remove(tag);
            if (fragments.size() > 0) {
                String s = fragments.get(fragments.size() - 1);
                currentFragmentIndex = fragments.size() - 1;
                bringFragmentToTop(s);
            }
        }
    }

    private void bringFragmentToTop(String tag) {
        currentTagDisplayed = tag;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        for (int i = 0; i < fragments.size(); i++) {
            Fragment fragment = fragmentManager.findFragmentByTag(fragments.get(i));
            if (fragment != null){
                if (fragments.get(i).compareTo(tag) == 0) {
                    fragmentTransaction.show(fragment);
                } else {
                    fragmentTransaction.hide(fragment);
                }
            }

        }
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }


    private void findViews() {
        mNavigationView = findViewById(R.id.bottom_navigation);
        mFeedbackBanner = findViewById(R.id.banner_feedback);
        mFeedbackTextView = findViewById(R.id.text_feedback);
        mBaseContainer = findViewById(R.id.container_base);
        mHadScreenView = findViewById(R.id.had_screen);
        mHadCircleImageView = findViewById(R.id.had_imageView);
        mHadActivityImageView = findViewById(R.id.had_activity_imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (currentTagDisplayed.equals(SEAT_FRAGMENT_TAG) || currentTagDisplayed.equals(ENTERTAINMENT_FRAGMENT_TAG)){
            menu.clear();
        }else{
            getMenuInflater().inflate(R.menu.driving_profiles, menu);
            boolean isSignedIn = SharedPrefsUtil.getBooleanPreference(
                    this,
                    SharedPrefsUtil.KEY_SIGNED_IN,
                    false);
            if (isSignedIn) {
                menu.findItem(R.id.sign_in).setTitle(R.string.action_sign_out);

            }else{
                menu.findItem(R.id.sign_in).setTitle(R.string.action_sign_in);
            }

        }

        return true;
    }

    public void onBackPressed(boolean reload){
        if (!TextUtils.isEmpty(currentTagDisplayed) && !currentTagDisplayed.equals(PROFILES_LIST_FRAGMENT_TAG)){
            removeFragment(currentTagDisplayed);
            switch (currentTagDisplayed){
                case REPORT_DETAILS_FRAGMENT_TAG:{

                    if(reload){
                        removeFragment(currentTagDisplayed);
                        ((ReportMainFragment) getFragmentByTag(REPORT_FRAGMENT_TAG)).loadReports();
                    }
                    break;
                }
                case REPORT_FRAGMENT_TAG:{

                    // Reload the data from fragment
                    if(reload){
                            // Remove the page that is not updated
                            ((ReportMainFragment) getFragmentByTag(REPORT_FRAGMENT_TAG)).loadReports();
                    }
                    break;
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(currentTagDisplayed) && !currentTagDisplayed.equals(PROFILES_LIST_FRAGMENT_TAG)){
            removeFragment(currentTagDisplayed);

            switch (currentTagDisplayed){
                case BASIC_PROFILE_FRAGMENT_TAG:
                case NEW_PROFILE_FRAGMENT_TAG:
                case WORK_PROFILE_FRAGMENT_TAG:
                case SNOW_PROFILE_FRAGMENT_TAG:
                    mNavigationView.setOnNavigationItemSelectedListener(null);
                    mNavigationView.setSelectedItemId(R.id.action_profiles);
                    mNavigationView.setOnNavigationItemSelectedListener(this);
                    break;
                case PROFILES_LIST_FRAGMENT_TAG:
                    mNavigationView.setOnNavigationItemSelectedListener(null);
                    clearFragmentsStack();
                    createOrShowFragment(mDrivingProfilesListFragment, PROFILES_LIST_FRAGMENT_TAG);
                    mNavigationView.setSelectedItemId(R.id.action_profiles);
                    mNavigationView.setOnNavigationItemSelectedListener(this);
                    break;
                case PERSONA_FRAGMENT_TAG:
                    if (getSupportActionBar() != null){
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    }
                    mNavigationView.setOnNavigationItemSelectedListener(null);
                    mNavigationView.setSelectedItemId(R.id.action_persona);
                    mNavigationView.setOnNavigationItemSelectedListener(this);
                    break;
                case SETTINGS_FRAGMENT_TAG:
                    if (getSupportActionBar() != null){
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    }
                    mNavigationView.setOnNavigationItemSelectedListener(null);
                    mNavigationView.setSelectedItemId(R.id.action_settings);
                    mNavigationView.setOnNavigationItemSelectedListener(this);
                    break;
                case DRIVER_VERIFICATION_FRAGMENT_TAG:
                    if (getSupportActionBar() != null){
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    }
                    mNavigationView.setOnNavigationItemSelectedListener(null);
                    mNavigationView.setSelectedItemId(R.id.action_connection);
                    mNavigationView.setOnNavigationItemSelectedListener(this);
                    break;
            }
        }else{
            boolean isSignedIn = SharedPrefsUtil.getBooleanPreference(
                    this,
                    SharedPrefsUtil.KEY_SIGNED_IN,
                    false);
            if (isSignedIn) {
                onSignOut(true);
            }else{
                super.onBackPressed();
            }
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_profiles:
                clearFragmentsStack();
                createOrShowFragment(mDrivingProfilesListFragment, PROFILES_LIST_FRAGMENT_TAG);
                return true;
            case R.id.action_persona:
                PersonaFragment personaFragment = new PersonaFragment();
                createOrShowFragment(personaFragment, PERSONA_FRAGMENT_TAG);
                return true;
            case R.id.action_connection:
                loadDriverVerification();
                return true;
            case R.id.action_settings:
                return createSettingsFragment();
        }
        return false;
    }

    public boolean createSettingsFragment(){
        SettingsFragment settingsFragment = new SettingsFragment();
        createOrShowFragment(settingsFragment, SETTINGS_FRAGMENT_TAG);
        return true;
    }

    private void clearFragmentsStack() {
        List<String> fragmentsToBeDeleted = new ArrayList<>(fragments);
        for (String tag: fragmentsToBeDeleted){
            removeFragment(tag);
        }
    }

    @Override
    public void loadDriverVerification() {
        mNavigationView.setOnNavigationItemSelectedListener(null);
        mNavigationView.setSelectedItemId(R.id.action_connection);
        mNavigationView.setOnNavigationItemSelectedListener(this);

        CarConnectionFragment driverVerificationFrag = CarConnectionFragment.newInstance();
        driverVerificationFrag.setNavigationCallback(this);
        createOrShowFragment(driverVerificationFrag, DRIVER_VERIFICATION_FRAGMENT_TAG);
    }

    @Override
    public void onProfileListItemLongClick(Profile profile) {
        if (profile!=null && (profile.getOriginalName().equals(getString(R.string.profile_basic))||profile.getOriginalName().equals(getString(R.string.profile_snow))||
                profile.getOriginalName().equals(getString(R.string.profile_work)))) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.message_delete_profile)
                .setPositiveButton(R.string.action_yes, (dialog, id) -> {
                    if (profile != null){
                        RealmController.with().deleteProfile(profile.getName());
                    }
                })
                .setNegativeButton(R.string.action_no, (dialog, id) -> {
                    // User cancelled the dialog
                });
        builder.create().show();

    }

    @Override
    public void onProfileSelected(Profile profile) {
        if (profile==null) return;
        if (profile.getOriginalName()==null|| profile.getOriginalName().length()==0){
            NewProfileFragment newProfileFragment = NewProfileFragment.newInstance(profile);
            createOrShowFragment(newProfileFragment, NEW_PROFILE_FRAGMENT_TAG);

            return;
        }

        mNavigationView.setSelectedItemId(R.id.action_profiles);

        switch (profile.getId()+"") {
            case Constants.ID_BASIC_PROFILE: {
                BasicProfileFragment basicProfileFragment = new BasicProfileFragment();
                createOrShowFragment(basicProfileFragment, BASIC_PROFILE_FRAGMENT_TAG);
                break;
            }
            case Constants.ID_WORK_PROFILE: {
                WorkProfileFragment workProfileFragment = new WorkProfileFragment();
                createOrShowFragment(workProfileFragment, WORK_PROFILE_FRAGMENT_TAG);
                break;
            }
            case Constants.ID_SNOW_PROFILE: {
                SnowProfileFragment snowProfileFragment = new SnowProfileFragment();
                createOrShowFragment(snowProfileFragment, SNOW_PROFILE_FRAGMENT_TAG);
                break;
            }
            default: {
                NewProfileFragment newProfileFragment = NewProfileFragment.newInstance(profile);
                createOrShowFragment(newProfileFragment, NEW_PROFILE_FRAGMENT_TAG);
                break;
            }
        }
    }

    @Override
    public void onJoystickScreenLaunch(ArrayList<JsonParameter> parameters, String pagerGroup, String profileId) {
        MirrorFragment fragment = MirrorFragment.newInstance(parameters, pagerGroup, profileId);
        createOrShowFragment(fragment, MIRROR_FRAGMENT_TAG);
    }

    @Override
    public void onSeatScreenLaunch(ArrayList<JsonParameter> cp, String profileId) {
        SeatFragment fragment = SeatFragment.newInstance(cp, profileId);
        createOrShowFragment(fragment, SEAT_FRAGMENT_TAG);
        invalidateOptionsMenu();
    }

    @Override
    public void onReportsScreenLaunch() {
        ReportMainFragment reportMainFragment = ReportMainFragment.newInstance();
//        reportMainFragment.setNavigationCallback(this);
        createOrShowFragment(reportMainFragment, REPORT_FRAGMENT_TAG);
       // invalidateOptionsMenu();
    }

    @Override
    public void onCreateReportScreenLaunch() {
        ReportCreateFragment fragment = ReportCreateFragment.newInstance();
//        fragment.setNavigationCallback(this);
        createOrShowFragment(fragment, REPORT_CREATE_FRAGMENT_TAG);
        //invalidateOptionsMenu();
    }

    @Override
    public void onReportDetailsScreenLaunch(Report report){
        ReportDetailsFragment fragment = ReportDetailsFragment.newInstance(report);
        //fragment.setNavigationCallback(this);
        createOrShowFragment(fragment, REPORT_DETAILS_FRAGMENT_TAG);
        //invalidateOptionsMenu();
    }

    @Override
    public void onReportUpdateScreenLaunch(Report report) {
        ReportUpdateFragment fragment = ReportUpdateFragment.newInstance(report);
        //fragment.setNavigationCallback(this);
        createOrShowFragment(fragment, REPORT_UPDATE_FRAGMENT_TAG);
        //invalidateOptionsMenu();
    }

    @Override
    public void initializeParameters(List<JsonParameter> profile, List<JsonParameter> persona, Exception e) {

        setPersonaParams(persona);
        setLcsParamIds((ArrayList<JsonParameter>)profile);

        if (e != null) {
            Toast.makeText(this, R.string.message_json_syntax_error, Toast.LENGTH_SHORT)
                    .show();
        }

        for (JsonParameter parameter: profile){
            if (RealmController.with()
                    .getComponentParameter(parameter.getParameter(),
                            Integer.valueOf(Constants.ID_BASIC_PROFILE)) == null){
                RealmController.with()
                        .updateComponentParameter(
                                parameter.getName(),
                                "0",
                                parameter.getType(),
                                parameter.getParameter(),
                                Integer.valueOf(Constants.ID_BASIC_PROFILE));
            }
            if (RealmController.with()
                    .getComponentParameter(parameter.getParameter(), Integer.valueOf(Constants.ID_SNOW_PROFILE)) == null) {
                RealmController.with().updateComponentParameter(
                        parameter.getName(),
                        "0",
                        parameter.getType(),
                        parameter.getParameter(),
                        Integer.valueOf(Constants.ID_SNOW_PROFILE));
            }

            if (RealmController.with()
                    .getComponentParameter(parameter.getParameter(), Integer.valueOf(Constants.ID_WORK_PROFILE)) == null) {
                RealmController.with().updateComponentParameter(
                        parameter.getName(),
                        "0",
                        parameter.getType(),
                        parameter.getParameter(),
                        Integer.valueOf(Constants.ID_WORK_PROFILE));
            }
        }

        if (persona != null){
            for (JsonParameter parameter: persona){
                if (RealmController.with()
                        .getComponentParameter(parameter.getParameter(), Integer.valueOf(Constants.ID_PERSONA)) == null) {
                    Log.wtf(TAG,"initializeParameters for null persona");
                    RealmController.with().updateComponentParameter(
                            parameter.getName(),
                            "0",
                            parameter.getType(),
                            parameter.getParameter(),
                            Integer.valueOf(Constants.ID_PERSONA));
                }
            }
        }
    }

    @Override
    public void onChangedParamsReceived(String id) {
        BaseProfileFragment fragment;
        if (currentTagDisplayed.equals(BASIC_PROFILE_FRAGMENT_TAG) && id.equals(Constants.ID_BASIC_PROFILE)
            || currentTagDisplayed.equals(SNOW_PROFILE_FRAGMENT_TAG) && id.equals(Constants.ID_SNOW_PROFILE)
            || currentTagDisplayed.equals(WORK_PROFILE_FRAGMENT_TAG) && id.equals(Constants.ID_WORK_PROFILE)
            || currentTagDisplayed.equals(PERSONA_FRAGMENT_TAG) && id.equals(Constants.ID_PERSONA)){
            fragment = (BaseProfileFragment)getSupportFragmentManager().findFragmentByTag(currentTagDisplayed);
            if (fragment != null){
                fragment.populateUi(id);
            }

        }
    }

    @Override
    public void onFeedbackRequested(int feedbackParameter) {
        mFeedbackBanner.setVisibility(View.VISIBLE);
        switch (feedbackParameter){
            case Constants.FEEDBACK_LANECHANGE:
                mFeedbackTextView.setText(getString(R.string.text_feedback_param, getString(R.string.lane_change)));
                break;
            case Constants.FEEDBACK_OVERTAKE:
                mFeedbackTextView.setText(getString(R.string.text_feedback_param, getString(R.string.overtake)));
                break;
        }
    }

    @Override
    public void onProfileActivationRequested() {
        if (!isProfileActivationDialogShown){
            showProfileActivationDialog();
            isProfileActivationDialogShown = true;
        }
    }

    public void onSignOut(boolean exitingApp) {
        super.showSignOutDialog(exitingApp);
    }

    @Override
    public void onVerificationCompleted() {
        onBackPressed();
        super.onVerificationSuccess();
        Toast.makeText(this, R.string.message_success_face_model_adaption,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRefreshProfilesActivation() {
        if (mDrivingProfilesListFragment != null){
            mDrivingProfilesListFragment.updateProfilesList();
        }
    }

    public void onTakePhoto(View view) {
        switch (currentTagDisplayed){
            case BASIC_PROFILE_FRAGMENT_TAG:
            case SNOW_PROFILE_FRAGMENT_TAG:
            case WORK_PROFILE_FRAGMENT_TAG:
            case NEW_PROFILE_FRAGMENT_TAG:
            case PERSONA_FRAGMENT_TAG:
                BaseProfileFragment fragment = ((BaseProfileFragment)getSupportFragmentManager().findFragmentByTag(currentTagDisplayed));
                if (fragment != null){
                    fragment.onTakePhoto(view);
                }
                break;
        }
    }

    @Override
    public void onSignIn() {
        SharedPrefsUtil.setBooleanPreference(this, SharedPrefsUtil.KEY_SIGNED_IN, true);
        TempPersistedSignIn.getInstance().setSignedIn(true);
        List<String> emptyMessageList = new ArrayList<>();
        emptyMessageList.add(Constants.ID_EMPTY_PACKAGE_SIGN_IN);
        Log.wtf(TAG,"sign in");
        UdpUtils.sendBroadcast(emptyMessageList, this, new MainNavigationActivity());

        new UdpUtils().startReceivingUdp(this, mDrivingProfilesListFragment);
    }

    @Override
    public void onSignOut() {
        SharedPrefsUtil.setBooleanPreference(this,
                SharedPrefsUtil.KEY_SIGNED_IN, false);
        TempPersistedSignIn.getInstance().setSignedIn(false);
        List<String> emptyMessageList = new ArrayList<>();
        emptyMessageList.add(Constants.ID_EMPTY_PACKAGE_SIGN_OUT);
        Log.wtf(TAG,"sign out");
        UdpUtils.sendBroadcast(emptyMessageList,
                this,
                this);

        SharedPrefsUtil.setBooleanPreference(this, SharedPrefsUtil.KEY_RECEIVED_DETAILS_AFTER_SIGNOUT, false);
    }

    public void onFasterFeedback(View view) {
        List<String> feedbackPackValues = new ArrayList<>();
        feedbackPackValues.add(Constants.ID_ANSWER_FEEDBACK_PACKAGE+"");
        feedbackPackValues.add(Constants.FEEDBACK_ANSWER_FASTER+"");
        UdpUtils.sendBroadcast(feedbackPackValues, this, this);
        mFeedbackBanner.setVisibility(View.GONE);
    }

    public void onSlowerFeedback(View view) {
        List<String> feedbackPackValues = new ArrayList<>();
        feedbackPackValues.add(Constants.ID_ANSWER_FEEDBACK_PACKAGE+"");
        feedbackPackValues.add(Constants.FEEDBACK_ANSWER_SLOWER+"");
        UdpUtils.sendBroadcast(feedbackPackValues, this, this);
        mFeedbackBanner.setVisibility(View.GONE);
    }

    public void onKeepFeedback(View view) {
        List<String> feedbackPackValues = new ArrayList<>();
        feedbackPackValues.add(Constants.ID_ANSWER_FEEDBACK_PACKAGE+"");
        feedbackPackValues.add(Constants.FEEDBACK_ANSWER_KEEP+"");
        UdpUtils.sendBroadcast(feedbackPackValues, this, this);
        mFeedbackBanner.setVisibility(View.GONE);
    }

    private void showProfileActivationDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String currentActiveProfile = "";
        switch (SharedPrefsUtil.getIntegerPreference(this, SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, 1)+""){
            case Constants.ID_BASIC_PROFILE:
                currentActiveProfile = getString(R.string.profile_basic);
                break;
            case Constants.ID_SNOW_PROFILE:
                currentActiveProfile = getString(R.string.profile_snow);
                break;
            case Constants.ID_WORK_PROFILE:
                currentActiveProfile = getString(R.string.profile_work);
                break;
        }
        builder.setMessage(getString(R.string.message_activated_profile, currentActiveProfile))
                .setTitle(R.string.title_driving_profile)
                .setNegativeButton(R.string.action_use, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    isProfileActivationDialogShown = false;
                    sendActivationDecision();
                })
                .setPositiveButton(R.string.action_select, (dialogInterface, i) -> {
                    showProfileSelectionDialog();
                    dialogInterface.dismiss();
                    isProfileActivationDialogShown = false;
                })
                .setOnDismissListener(dialogInterface -> isProfileActivationDialogShown = false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sendActivationDecision() {
        List<String> decisionPack = new ArrayList<>();
        decisionPack.add(Constants.ID_PROFILE_ACTIVATION_PACKAGE+"");
        int currentActiveProfile = SharedPrefsUtil.getIntegerPreference(this, SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, Integer.parseInt(Constants.ID_BASIC_PROFILE));
        decisionPack.add(Constants.ID_BASIC_PROFILE.equals(currentActiveProfile+"") ? "1" : "0");
        decisionPack.add(Constants.ID_SNOW_PROFILE.equals(currentActiveProfile+"") ? "1" : "0");
        decisionPack.add(Constants.ID_WORK_PROFILE.equals(currentActiveProfile+"") ? "1" : "0");
        UdpUtils.sendBroadcast(decisionPack, this, this);
    }

    private void showProfileSelectionDialog() {
        List<Profile> profiles = RealmController.with().getProfilesWithoutPersona();
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.dialog_select_profile, mBaseContainer, false);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this)
                .setView(convertView)
                .setTitle(R.string.title_select_profile)
                .setNegativeButton(R.string.action_cancel, (dialogInterface, i) -> {
                    sendActivationDecision();
                    dialogInterface.dismiss();
                })
                .setPositiveButton(R.string.action_ok, (dialogInterface, i) -> {
                    activateTempProfile();
                    sendActivationDecision();
                    dialogInterface.dismiss();
                });
        RecyclerView rv = convertView.findViewById(R.id.list_profiles);
        rv.setLayoutManager(new LinearLayoutManager(this));
        ActivateProfileAdapter adapter = new ActivateProfileAdapter(profiles, this);
        rv.setAdapter(adapter);
        alertDialog.show();
    }

    private void activateTempProfile(){
        int previouslyActivatedProfileId;
        previouslyActivatedProfileId = SharedPrefsUtil.getIntegerPreference(
                this,
                SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID,
                1
        );

        int tempProfileId = SharedPrefsUtil.getIntegerPreference(MainNavigationActivity.this, SharedPrefsUtil.KEY_TEMP_ACTIVATED_PROFILE, previouslyActivatedProfileId);

        if (previouslyActivatedProfileId != tempProfileId){
            SharedPrefsUtil.setIntegerPreference(this, SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, tempProfileId);
            if (currentTagDisplayed.equals(PROFILES_LIST_FRAGMENT_TAG)){
                mDrivingProfilesListFragment.updateProfilesList();
            }
        }

        SharedPrefsUtil.setIntegerPreference(this, SharedPrefsUtil.KEY_LAST_ACTIVATION_SOURCE, Constants.ACTIVATION_SOURCE_USER);
    }

    @Override
    public void onCarParamsReceived(double[] dValues) {
        SharedPrefsUtil.setStringPreference(this, SharedPrefsUtil.KEY_CAR_ID, dValues[1]+"");
        SharedPrefsUtil.setStringPreference(this, SharedPrefsUtil.KEY_MILEAGE, dValues[2]+"");
        Log.wtf(TAG,"package 993 : "+dValues[1]+", "+dValues[2]);
    }

    @Override
    public void onHadActivationChanged(double dValue) {
        Log.wtf(TAG,"package 995 : "+ dValue+" active had");
        if(SharedPrefsUtil.getBooleanPreference(this, SharedPrefsUtil.KEY_HAD_ACTIVE, false) != dValue > 0){
            SharedPrefsUtil.setBooleanPreference(this, SharedPrefsUtil.KEY_HAD_ACTIVE, dValue > 0);

            startService(new Intent(this, NotificationForegroundService.class)
                    .putExtra("foreground", true));
//            Window window = getWindow();
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(dValue > 0 ? getResources().getColor(R.color.loginGreenButton) : getResources().getColor(R.color.colorPrimaryDark));

            //mHadScreenView.setVisibility(dValue > 0 ? View.VISIBLE : View.GONE);
            //mHadCircleImageView.setImageResource(R.drawable.had_active);
            //mHadActivityImageView.setImageResource(0);
        }
    }

    @Override
    public void onHadActivityReceived(double dValue) {
        Log.wtf(TAG,"package 991 : "+ dValue+" action had received");

        switch ((int)dValue){
            case Constants.HAD_ACTIVITY_1:
                displayNotificationForHadActivity(getString(R.string.had_activity_1), R.drawable.side_activity1);
                break;
            case Constants.HAD_ACTIVITY_2:
                displayNotificationForHadActivity(getString(R.string.had_activity_2), R.drawable.side_activity2);
                break;
            case Constants.HAD_ACTIVITY_3:
                displayNotificationForHadActivity(getString(R.string.had_activity_3), R.drawable.side_activity3);
                break;
            case Constants.HAD_ACTIVITY_4:
                displayNotificationForHadActivity(getString(R.string.had_activity_4), R.drawable.side_activity4);
                break;
            case Constants.HAD_ACTIVITY_5:
                displayNotificationForHadActivity(getString(R.string.had_activity_5), R.drawable.side_activity5);
                break;
            case Constants.HAD_ACTIVITY_6:
                displayNotificationForHadActivity(getString(R.string.had_activity_6), R.drawable.side_activity6);
                break;
            case Constants.HAD_ACTIVITY_7:
                displayNotificationForHadActivity(getString(R.string.had_activity_7), R.drawable.side_activity7);
                break;
            case Constants.HAD_ACTIVITY_8:
                displayNotificationForHadActivity(getString(R.string.had_activity_8), R.drawable.side_activity8);
                break;
            case Constants.HAD_ACTIVITY_9:
                displayNotificationForHadActivity(getString(R.string.had_activity_9), R.drawable.side_activity9);
                break;
            case Constants.HAD_ACTIVITY_10:
                displayNotificationForHadActivity(getString(R.string.had_activity_10), R.drawable.side_activity10);
                break;
            case Constants.HAD_ACTIVITY_11:
                displayNotificationForHadActivity(getString(R.string.had_activity_11), R.drawable.side_activity11);
                break;
            case Constants.HAD_ACTIVITY_12:
                displayNotificationForHadActivity(getString(R.string.had_activity_12), R.drawable.side_activity12);
                break;
            case Constants.HAD_ACTIVITY_13:
                displayNotificationForHadActivity(getString(R.string.had_activity_13), R.drawable.side_activity13);
                break;
            case Constants.HAD_ACTIVITY_14:
                displayNotificationForHadActivity(getString(R.string.had_activity_14), R.drawable.side_activity14);
                break;
            case Constants.HAD_ACTIVITY_15:
                displayNotificationForHadActivity(getString(R.string.had_activity_15), R.drawable.side_activity15);
                break;
            case Constants.HAD_ACTIVITY_16:
                displayNotificationForHadActivity(getString(R.string.had_activity_16), R.drawable.side_activity16);
                break;
            case Constants.HAD_ACTIVITY_17:
                displayNotificationForHadActivity(getString(R.string.had_activity_17), R.drawable.side_activity17);
                break;
            case Constants.HAD_ACTIVITY_18:
                displayNotificationForHadActivity(getString(R.string.had_activity_18), R.drawable.side_activity18);
                break;
            case Constants.HAD_ACTIVITY_19:
                displayNotificationForHadActivity(getString(R.string.had_activity_19), R.drawable.side_activity19);
                break;
            case Constants.HAD_ACTIVITY_20:
                displayNotificationForHadActivity(getString(R.string.had_activity_20), R.drawable.side_activity20);
                break;
            case Constants.HAD_ACTIVITY_21:
                displayNotificationForHadActivity(getString(R.string.had_activity_21), R.drawable.side_activity21);
                break;
            case Constants.HAD_ACTIVITY_22:
                displayNotificationForHadActivity(getString(R.string.had_activity_22), R.drawable.side_activity22);
                break;
            case Constants.HAD_ACTIVITY_23:
                displayNotificationForHadActivity(getString(R.string.had_activity_23), R.drawable.side_activity23);
                break;
            case Constants.HAD_ACTIVITY_24:
                displayNotificationForHadActivity(getString(R.string.had_activity_24), R.drawable.side_activity24);
                break;
            case Constants.HAD_ACTIVITY_25:
                displayNotificationForHadActivity(getString(R.string.had_activity_25), R.drawable.side_activity25);
                break;
            case Constants.HAD_ACTIVITY_26:
                displayNotificationForHadActivity(getString(R.string.had_activity_26), R.drawable.side_activity26);
                break;
            case Constants.HAD_ACTIVITY_27:
                displayNotificationForHadActivity(getString(R.string.had_activity_27), R.drawable.side_activity27);
                break;
            case Constants.HAD_ACTIVITY_28:
                displayNotificationForHadActivity(getString(R.string.had_activity_28), R.drawable.side_activity28);
                break;
            case Constants.HAD_ACTIVITY_29:
                displayNotificationForHadActivity(getString(R.string.had_activity_29), R.drawable.side_activity29);
                break;
            case Constants.HAD_ACTIVITY_30:
                displayNotificationForHadActivity(getString(R.string.had_activity_30), R.drawable.side_activity30);
                break;
            case Constants.HAD_ACTIVITY_31:
                displayNotificationForHadActivity(getString(R.string.had_activity_31), R.drawable.side_activity31);
                break;
            case Constants.HAD_ACTIVITY_32:
                displayNotificationForHadActivity(getString(R.string.had_activity_32), R.drawable.side_activity32);
                break;
            case Constants.HAD_ACTIVITY_33:
                displayNotificationForHadActivity(getString(R.string.had_activity_33), R.drawable.side_activity33);
                break;
            case Constants.HAD_ACTIVITY_34:
                displayNotificationForHadActivity(getString(R.string.had_activity_34), R.drawable.side_activity34);
                break;
            case Constants.HAD_TOR:
                startService(new Intent(this, NotificationForegroundService.class)
                        .putExtra("foreground", true));
                break;
        }

    }

    public BottomNavigationView getmNavigationView() {
        return mNavigationView;
    }

    @Override
    public void onEntertainmentScreenLaunch(ArrayList<JsonParameter> cp, String profileId) {
        EntertainmentFragment fragment = EntertainmentFragment.newInstance(cp, profileId);
        createOrShowFragment(fragment, ENTERTAINMENT_FRAGMENT_TAG);
        invalidateOptionsMenu();
    }

    private void displayNotificationForHadActivity(String title, int iconRes){

        Bitmap largeicon = BitmapFactory.decodeResource(getResources(), iconRes);


        Notification notification = new NotificationCompat.Builder(this,Constants.NOTIFICATION_CHANNEL_1)
                .setContentTitle(title)
                .setContentText("Side activity")
                .setSmallIcon(R.drawable.ic_stat_had)
                .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary))
                .setLargeIcon(largeicon)
                .setTimeoutAfter(10 * 1000)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();

        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(m, notification);

    }


}
