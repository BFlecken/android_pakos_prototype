package com.mvise.android.pakos.pakosprototype.utils;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Window;
import android.widget.Button;

import com.google.gson.Gson;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.Model;

import java.io.File;

import static com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment.CAMERA_REQUEST_CODE;
import static com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment.GALLERY_REQUEST_CODE;
import static com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment.READ_WRITE_CAMERA_REQUEST_CODE;
import static com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment.READ_WRITE_REQUEST_CODE;

public class TakePictureUtil {
    private Fragment fragment;
    private Context context;
    private Uri picUri;

    public TakePictureUtil(Fragment fragment, Context context){
        this.fragment = fragment;
        this.context = context;
    }

    public void onTakePhoto() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_choose_pic);

        Button mGallerybtn = dialog.findViewById(R.id.gallerydialogbtn);
        Button mCarbtn = dialog.findViewById(R.id.cardialogbtn);
        Button mCamerabtn =  dialog.findViewById(R.id.cameradialogbtn);

        mGallerybtn.setOnClickListener(v -> {
            if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT
                    && (ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=PackageManager.PERMISSION_GRANTED)) {
                fragment.requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        }, READ_WRITE_REQUEST_CODE);
            }else{
                afterGalleryPermissions();
            }
            dialog.cancel();
        });

        mCamerabtn.setOnClickListener(v -> {
            if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT
                    && (ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.CAMERA) !=PackageManager.PERMISSION_GRANTED)) {
                fragment.requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA},
                        READ_WRITE_CAMERA_REQUEST_CODE);
            }else{
                afterCameraPermission();
            }

            dialog.cancel();
        });

        String modelString = SharedPrefsUtil.getStringPreference(context, SharedPrefsUtil.KEY_MODEL);
        boolean hasPhotoFromCar = false;
        if (!TextUtils.isEmpty(modelString)) {
            Model currentModel = new Gson().fromJson(modelString, Model.class);
            if (!TextUtils.isEmpty(currentModel.getThumbnail())) {
                if (currentModel.getThumbnail().endsWith("\\u003d")){
                    String thumbnail = currentModel.getThumbnail().replace("\\u003d","");
                    currentModel.setThumbnail(thumbnail);
                }
                hasPhotoFromCar = true;
            }
        }
        boolean isSignedIn = SharedPrefsUtil.getBooleanPreference(
                context,
                SharedPrefsUtil.KEY_SIGNED_IN,
                false);
        if (isSignedIn && hasPhotoFromCar) {
            mCarbtn.setEnabled(true);
            mCarbtn.setTextColor(context.getResources().getColor(android.R.color.black));
            mCarbtn.setCompoundDrawablesWithIntrinsicBounds(null,null,null,
                    context.getResources().getDrawable(R.drawable.baseline_directions_car_white_48));
        }else {
            mCarbtn.setEnabled(false);
            mCarbtn.setTextColor(context.getResources().getColor(android.R.color.darker_gray));
            mCarbtn.setCompoundDrawablesWithIntrinsicBounds(null,null,null,
                    context.getResources().getDrawable(R.drawable.disabled_car));
        }

        mCarbtn.setOnClickListener(viewEr -> dialog.dismiss());
        dialog.show();
    }

    private void afterCameraPermission(){

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = MediaUtils.getOutputMediaFile(context);
        picUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        fragment.startActivityForResult(intent, CAMERA_REQUEST_CODE);

    }

    private void afterGalleryPermissions() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        fragment.startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    public Uri getPicUri(){
        return picUri;
    }
}
