package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class SoundDetails extends BasePOJO{

    @SerializedName("stationary")
    private int stationary;

    @SerializedName("engineSpeed")
    private int engineSpeed;

    @SerializedName("driveBy")
    private int driveBy;

    @Nullable
    @SerializedName("id")
    private String id;

}
