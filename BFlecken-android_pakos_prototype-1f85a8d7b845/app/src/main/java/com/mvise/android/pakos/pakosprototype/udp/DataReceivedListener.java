package com.mvise.android.pakos.pakosprototype.udp;

public interface DataReceivedListener {
    void onDataReceived(String actualResult);
}
