package com.mvise.android.pakos.pakosprototype.realm;

import android.util.Log;

import com.mvise.android.pakos.pakosprototype.App;
import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;
import com.mvise.android.pakos.pakosprototype.model.Profile;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;

import java.util.List;
import java.util.Objects;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Absolute on 6/6/2018.
 */

public class RealmController {

    private static RealmController instance;
    private final Realm realm;
    private static final String TAG = RealmController.class.getSimpleName();

    public RealmController() {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with() {

        if (instance == null) {
            instance = new RealmController();
        }
        return instance;
    }

    public Realm getRealm() {
        return realm;
    }

    public RealmResults<Profile> getProfiles() {
        return realm.where(Profile.class).findAll();
    }

    //query a single item with the given id
    public Profile getProfile(String id) {
        return realm.where(Profile.class).equalTo(Constants.PROFILE_ID, Integer.valueOf(id)).findFirst();
    }

    public void deleteProfile(String profilename){
        Profile p = realm.where(Profile.class).equalTo(Constants.PROFILE_NAME,profilename).findFirst();
        realm.beginTransaction();
        if (p!=null){
            if(SharedPrefsUtil.getIntegerPreference(App.getAppContext(), SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, 1) == p.getId()){
                SharedPrefsUtil.setIntegerPreference(App.getAppContext(), SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, 1);
            }

            p.deleteFromRealm();
        }
        realm.commitTransaction();
    }

    public void insertProfile(Profile profile){
        realm.beginTransaction();
        realm.copyToRealm(profile);
        realm.commitTransaction();
    }

    public void replaceComponentParameters(List<ComponentParameter> componentParameters){
        // Start operation
        realm.beginTransaction();

        // Remove all old profiles
        realm.delete(ComponentParameter.class);

        // Insert new profiles
        for(ComponentParameter componentParameter: componentParameters){
            realm.copyToRealm(componentParameter);
        }

        // Store data
        realm.commitTransaction();
    }

    public void replaceProfiles(List<Profile> profiles){

        // Start operation
        realm.beginTransaction();

        // Remove all old profiles
        realm.delete(Profile.class);

        // Insert new profiles
        for(Profile profile: profiles){
            realm.copyToRealm(profile);
        }

        // Store data
        realm.commitTransaction();
    }

    public RealmResults<ComponentParameter> getParamValues() {
        return realm.where(ComponentParameter.class).findAll();
    }

    public List<ComponentParameter> getParamValuesForProfile(int profile){
        return realm.where(ComponentParameter.class)
                .equalTo(Constants.COMPONENT_PARAM_PROFILE,profile)
                .findAll();
    }

    public ComponentParameter getComponentParameter(String parameter, int profile){
        if (realm.where(ComponentParameter.class).equalTo(Constants.COMPONENT_PARAM_PARAMETER, parameter).equalTo(Constants.COMPONENT_PARAM_PROFILE,profile)==null){
            return null;
        }
        return realm.where(ComponentParameter.class).equalTo(Constants.COMPONENT_PARAM_PARAMETER,parameter).equalTo(Constants.COMPONENT_PARAM_PROFILE,profile).findFirst();
    }

    public void updateComponentParameter(String paramName, String value, String type, String parameter, int profile){
        ComponentParameter newParam = new ComponentParameter();
        ComponentParameter p = realm.where(ComponentParameter.class)
                .equalTo(Constants.COMPONENT_PARAM_PARAMETER,parameter)
                .equalTo(Constants.COMPONENT_PARAM_PROFILE,profile).findFirst();
        Log.wtf(TAG,"updating component parameter: found "+ ((p==null)?"null":p.getName()+" "+p.getProfile()+" " +p.getValue()+" "+p.getParameter()));
        int currentId;
        int lastId;
        if (p==null) {
            if (realm.where(ComponentParameter.class).findAll().size()==0) {
                lastId=0;
            }
            else {
                lastId = realm.where(ComponentParameter.class).max(Constants.COMPONENT_PARAM_ID) != null ?
                        Objects.requireNonNull(realm.where(ComponentParameter.class).max(Constants.COMPONENT_PARAM_ID)).intValue() : 0;
            }
            currentId=lastId+1;

            newParam.setId(currentId);
            newParam.setValue(value);
            newParam.setType(type);
            newParam.setName(paramName);
            newParam.setProfile(profile);
            newParam.setParameter(parameter);
        }else{
            newParam.setId(p.getId());
            newParam.setValue(value);
            newParam.setType(type);
            newParam.setName(paramName);
            newParam.setProfile(profile);
            newParam.setParameter(parameter);
        }
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(newParam);
        realm.commitTransaction();

    }

    public void updateProfile(String originalName, String newName, String imagePath){
        Profile profile = realm.where(Profile.class)
                .equalTo(Constants.PROFILE_ORIGINAL_NAME,originalName)
                .findFirst();
        if (profile!=null){
            realm.beginTransaction();
            profile.setName(newName);
            if(imagePath != null){
                profile.setImagePath(imagePath);
            }
            realm.copyToRealmOrUpdate(profile);
            realm.commitTransaction();
        }
    }

    public int getProfileId(String profileName){
        Profile profile = realm.where(Profile.class).equalTo(Constants.PROFILE_NAME,profileName).findFirst();
        if (profile!=null){
            return profile.getId();
        }
        return -1;
    }

    public RealmResults<Profile> getProfilesWithoutPersona() {
        return realm.where(Profile.class).notEqualTo(Constants.PROFILE_ID, Integer.valueOf(Constants.ID_PERSONA)).findAll();
    }
}