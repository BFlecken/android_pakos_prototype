package com.mvise.android.pakos.pakosprototype.model;

import com.google.gson.JsonObject;

public class OrderedJsonObjectList {
    private JsonObject jsonObjects;
    private int position;

    public OrderedJsonObjectList(JsonObject jsonObjects, int position) {
        this.jsonObjects = jsonObjects;
        this.position = position;
    }

    public OrderedJsonObjectList() {
    }

    public JsonObject getJsonObjects() {
        return jsonObjects;
    }

    public void setJsonObjects(JsonObject jsonObjects) {
        this.jsonObjects = jsonObjects;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
