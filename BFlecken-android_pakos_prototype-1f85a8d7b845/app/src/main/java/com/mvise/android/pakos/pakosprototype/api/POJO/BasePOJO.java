package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

class BasePOJO implements Serializable {

    @Nullable
    @SerializedName("$class")
    String $class;

    public String stringFormat(String value){
        if(value == null) return "";
        return value;
    }

}
