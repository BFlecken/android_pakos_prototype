package com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mvise.android.pakos.pakosprototype.App;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Report;
import com.mvise.android.pakos.pakosprototype.api.POJO.ReportLogEntry;
import com.mvise.android.pakos.pakosprototype.api.blockchain.ReportApi;
import com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity;
import com.mvise.android.pakos.pakosprototype.views.custom.StationaryViewPager;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.adapter.ReportPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class ReportMainFragment extends Fragment {

    @Inject
    Retrofit retrofitInstance;
    private ReportPagerAdapter reportPagerAdapter;
    private StationaryViewPager viewPager;
    private TextView titleTextView;
    private TabLayout tabLayout;
    private Toolbar mToolbar;
    private FloatingActionButton createReportButton;

    public static ReportMainFragment newInstance(){
        ReportMainFragment reportMainFragment = new ReportMainFragment();
        return reportMainFragment;
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.report_fragment_main, container, false);
        findViews(rootView);

        App.getDaggerComponent().inject(this);

        reportPagerAdapter = new ReportPagerAdapter(getChildFragmentManager());
        // Set the adapter onto the view pager
        viewPager.setAdapter(reportPagerAdapter);

        // Give the TabLayout the ViewPager
        tabLayout.setupWithViewPager(viewPager);

        mToolbar.setTitle("Reports");
        titleTextView.setText("Reports");
        if (getActivity() != null){
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
            Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

//        if (getActivity() != null){
//            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
//            Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
//        }

        createReportButton.setOnClickListener(v -> MainNavigationActivity.getInstance().onCreateReportScreenLaunch());
        loadReports();

        return rootView;
    }

    @SuppressLint("CheckResult")
    public void loadReports(){
        retrofitInstance.create(ReportApi.class)
            .getReportList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe((reportList) -> reportPagerAdapter.setReports(reportList),
                (error) ->{

                    // TODO: Remove after tests
                    List<Report> reportList = new ArrayList<>();
                    List<ReportLogEntry> comments = new ArrayList<>();
                    comments.add(new ReportLogEntry("this is the demo comment","06.06.18, 04:34 AM comment",null));

                    reportList.add(new Report("1","OPEN","Open problem 1", "Title 1"));
                    reportList.add(new Report("8","OPEN","Open problem 1", "hardCode comment",comments));
                    reportList.add(new Report("2","OPEN","Open problem 2"));
                    reportList.add(new Report("3","IN_PROGRESS","in progress problem 1","Title 2"));
                    reportList.add(new Report("4","DONE","Done problem 1","Title 3"));
                    reportList.add(new Report("5","DONE","Done problem 2","Title 4"));
                    reportList.add(new Report("6","DONE","Done problem 3","Title 5"));
                    reportList.add(new Report("7","DONE","Done problem 4","Title 6"));


                    reportPagerAdapter.setReports(reportList);
                }
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void findViews(View rootView) {
        viewPager = rootView.findViewById(R.id.viewpager);
        titleTextView = rootView.findViewById(R.id.text_toolbar);
        tabLayout = rootView.findViewById(R.id.sliding_tabs);
        mToolbar = rootView.findViewById(R.id.toolbar);
        createReportButton = rootView.findViewById(R.id.create_report_button);
    }

    }
