package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.LicencePlate;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ChangeLicencePlateApi {

    @GET("de.vda.changeLicencePlate")
    Observable<List<LicencePlate>> getLicencePlateList();

    @POST("de.vda.changeLicencePlate")
    Completable addLicencePlate(@Body LicencePlate licencePlate);

    @GET("de.vda.changeLicencePlate/{id}")
    Single<LicencePlate> getLicencePlatedById(@Path("id") String id);
}
