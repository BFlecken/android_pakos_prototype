package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Parameter extends BasePOJO {

    @NonNull
    @SerializedName("parameterId")
    private String parameterId;

    @NonNull
    @SerializedName("parameterName")
    private String parameterName;

    @NonNull
    @SerializedName("parameterValue")
    private String parameterValue;

    @Nullable
    @SerializedName("parameter_info")
    private String parameterInfo;

    @Nullable
    @SerializedName("id")
    private String id;

    public Parameter(@NonNull String parameterId, @NonNull String parameterName,
                     @NonNull String parameterValue, @Nullable String parameterInfo) {
        this.parameterId = parameterId;
        this.parameterName = parameterName;
        this.parameterValue = parameterValue;
        this.parameterInfo = parameterInfo;
    }

    @NonNull
    public String getParameterId() {
        return parameterId;
    }

    @NonNull
    public String getParameterName() {
        return parameterName;
    }

    @NonNull
    public String getParameterValue() {
        return parameterValue;
    }

    @NonNull
    public String getParameterInfo() {
        return parameterInfo;
    }

    @Nullable
    public String getId() {
        return id;
    }
}
