package com.mvise.android.pakos.pakosprototype.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity;

import java.util.Date;

public class NotificationForegroundService extends Service {

    private NotificationManagerCompat notificationManager;
    private boolean isShowingForegroundNotification;

    public NotificationForegroundService(){

    }

    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(this, "onBind Called", Toast.LENGTH_SHORT).show();
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "onCreate Called", Toast.LENGTH_SHORT).show();
        notificationManager = NotificationManagerCompat.from(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Toast.makeText(this, "onStartCommand Called", Toast.LENGTH_SHORT).show();
        if (intent.hasExtra("foreground")) {
            //Foreground Service Demo
            if (isShowingForegroundNotification) {
                disableHad();
                stopSelf();
            } else
                enableHad();
        } else {
            //Not foreground Service Demo
//            String message = intent.getExtras().getString("extra");
//            displayNotificationForHadActivities(message);

        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "onDestroy Called", Toast.LENGTH_SHORT).show();
        notificationManager.cancelAll();

        if (isShowingForegroundNotification) disableHad();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Toast.makeText(this, "onUnbind Called", Toast.LENGTH_SHORT).show();
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        Toast.makeText(this, "onRebind Called", Toast.LENGTH_SHORT).show();
        super.onRebind(intent);
    }

    private void displayNotificationForHadActivities(String message) {

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainNavigationActivity.class), 0);

        Bitmap largeicon = BitmapFactory.decodeResource(getResources(), R.drawable.had_active);

        Notification notification = new NotificationCompat.Builder(this,Constants.NOTIFICATION_CHANNEL_1)
                .setContentTitle(message)
                .setContentText("Touch to turn off service")
                .setSmallIcon(R.drawable.ic_stat_had)
                .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary))
                .setLargeIcon(largeicon)
                .setTicker("Starting up!!!")
                .setGroup("had_activities")
               // .setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setOngoing(false) //by default false
                .build();

        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(m, notification);
    }


    void enableHad() {

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainNavigationActivity.class), 0);

        Bitmap largeicon = BitmapFactory.decodeResource(getResources(), R.drawable.phone_interacting);

        Notification notification = new NotificationCompat.Builder(this, Constants.NOTIFICATION_CHANNEL_1)
                .setContentTitle("Highly automated driving enabled")
                .setContentText("Side activity")
                .setSmallIcon(R.drawable.ic_stat_had)
                .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary))
                .setLargeIcon(largeicon)
                .setTicker("Starting up!!!")
                //.setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setOngoing(false) //Always true in startForeground
                .build();

        startForeground(1, notification);
        isShowingForegroundNotification = true;
    }


    private void disableHad() {
        stopForeground(true);
        isShowingForegroundNotification = false;
    }

}
