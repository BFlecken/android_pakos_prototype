package com.mvise.android.pakos.pakosprototype.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.Profile;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ActivateProfileAdapter extends RecyclerView.Adapter<ActivateProfileAdapter.ViewHolder> {

    private List<Profile> mProfiles;
    private int mTempActivatedProfile;
    private Context mContext;
    private Set<RadioButton> mRadioButtons = new HashSet<>();

    private static final String TAG = ActivateProfileAdapter.class.getSimpleName();

    static class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton profileRadio;
        ImageView profileImage;
        View itemView;
        TextView profileText;

        ViewHolder(View v) {
            super(v);
            itemView = v;
            profileImage = v.findViewById(R.id.image_profile);
            profileRadio = v.findViewById(R.id.radio_profile);
            profileText = v.findViewById(R.id.text_profile);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public  ActivateProfileAdapter(List<Profile> data, Context context) {
        mProfiles = data;
        mContext = context;
        mTempActivatedProfile = SharedPrefsUtil.getIntegerPreference(mContext, SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, Integer.parseInt(Constants.ID_BASIC_PROFILE));
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_activate_profile, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mRadioButtons.add(holder.profileRadio);

        holder.profileText.setText(mProfiles.get(position).getName());
        String imagePath = mProfiles.get(position).getImagePath();
        if (TextUtils.isEmpty(imagePath)){
            switch (mProfiles.get(position).getId()+""){
                case Constants.ID_BASIC_PROFILE:
                    Glide.with(mContext)
                            .load(R.drawable.vis_profile_basic_2)
                            .into(holder.profileImage);
                    break;
                case Constants.ID_SNOW_PROFILE:
                    Glide.with(mContext)
                            .load(R.drawable.vis_profile_snow)
                            .into(holder.profileImage);
                    break;
                case Constants.ID_WORK_PROFILE:
                    Glide.with(mContext)
                            .load(R.drawable.vis_profile_work)
                            .into(holder.profileImage);
                    break;
            }
        }else{
            Glide.with(mContext)
                    .load(mProfiles.get(position).getImagePath())
                    .into(holder.profileImage);
        }

        holder.profileRadio.setChecked(mTempActivatedProfile == mProfiles.get(position).getId());
        holder.profileRadio.setTag(mProfiles.get(position).getId());

        holder.profileRadio.setOnClickListener(view -> {

           if (((RadioButton)view).isChecked()){
               for (RadioButton btn: mRadioButtons){
                   if (btn.getTag().toString().equals(view.getTag().toString())){
                       btn.setChecked(true);
                   }else {
                       btn.setChecked(false);
                   }
               }
               mTempActivatedProfile = Integer.parseInt(view.getTag().toString());
               SharedPrefsUtil.setIntegerPreference(mContext, SharedPrefsUtil.KEY_TEMP_ACTIVATED_PROFILE,Integer.parseInt(view.getTag().toString()));
               Log.wtf(TAG, "activated profile " +Integer.parseInt(view.getTag().toString()));
           }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (mProfiles == null) {
            return 0;
        }
        return mProfiles.size();
    }
}