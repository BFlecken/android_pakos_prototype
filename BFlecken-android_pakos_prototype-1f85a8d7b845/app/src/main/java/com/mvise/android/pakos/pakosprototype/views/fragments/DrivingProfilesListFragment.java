package com.mvise.android.pakos.pakosprototype.views.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.adapter.DrivingProfilesListAdapter;
import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.model.Profile;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.udp.DataReceivedListener;
import com.mvise.android.pakos.pakosprototype.udp.UdpUtils;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.JsonReaderAsync;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;
import com.mvise.android.pakos.pakosprototype.utils.TempPersistedSignIn;
import com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity;
import com.mvise.android.pakos.pakosprototype.views.base.MainFragmentsNavigationCallback;
import com.mvise.android.pakos.pakosprototype.views.base.UdpCommunicationListener;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

public class DrivingProfilesListFragment extends Fragment implements DrivingProfilesListAdapter.DrivingProfileListener,
        DataReceivedListener {

    private static final String TAG = "views.DrivingProfilesListFragment";
    public static final long DURATION_BETWEEN_PROFILES = 100;
    public static final int RC_VERIFICATION = 101;

    public static DrivingProfilesListAdapter adapter;

    public ListView profilesListView;
    public static RealmResults<Profile> profilesList;
    public static ArrayList<JsonParameter> lcs;
    public static List<JsonParameter> personaParams = new ArrayList<>();

    private FloatingActionButton fab;
    private TextView mReceivedTextView;
    private ScrollView mReceiveSectionScrollView;

    public static boolean sFirstTimeSent = true;
    //private StorageReference mStorageRef;
    private Context mContext;
    private Activity mActivity;
    private MainFragmentsNavigationCallback mNavigationCallback;
    private UdpCommunicationListener mCommunicationListener;

    private Toolbar mToolbar;

    public DrivingProfilesListFragment() {
        // Required empty public constructor
    }

    public void setNavigationCallback(MainFragmentsNavigationCallback callback){
        mNavigationCallback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mContext = getContext();
        mActivity = getActivity();
    }

    private void findViews(View view) {
        profilesListView =  view.findViewById(R.id.profiles_list);
        fab = view.findViewById(R.id.fab);
        mReceivedTextView = view.findViewById(R.id.text_received);
        mToolbar = view.findViewById(R.id.toolbar);
        mReceiveSectionScrollView = view.findViewById(R.id.scroll_received);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_driving_profiles_list, container, false);
      //  receivedDataToDouble("@ffffff\"@ffffff\"");//=>9.2
        //mStorageRef = FirebaseStorage.getInstance().getReference();
        findViews(root);

        profilesList = RealmController.with().getProfilesWithoutPersona();
        if (profilesList.isEmpty()){
            profilesList = insertDefaultProfiles();
        }

//        for (Profile profile: profilesList){
//            Log.wtf(TAG, profile.getId()+" "+profile.getName()+" "+profile.getOriginalName());
//        }

        adapter = new DrivingProfilesListAdapter(profilesList, getContext(), this);
        profilesListView.setAdapter(adapter);
        profilesListView.setOnItemLongClickListener((parent, view, position, id) -> {
            Profile profile = profilesList.get(position);

            mNavigationCallback.onProfileListItemLongClick(profile);
            return true;
        });
        profilesListView.setOnItemClickListener((parent, view, position, id) -> {
            Profile profile = profilesList.get(position);
            if (profile==null) return;

            mNavigationCallback.onProfileSelected(profile);
        });

        fab.setOnClickListener(v -> {
            Profile p = new Profile();
            p.setOriginalName("New profile");
            mNavigationCallback.onProfileSelected(p);
        });

        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    100);
        }else{
            readCodesFile();
        }

        if (getActivity() != null){
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        }
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void readCodesFile() {
        JsonReaderAsync jsonReaderTask = new JsonReaderAsync((MainNavigationActivity)getActivity(), getActivity());
        jsonReaderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static void setLcsParamIds(ArrayList<JsonParameter> list){
        lcs =  list;
        adapter.notifyDataSetChanged();
    }

    public static void setPersonaParams(List<JsonParameter> personaParams) {
        DrivingProfilesListFragment.personaParams = personaParams;
    }

    private RealmResults<Profile> insertDefaultProfiles() {
        List<Profile> profiles = new ArrayList<>();

        profiles.add(new Profile(
                Integer.parseInt(Constants.ID_PERSONA),
                "Persona",
                "User profile",
                "Persona",
                null
            ));
        profiles.add(new Profile(
                Integer.parseInt(Constants.ID_BASIC_PROFILE),
                getString(R.string.profile_basic),
                "Main profile",
                getString(R.string.profile_basic),
                null
                ));

        profiles.add(new Profile(
                Integer.parseInt(Constants.ID_SNOW_PROFILE),
                "Snow",
                "Sub profile",
                "Snow",
                null
        ));

        profiles.add(new Profile(
                Integer.parseInt(Constants.ID_WORK_PROFILE),
                "Work",
                "Sub profile",
                "Work",
                null
        ));

        RealmController.with().replaceProfiles(profiles);
        return RealmController.with().getProfilesWithoutPersona();
    }

    @Override
    public void onSelected(String profile, Context context) {//called if it's not the same profile as the one active til now

        if (!SharedPrefsUtil.getBooleanPreference(context, SharedPrefsUtil.KEY_SIGNED_IN, false)){
            return;
        }

        List<String> tempProfileToSend= new ArrayList<>();
        tempProfileToSend.add(profile);
        if (lcs == null){
            return;
        }

        List<JsonParameter> params = lcs;
        for (JsonParameter param: params){

            String name= param.getParameter();

            if (param.getType().equals("boolean")){
                ComponentParameter componentParameter = RealmController.with()
                        .getComponentParameter(name,Integer.parseInt(profile));
                if (componentParameter == null){
                    componentParameter = RealmController.with()
                            .getComponentParameter(name,Integer.parseInt(profile));
                    if (componentParameter == null){
                        Log.wtf(TAG, "param value profile "+profile+" was null, using 0 for "+name);
                        tempProfileToSend.add("0");
                        Log.wtf(TAG, "sending: "+name+ " "+0);
                    }else{
                        Log.wtf(TAG, "param value profile "+profile+"was "+ componentParameter +" for "+name);
                        if (componentParameter.getValue().equals("true")|| componentParameter.getValue().equals(false+"")){
                            tempProfileToSend.add(componentParameter.getValue().equals(true+"")?1+"":0+"");
                            Log.wtf(TAG, "sending: "+name+ " "+(componentParameter.getValue().equals(true+"")?1+"":0+""));
                        }else{
                            tempProfileToSend.add(0+"");
                            Log.wtf(TAG,"sending: "+name+" " + 0);
                        }
                    }
                }else{
                    tempProfileToSend.add(componentParameter.getValue().equals(true+"")?1+"":0+"");
                    Log.wtf(TAG,"sending: "+name+" " + (componentParameter.getValue().equals(true+"")?1+"":0+""));
                }

            }else if (param.getType().equals(Constants.PARAM_TYPE_INT) || param.getType().equals(Constants.PARAM_TYPE_DOUBLE)){
                ComponentParameter componentParameter = RealmController.with()
                        .getComponentParameter(name, Integer.parseInt(profile));
                if (componentParameter == null){
                    Log.wtf(TAG, "param value profile "+profile+" was null, using 0 for "+name);
                    tempProfileToSend.add(0+"");
                    Log.wtf(TAG,"sending: "+name+" " + 0);
                }else{
                    Log.wtf(TAG, "param value profile "+profile+"was "+ componentParameter +" for "+name);
                    try{
                        double testD = Double.parseDouble(componentParameter.getValue());
                        tempProfileToSend.add(testD+"");
                        Log.wtf(TAG,"sending: "+name+" " + componentParameter.getValue());
                    }catch (NumberFormatException e){
                        e.printStackTrace();
                        tempProfileToSend.add(0+"");
                        Log.wtf(TAG,e.toString());
                        Log.wtf(TAG,"sending: "+name+" " + 0);
                    }

                }
            }
        }
        UdpUtils.sendBroadcast(tempProfileToSend, context, (Activity)context);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //read external
        // If request is cancelled, the result arrays are empty.
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted
                readCodesFile();
            } else {
                // permission denied
                Log.wtf("json-content", "denied");
                mActivity.finish();
            }
        }
    }

    public void onResume() {
        super.onResume();
        updateProfilesList();
    }

    public void updateProfilesList() {
        if (adapter != null){
            adapter.notifyDataSetChanged();
            profilesListView.setAdapter(adapter);
        }
    }

    public void saveReceivedChanges(double[] values) {

        boolean isChanged = false;
        List<JsonParameter> usedParamsList = ((int)values[0]+"").equals(Constants.ID_PERSONA) ? personaParams : lcs;
        if (usedParamsList != null){
            for (int i = 0; i < usedParamsList.size() && i < values.length-1; i++) {
                ComponentParameter oldCp = RealmController.with().getComponentParameter(usedParamsList.get(i).getParameter(),
                        (int)values[0]);
                if (oldCp.getValue().equals(usedParamsList.get(i).getType().equals(Constants.PARAM_TYPE_BOOLEAN) ? (values[i+1] > 0) +"" : values [i+1]+"")){
                    continue;
                }
                isChanged = true;

                ComponentParameter componentParameter = new ComponentParameter();

                componentParameter.setProfile((int)values[0]);
                componentParameter.setName(usedParamsList.get(i).getName());
                componentParameter.setType(usedParamsList.get(i).getType());
                componentParameter.setParameter(usedParamsList.get(i).getParameter());

                componentParameter.setValue(usedParamsList.get(i).getType().equals(Constants.PARAM_TYPE_BOOLEAN) ? (values[i+1] > 0) +"" : values [i+1]+"");
                Log.wtf(TAG,"update: "+(usedParamsList.get(i).getType().equals(Constants.PARAM_TYPE_BOOLEAN) ? (values[i+1] > 0) +"" : values [i+1]+""));

                updateComponentDbValue(componentParameter, (int)values[0] + "");

            }
        }

        if (isChanged){
            if (mCommunicationListener != null){
                mCommunicationListener.onChangedParamsReceived((int)values[0]+"");
            }
        }

    }

    private void updateComponentDbValue(ComponentParameter componentParameter, String profileId) {
        Log.wtf(TAG,"updating parameter value in database, new value: "+componentParameter.getProfile()+" id "+componentParameter.getParameter()+" "+componentParameter.getValue());
        ComponentParameter oldComponentParam = RealmController.with().getComponentParameter(
                componentParameter.getParameter(),
                Integer.parseInt(profileId));
        RealmController.with().updateComponentParameter(
                oldComponentParam != null ? oldComponentParam.getName() : componentParameter.getName(),
                componentParameter.getValue(),
                componentParameter.getType(),
                componentParameter.getParameter(),
                Integer.parseInt(profileId));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof UdpCommunicationListener){
            mCommunicationListener = (UdpCommunicationListener)context;
        }
    }

    @Override
    public void onDataReceived(String actualResult) {
        if (actualResult == null) {
            return;
        }
        if (actualResult.startsWith("exception")){
              mReceivedTextView.append("\n\n"+actualResult);
              mReceiveSectionScrollView.fullScroll(View.FOCUS_DOWN);
        }else {
            String[] strValues = actualResult.split(" ");
            double[] dValues = new double[strValues.length];
            for (int i=0 ; i<dValues.length; i++){
                try{
                    dValues[i] = Double.parseDouble(strValues[i]);
                }catch (NumberFormatException e){
                    e.printStackTrace();
                    mReceivedTextView.append(e.toString());
                }
            }

            double id = dValues[0];
            Log.wtf(TAG,"data received, package id "+id);
            if (!SharedPrefsUtil.getBooleanPreference(mContext, SharedPrefsUtil.KEY_SIGNED_IN, false)
                    && !TempPersistedSignIn.getInstance().isSignedIn()
                    && (int)id == Constants.ID_CAR_PARAMS_PACKAGE){
                if (!TextUtils.isEmpty(mReceivedTextView.getText().toString()) && mReceivedTextView.getLineCount() > 20){
                    mReceivedTextView.setText("");
                }
                mReceivedTextView.append("\n"+actualResult);
                if (mCommunicationListener != null && dValues.length >= 3){
                    mCommunicationListener.onCarParamsReceived(dValues);
                }
                mReceiveSectionScrollView.fullScroll(View.FOCUS_DOWN);

                SharedPrefsUtil.setBooleanPreference(getContext(), SharedPrefsUtil.KEY_RECEIVED_DETAILS_AFTER_SIGNOUT, true);

            }else if ( SharedPrefsUtil.getBooleanPreference(getContext(), SharedPrefsUtil.KEY_SIGNED_IN, false)
                    || TempPersistedSignIn.getInstance().isSignedIn()){
                if (!TextUtils.isEmpty(mReceivedTextView.getText().toString()) && mReceivedTextView.getLineCount() > 20){
                    mReceivedTextView.setText("");
                }
                mReceivedTextView.append("\n"+actualResult);

                switch (((int)id)+""){
                    case Constants.ID_PERSONA:
                    case Constants.ID_BASIC_PROFILE:
                    case Constants.ID_SNOW_PROFILE:
                    case Constants.ID_WORK_PROFILE:
                        saveReceivedChanges(dValues);
                        break;
                    case Constants.ID_REQUEST_FEEDBACK_PACKAGE+"":
                        if (mCommunicationListener != null && dValues.length > 1){
                            Log.wtf(TAG, "FEEDBACK REQUEST PACKAGE: "+ ((int)id)+ " "+ dValues[1]);
                            mCommunicationListener.onFeedbackRequested((int)dValues[1]);
                        }
                        break;
                    case Constants.ID_ACTIVE_PROFILE_SELECTION_REQUEST_PACKAGE+"":
                        if (mCommunicationListener != null && dValues.length >= 2){
                            Log.wtf(TAG, "ACTIVE PROFILE SELECTION REQUEST PACKAGE: "+ ((int)id)+ " "+ dValues[1]);
                            mCommunicationListener.onProfileActivationRequested();
                        }
                        break;
                    case Constants.ID_HAD_ACTIVATION_PACKAGE+"":
                        if (mCommunicationListener != null && dValues.length >= 2){
                            Log.wtf(TAG, "HAD ACTIVATION PACKAGE: "+ ((int)id)+ " "+ dValues[1]);
                            mCommunicationListener.onHadActivationChanged(dValues[1]);
                        }
                        break;
                    case Constants.ID_HAD_ACTIVITY_PACKAGE+"":
                        if (mCommunicationListener != null && dValues.length >= 2){
                            Log.wtf(TAG, "HAD ACTION PACKAGE: "+ ((int)id)+ " "+ dValues[1]);
                            mCommunicationListener.onHadActivityReceived(dValues[1]);
                        }
                        break;
                }

                mReceiveSectionScrollView.fullScroll(View.FOCUS_DOWN);
            }
        }
    }
}
