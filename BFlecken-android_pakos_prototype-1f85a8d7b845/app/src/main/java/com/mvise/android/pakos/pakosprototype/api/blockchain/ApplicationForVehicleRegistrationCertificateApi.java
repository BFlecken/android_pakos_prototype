package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.ApplicationForVehicleRegistrationCertificate;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApplicationForVehicleRegistrationCertificateApi {

    @GET("de.vda.ApplicationForVehicleRegistrationCertificate")
    Observable<List<ApplicationForVehicleRegistrationCertificate>> getApplicationForVehicleRegistrationCertificateList();

    @POST("de.vda.ApplicationForVehicleRegistrationCertificate")
    Completable addApplicationForVehicleRegistrationCertificate(@Body ApplicationForVehicleRegistrationCertificate applicationForVehicleRegistrationCertificate);

    @GET("de.vda.ApplicationForVehicleRegistrationCertificate/{id}")
    Single<ApplicationForVehicleRegistrationCertificate> getApplicationForVehicleRegistrationCertificateById(@Path("id")String id);
}
