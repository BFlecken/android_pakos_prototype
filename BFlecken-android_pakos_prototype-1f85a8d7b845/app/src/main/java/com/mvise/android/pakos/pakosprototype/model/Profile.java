package com.mvise.android.pakos.pakosprototype.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Profile extends RealmObject implements Parcelable{

    @PrimaryKey
    private int id;

    private String name;
    private String details;
    private String originalName;

    private String imagePath;

    public Profile() {
    }

    public Profile(int id, String name, String details, String originalName, String imagePath) {
        this.id = id;
        this.name = name;
        this.details = details;
        this.originalName = originalName;
        this.imagePath = imagePath;
    }

    protected Profile(Parcel in) {
        id = in.readInt();
        name = in.readString();
        details = in.readString();
        originalName = in.readString();
        imagePath = in.readString();
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(details);
        parcel.writeString(originalName);
        parcel.writeString(imagePath);
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
