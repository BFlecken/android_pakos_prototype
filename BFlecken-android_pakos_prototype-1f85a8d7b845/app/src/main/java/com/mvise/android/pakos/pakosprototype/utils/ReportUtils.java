package com.mvise.android.pakos.pakosprototype.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mvise.android.pakos.pakosprototype.App;
import com.mvise.android.pakos.pakosprototype.R;

import java.util.HashMap;
import java.util.Map;

public class ReportUtils {

    private String OPEN;
    private String IN_PROGRESS;
    private String DONE;

    private void initiateStrings(){
        OPEN = App.getAppContext().getText(R.string.report_open).toString();
        IN_PROGRESS = App.getAppContext().getText(R.string.report_in_progress).toString();
        DONE = App.getAppContext().getText(R.string.report_done).toString();
    }


    // Non static properties
    private Map<String, String> statusList;

    public void showChangeDialog(Context context, EditText whereToMakeChange){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.report_dialog_status);

        final CheckBox openCheckBox = dialog.findViewById(R.id.report_dialog_status_open);
        final CheckBox inProgressCheckbox = dialog.findViewById(R.id.report_dialog_status_open_in_progress);
        final CheckBox fixedCheckBox = dialog.findViewById(R.id.report_dialog_status_open_fixed);
        final TextView confirmButton = dialog.findViewById(R.id.report_dialog_status_open_change);

        openCheckBox.setOnClickListener(vs -> {
            inProgressCheckbox.setChecked(false);
            fixedCheckBox.setChecked(false);
        });

        inProgressCheckbox.setOnClickListener(vs -> {
           openCheckBox.setChecked(false);
           fixedCheckBox.setChecked(false);
        });

        fixedCheckBox.setOnClickListener(vs -> {
           openCheckBox.setChecked(false);
           inProgressCheckbox.setChecked(false);
        });

        initiateStrings();

        confirmButton.setOnClickListener(vs -> {
            if(openCheckBox.isChecked()){
                whereToMakeChange.setText(OPEN);
            }
            if(inProgressCheckbox.isChecked()){
                whereToMakeChange.setText(IN_PROGRESS);
            }
            if(fixedCheckBox.isChecked()){
                whereToMakeChange.setText(DONE);
            }
            Toast.makeText(context, "The status changed", Toast.LENGTH_SHORT).show();
            dialog.cancel();
        });

        dialog.show();
    }

    public Map<String, String> getStatusList(){
        initiateStrings();
        if(statusList == null){
            statusList = new HashMap<>();
            statusList.put("OPEN", OPEN);
            statusList.put("IN_PROGRESS", IN_PROGRESS);
            statusList.put("DONE", DONE);
        }
        return statusList;
    }

    public String getStatusListReversed(String status){
        initiateStrings();
        if(OPEN.equals(status)) return "OPEN";
        if(IN_PROGRESS.equals(status)) return "IN_PROGRESS";
        if(DONE.equals(status)) return "DONE";

        return "";
    }

}
