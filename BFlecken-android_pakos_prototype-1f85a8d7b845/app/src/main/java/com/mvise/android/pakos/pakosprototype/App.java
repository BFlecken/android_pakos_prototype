package com.mvise.android.pakos.pakosprototype;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.mvise.android.pakos.pakosprototype.dagger.DaggerMainComponent;
import com.mvise.android.pakos.pakosprototype.dagger.MainComponent;
import com.mvise.android.pakos.pakosprototype.dagger.NetModule;
import com.mvise.android.pakos.pakosprototype.service.AppClosingService;
import com.mvise.android.pakos.pakosprototype.utils.Constants;

import io.realm.Realm;

/**
 * Created by Absolute on 6/6/2018.
 */

public class App extends Application {

    private static MainComponent component;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(getApplicationContext());
        App.context = getApplicationContext();

        createNotificationChannel();

        try{
            startService(new Intent(getBaseContext(), AppClosingService.class));
        }catch (IllegalStateException e){
            e.printStackTrace();
        }

        component = DaggerMainComponent.builder()
                .netModule(new NetModule())
                .build();
    }

    public static MainComponent getDaggerComponent() {
        return component;
    }

    public static Context getAppContext() {
        return App.context;
    }

    private void createNotificationChannel(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel_1 = new NotificationChannel(Constants.NOTIFICATION_CHANNEL_1,
                    "Channel 1",
                    NotificationManager.IMPORTANCE_HIGH);

            NotificationChannel channel_2 = new NotificationChannel(Constants.NOTIFICATION_CHANNEL_2,
                    "Channel 2",
                    NotificationManager.IMPORTANCE_LOW);

            NotificationManager manager = getSystemService(NotificationManager.class);
            if (manager != null) {
                manager.createNotificationChannel(channel_1);
                manager.createNotificationChannel(channel_2);
            }
        }
    }

}
