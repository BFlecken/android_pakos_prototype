package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DrivingProfileRegister extends BasePOJO {

    @NonNull
    @SerializedName("tenant")
    private Object tenant;

    @NonNull
    @SerializedName("masterProfileId")
    private String masterProfileId;

    @Nullable
    @SerializedName("activeDrivingProfile")
    private String activeDrivingProfile;

    @Nullable
    @SerializedName("user_settings")
    private String userSettings;

    @Nullable
    @SerializedName("drivingProfile")
    private List<DrivingProfile> drivingProfile;

    public DrivingProfileRegister(@NonNull Object tenant,
                                  @NonNull String masterProfileId) {
        this.tenant = tenant;
        this.masterProfileId = masterProfileId;
    }

    public DrivingProfileRegister(@NonNull Object tenant,
                                  @NonNull String masterProfileId,
                                  @Nullable String activeDrivingProfile,
                                  @Nullable List<DrivingProfile> drivingProfile) {
        this.tenant = tenant;
        this.masterProfileId = masterProfileId;
        this.activeDrivingProfile = activeDrivingProfile;
        this.drivingProfile = drivingProfile;
    }

    public DrivingProfileRegister(@NonNull Object tenant, @NonNull String masterProfileId,
                                  @Nullable String activeDrivingProfile,
                                  @Nullable String userSettings,
                                  @Nullable List<DrivingProfile> drivingProfile) {
        this.tenant = tenant;
        this.masterProfileId = masterProfileId;
        this.activeDrivingProfile = activeDrivingProfile;
        this.userSettings = userSettings;
        this.drivingProfile = drivingProfile;
    }

    public void setMasterProfileId(@NonNull String masterProfileId) {
        this.masterProfileId = masterProfileId;
    }

    @NonNull
    public List<DrivingProfile> getDrivingProfile() {
        return drivingProfile;
    }

    @NonNull
    public String getUserSettings() {
        return userSettings;
    }
}
