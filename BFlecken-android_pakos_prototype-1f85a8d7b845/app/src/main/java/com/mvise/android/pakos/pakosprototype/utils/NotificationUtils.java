package com.mvise.android.pakos.pakosprototype.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity;

/**
 * Utility class for creating notifications
 */
public class NotificationUtils {

    /*
     * This notification ID can be used to access our notification after we've displayed it. This
     * can be handy when we need to cancel the notification, or perhaps update it. This number is
     * arbitrary and can be set to whatever you like. 1138 is in no way significant.
     */
    private static final int LOOK_INTO_CAMERA_NOTIFICATION_ID = 1138;

    /**
     * This pending intent id is used to uniquely reference the pending intent
     */
    private static final int START_VERIFICATION_PENDING_INTENT_ID = 3417;

    private static final String NOTIFICATION_CHANNEL_ID= "pakos.notification";

   /* public static void clearAllNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
    }*/

   public static final String EXTRA_FRAGMENT = "EXTRA_FRAGMENT";

    public static void notifyUserToLookIntoCamera(Context context) {
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context,NOTIFICATION_CHANNEL_ID)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_small_notification_icon)
                .setLargeIcon(largeIcon(context))
                .setContentTitle(context.getString(R.string.notification_title))
                .setContentText(context.getString(R.string.notification_body))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(
                        context.getString(R.string.notification_body)))
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(contentIntent(context))
                .setAutoCancel(true);


        if (notificationManager != null) {
            notificationManager.notify(LOOK_INTO_CAMERA_NOTIFICATION_ID, notificationBuilder.build());
        }
    }

    private static PendingIntent contentIntent(Context context) {
        Intent startActivityIntent = new Intent(context, MainNavigationActivity.class);
        startActivityIntent.putExtra(EXTRA_FRAGMENT, MainNavigationActivity.DRIVER_VERIFICATION_FRAGMENT_TAG);
        return PendingIntent.getActivity(
                context,
                START_VERIFICATION_PENDING_INTENT_ID,
                startActivityIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static Bitmap largeIcon(Context context) {
        Resources res = context.getResources();
        return BitmapFactory.decodeResource(res, R.mipmap.ic_launcher);
    }
}