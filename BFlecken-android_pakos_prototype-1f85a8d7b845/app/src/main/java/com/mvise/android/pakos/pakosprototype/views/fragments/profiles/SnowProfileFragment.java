package com.mvise.android.pakos.pakosprototype.views.fragments.profiles;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.Model;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;
import com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment;
import com.mvise.android.pakos.pakosprototype.views.custom.ProfileScrollView;

import java.io.File;
import java.util.Objects;

public class SnowProfileFragment extends BaseProfileFragment {

    private EditText mProfileNameEditText;
    private Toolbar mToolbar;
    private ImageView mProfilePictureImageView;
    private ProfileScrollView mScrollView;

    private static final String BUNDLE_IMAGE_PATH = "IMAGE_PATH";

    private String mCurrentImagePath = "";

    public SnowProfileFragment(){
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.setProfileId(Constants.ID_SNOW_PROFILE);
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        if (rootView != null) {
            findViews(rootView);
        }
        mToolbar.setTitle("");
        if (getActivity() != null){
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
            Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        mProfileNameEditText.setText(R.string.profile_snow);

        if (savedInstanceState == null){
            super.populateUi(Constants.ID_SNOW_PROFILE);
            mCurrentImagePath = RealmController.with().getProfile(Constants.ID_SNOW_PROFILE)
                    .getImagePath();
            Log.wtf("path0504", mCurrentImagePath+" path");
            if (!TextUtils.isEmpty(mCurrentImagePath)){
                Glide.with(this)
                        .load(new File(mCurrentImagePath))
                        .into(mProfilePictureImageView);
            }
        }else{
            mCurrentImagePath = savedInstanceState.getString(BUNDLE_IMAGE_PATH);
            if (!TextUtils.isEmpty(mCurrentImagePath)){
                Glide.with(this)
                        .load(new File(mCurrentImagePath))
                        .into(mProfilePictureImageView);
            }
        }

        mScrollView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                v.performClick();
            }
            if (mProfileNameEditText.hasFocus()){
                mProfileNameEditText.clearFocus();
            }
            return false;
        });

        return rootView;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_snow_profile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String path = super.getmCurrentPhotoPath();
        if (!TextUtils.isEmpty(path)){
            Glide.with(this)
                    .load(new File(super.getmCurrentPhotoPath()))
                    .into(mProfilePictureImageView);
            mCurrentImagePath = super.getmCurrentPhotoPath();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(BUNDLE_IMAGE_PATH, mCurrentImagePath);
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void setProfilePictureFromCar()  {
        String modelString = SharedPrefsUtil.getStringPreference(getContext(),
                SharedPrefsUtil.KEY_MODEL);
        if (!TextUtils.isEmpty(modelString)) {
            Model currentModel = new Gson().fromJson(modelString, Model.class);
            if (!TextUtils.isEmpty(currentModel.getThumbnail())) {
                if (currentModel.getThumbnail().endsWith("\\u003d")){
                    String thumbnail = currentModel.getThumbnail().replace("\\u003d","");
                    currentModel.setThumbnail(thumbnail);
                }
                byte[] decodedString = Base64.decode(currentModel.getThumbnail(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,
                        decodedString.length);
                mProfilePictureImageView.setImageBitmap(decodedByte);
            }
        }

    }

    private void findViews(View rootView) {
        mProfileNameEditText = rootView.findViewById(R.id.edit_profile_name);
        mToolbar = rootView.findViewById(R.id.toolbar_basic);
        mProfilePictureImageView = rootView.findViewById(R.id.image_profile_pic);
        mScrollView = rootView.findViewById(R.id.scrollView);
    }

    public void onTakePhoto(View view) {
        super.displayPopupChooseImage();
    }

}
