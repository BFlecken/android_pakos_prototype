package com.mvise.android.pakos.pakosprototype.api.POJO;

import com.google.gson.annotations.SerializedName;

public class TrailerDetails extends BasePOJO {

    @SerializedName("maxPermissibleTowableMassBraked")
    private int maxPermissibleTowableMassBraked;

    @SerializedName("maxPermissibleTowableMassUnbraked")
    private int maxPermissibleTowableMassUnbraked;
}
