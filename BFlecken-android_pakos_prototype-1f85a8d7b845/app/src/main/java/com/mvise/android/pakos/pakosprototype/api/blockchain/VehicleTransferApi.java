package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.VehicleTransfer;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface VehicleTransferApi {

    @GET("de.vda.VehicleTransfer")
    Observable<List<VehicleTransfer>> getVehicleTransferList();

    @POST("de.vda.VehicleTransfer")
    Completable addVehicleTransfer(@Body VehicleTransfer vehicleTransfer);

    @GET("de.vda.VehicleTransfer/{id}")
    Single<VehicleTransfer> getVehicleTransferById(@Path("id") String id);
}
