package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.LogVehicle;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface LoginVehicleApi {

    @GET("de.vda.loginVehicle")
    Observable<List<LogVehicle>> getLoginVehicleList();

    @POST("de.vda.loginVehicle")
    Completable addLoginVehicle(@Body LogVehicle had);

    @GET("de.vda.loginVehicle/{id}")
    Single<LogVehicle> getLoginVehicleById(@Path("id") String id);
}
