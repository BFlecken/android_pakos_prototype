package com.mvise.android.pakos.pakosprototype.api;

import com.mvise.android.pakos.pakosprototype.model.Model;
import com.mvise.android.pakos.pakosprototype.model.VerificationModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface FaceIDApiInterface {

    @POST("/verify_model")
    Call<VerificationModel> verifyModel(@Query("apikey") String apiKey, @Body VerificationModel modelPOJO);

    @POST("/enroll")
    Call<Model> enroll(@Query("apikey") String apiKey);
}
