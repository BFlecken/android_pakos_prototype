package com.mvise.android.pakos.pakosprototype.utils;

public class TempPersistedSignIn {

    private static TempPersistedSignIn instance;
    private boolean isSignedIn;

    public static TempPersistedSignIn getInstance() {
        if (instance == null){
            instance = new TempPersistedSignIn();
        }
        return instance;
    }

    public void setSignedIn(boolean signedIn) {
        isSignedIn = signedIn;
    }

    public boolean isSignedIn() {
        return isSignedIn;
    }
}
