package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.Company;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CompanyApi {

    @GET("de.pakos.lifecycle.Company")
    Observable<List<Company>> getCompanyList();

    @POST("de.pakos.lifecycle.Company")
    Completable addCompany(@Body Company company);

    @GET("de.pakos.lifecycle.Company/{id}")
    Single<Company> getCompanyById(@Path("id") String id);

    @HEAD("de.pakos.lifecycle.Company/{id}")
    Completable checkCompany(@Path("id") String id);

    @PUT("de.pakos.lifecycle.Company/{id}")
    Completable updateCompany(@Path("id") String id, @Body Company company);

    @DELETE("de.pakos.lifecycle.Company/{id}")
    Completable deleteCompany(@Path("id") String id);
}
