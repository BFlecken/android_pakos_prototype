package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.Suspicious;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UpdateSuspicious {

    @GET("de.vda.updateSuspicious")
    Observable<List<Suspicious>> getSuspiciousList();

    @POST("de.vda.updateSuspicious")
    Completable addSuspicious(@Body Suspicious suspicious);

    @GET("de.vda.updateSuspicious/{id}")
    Single<Suspicious> getSuspiciousById(@Path("id") String id);
}
