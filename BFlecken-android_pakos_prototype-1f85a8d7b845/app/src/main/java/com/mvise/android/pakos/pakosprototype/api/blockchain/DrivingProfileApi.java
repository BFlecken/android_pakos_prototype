package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.DrivingProfile;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DrivingProfileApi {

    @GET("de.vda.DrivingProfile")
    Observable<List<DrivingProfile>> getDrivingProfileList();

    @POST("de.vda.DrivingProfile")
    Completable addDrivingProfile(@Body DrivingProfile drivingProfile);

    @GET("de.vda.DrivingProfile/{id}")
    Single<DrivingProfile> getDrivingProfileById(@Path("id") String id);

    @HEAD("de.vda.DrivingProfile/{id}")
    Completable checkDrivingProfile(@Path("id") String id);

    @PUT("de.vda.DrivingProfile/{id}")
    Completable updateDrivingProfile(@Path("id") String id, @Body DrivingProfile drivingProfile);

    @DELETE("de.vda.DrivingProfile/{id}")
    Completable deleteDrivingProfile(@Path("id") String id);
}
