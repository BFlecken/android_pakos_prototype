package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class ContactDetails extends BasePOJO{

    @Nullable
    @SerializedName("email")
    private String email;

    @Nullable
    @SerializedName("mobilePhone")
    private String mobilePhone;

    @Nullable
    @SerializedName("homePhone")
    private String homePhone;

    @Nullable
    @SerializedName("address")
    private Address address;

    @Nullable
    @SerializedName("id")
    private String id;

    public ContactDetails(@Nullable String email, @Nullable String mobilePhone, @Nullable String homePhone, @Nullable Address address, @Nullable String id) {
        this.email = email;
        this.mobilePhone = mobilePhone;
        this.homePhone = homePhone;
        this.address = address;
        this.id = id;
    }
}
