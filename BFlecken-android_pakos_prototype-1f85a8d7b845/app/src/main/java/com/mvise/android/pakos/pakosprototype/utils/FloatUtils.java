package com.mvise.android.pakos.pakosprototype.utils;

import java.math.BigDecimal;

public class FloatUtils {
    // sum
    public static float addDecimals(String a, String b) {
        BigDecimal BigDec = new BigDecimal(a+"");
        BigDec = BigDec.add(new BigDecimal(b+""));
        return BigDec.floatValue();
    }

    public static float subtractDecimals(String s, String s1) {
        BigDecimal BigDec = new BigDecimal(s+"");
        BigDec = BigDec.subtract(new BigDecimal(s1+""));
        return BigDec.floatValue();
    }
}
