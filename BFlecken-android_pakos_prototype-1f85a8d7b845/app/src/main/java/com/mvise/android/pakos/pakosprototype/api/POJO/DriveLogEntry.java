package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class DriveLogEntry extends BasePOJO {

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @NonNull
    @SerializedName("tenant")
    private Object tenant;

    @SerializedName("HAD")
    private boolean HAD;

    @NonNull
    @SerializedName("event")
    private String event;

    @NonNull
    @SerializedName("timestamp")
    private String timestamp;

    @Nullable
    @SerializedName("id")
    private String id;


}
