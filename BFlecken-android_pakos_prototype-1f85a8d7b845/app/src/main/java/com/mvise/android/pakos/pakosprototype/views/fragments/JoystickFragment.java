package com.mvise.android.pakos.pakosprototype.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.utils.FloatUtils;
import com.mvise.android.pakos.pakosprototype.utils.JoystickValueUpdateListener;
import com.mvise.android.pakos.pakosprototype.views.custom.JoystickView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class JoystickFragment extends Fragment {
    public static final String ARG_JSON_PARAMS = "JSON_PARAMS";
    public static final String ARG_PROFILE_ID = "PROFILE_ID";
    private static final String SUFFIX_X_AXIS = "x";
    private static final String SUFFIX_Y_AXIS = "y";
    private static final String ARG_COMP_PARAMS = "COMPONENT_PARAMS";
    private static final String TAG = JoystickFragment.class.getSimpleName();

    private List<JsonParameter> mJsonParameters = new ArrayList<>();
    private List<ComponentParameter> mComponentParams = new ArrayList<>();

    private JoystickView mJoystick;
    private ImageButton mLeftButton, mRightButton, mUpButton, mDownButton;
    private LinearLayout mValueLabelsContainer;
    private List<View> mGeneratedViews = new ArrayList<>();

    private float mValueX, mValueY;
    private ComponentParameter mComponentX, mComponentY;

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.button_left:
                    //x axis decreasing
                    mValueX = FloatUtils.subtractDecimals(mValueX+"", "0.1");
                    mComponentX.setValue(mValueX+"");
                    if (mListener != null){
                        mListener.onValueChanged(mComponentX);
                    }

                    if (mGeneratedViews != null){
                        if (mGeneratedViews.size() > 0){
                            TextView xValueText = (TextView)mGeneratedViews.get(0);
                            xValueText.setText(String.valueOf(mValueX));
                        }
                        if (mGeneratedViews.size() > 1){
                            TextView yValueText = (TextView) mGeneratedViews.get(1);
                            yValueText.setText(String.valueOf(mValueY));
                        }
                    }
                    break;
                case R.id.button_down:
                    //y axis decreasing
                    mValueY = FloatUtils.subtractDecimals(mValueY+"", "0.1");
                    mComponentY.setValue(mValueY+"");
                    if (mListener != null){
                        mListener.onValueChanged(mComponentY);
                    }

                    if (mGeneratedViews != null){
                        if (mGeneratedViews.size() > 0){
                            TextView xValueText = (TextView)mGeneratedViews.get(0);
                            xValueText.setText(String.valueOf(mValueX));
                        }
                        if (mGeneratedViews.size() > 1){
                            TextView yValueText = (TextView) mGeneratedViews.get(1);
                            yValueText.setText(String.valueOf(mValueY));
                        }
                    }

                    break;
                case R.id.button_up:
                    //y axis increasing
                    mValueY = FloatUtils.addDecimals(mValueY+"", "0.1");
                    mComponentY.setValue(mValueY+"");
                    if (mListener != null){
                        mListener.onValueChanged(mComponentY);
                    }
                    if (mGeneratedViews != null){
                        if (mGeneratedViews.size() > 0){
                            TextView xValueText = (TextView)mGeneratedViews.get(0);
                            xValueText.setText(String.valueOf(mValueX));
                        }
                        if (mGeneratedViews.size() > 1){
                            TextView yValueText = (TextView) mGeneratedViews.get(1);
                            yValueText.setText(String.valueOf(mValueY));
                        }
                    }
                    break;
                case R.id.button_right:
                    //x axis increasing
                    mValueX = FloatUtils.addDecimals(mValueX+"", "0.1");
                    mComponentX.setValue(mValueX+"");
                    if (mListener != null){
                        mListener.onValueChanged(mComponentX);
                    }
                    if (mGeneratedViews != null){
                        if (mGeneratedViews.size() > 0){
                            TextView xValueText = (TextView)mGeneratedViews.get(0);
                            xValueText.setText(String.valueOf(mValueX));
                        }
                        if (mGeneratedViews.size() > 1){
                            TextView yValueText = (TextView) mGeneratedViews.get(1);
                            yValueText.setText(String.valueOf(mValueY));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    };
    private JoystickValueUpdateListener mListener;

    public JoystickFragment() {
        // Required empty public constructor
    }

    public static JoystickFragment newInstance(List<JsonParameter> parameters, String profileId, List<ComponentParameter> componentParameters) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_JSON_PARAMS, (ArrayList<JsonParameter>) parameters);
        args.putString(ARG_PROFILE_ID, profileId);
        args.putParcelableArrayList(ARG_COMP_PARAMS, (ArrayList<ComponentParameter>)componentParameters);
        JoystickFragment fragment = new JoystickFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            mJsonParameters = getArguments().getParcelableArrayList(ARG_JSON_PARAMS);
            mComponentParams = getArguments().getParcelableArrayList(ARG_COMP_PARAMS);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_joystick, container, false);
        findViews(rootView);

        mLeftButton.setOnClickListener(mOnClickListener);
        mRightButton.setOnClickListener(mOnClickListener);
        mUpButton.setOnClickListener(mOnClickListener);
        mDownButton.setOnClickListener(mOnClickListener);

        if (mJsonParameters != null){
            for (JsonParameter parameter: mJsonParameters){
                generateValueLabel(parameter);
            }
        }

        mJoystick.setOnMoveListener((angle, strength) -> {
            if (strength > 50){

                if (angle>=0 && angle<=45 || angle>=315){
                   //x axis increasing
                    mValueX = FloatUtils.addDecimals(mValueX+"", "0.1");
                    mComponentX.setValue(mValueX+"");
                    if (mListener != null){
                        mListener.onValueChanged(mComponentX);
                    }
                }else if (angle > 45 && angle <= 135){
                    //y axis increasing
                    mValueY = FloatUtils.addDecimals(mValueY+"", "0.1");
                    mComponentY.setValue(mValueY+"");
                    if (mListener != null){
                        mListener.onValueChanged(mComponentY);
                    }
                }else if (angle > 135 && angle <= 225){
                    //x axis decreasing
                    mValueX = FloatUtils.subtractDecimals(mValueX+"", "0.1");
                    mComponentX.setValue(mValueX+"");
                    if (mListener != null){
                        mListener.onValueChanged(mComponentX);
                    }
                }else{
                    //y axis decreasing
                    mValueY = FloatUtils.subtractDecimals(mValueY+"", "0.1");
                    mComponentY.setValue(mValueY+"");
                    if (mListener != null){
                        mListener.onValueChanged(mComponentY);
                    }
                }

                if (mGeneratedViews != null){
                    if (mGeneratedViews.size() > 0){
                        TextView xValueText = (TextView)mGeneratedViews.get(0);
                        xValueText.setText(String.valueOf(mValueX));
                    }
                    if (mGeneratedViews.size() > 1){
                        TextView yValueText = (TextView) mGeneratedViews.get(1);
                        yValueText.setText(String.valueOf(mValueY));
                    }
                }
            }

        });

        return rootView;
    }

    private void findViews(View rootView){
        mJoystick = rootView.findViewById(R.id.joystick);
        mLeftButton = rootView.findViewById(R.id.button_left);
        mRightButton = rootView.findViewById(R.id.button_right);
        mUpButton = rootView.findViewById(R.id.button_up);
        mDownButton = rootView.findViewById(R.id.button_down);
        mValueLabelsContainer = rootView.findViewById(R.id.container_header);
    }

    private void generateValueLabel(JsonParameter parameter) {

        if (parameter != null && getActivity() != null){
            LinearLayout valueLayout =
                    (LinearLayout) ((LayoutInflater) Objects.requireNonNull(getActivity().getSystemService(LAYOUT_INFLATER_SERVICE)))
                            .inflate(R.layout.label_joystick_value, mValueLabelsContainer, false);
            valueLayout.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
            mValueLabelsContainer.addView(valueLayout);
            TextView labelText = valueLayout.findViewById(R.id.text_label);
            labelText.setText(parameter.getName());
            TextView valueText = valueLayout.findViewById(R.id.text_value);

            mGeneratedViews.add(valueText);

            for (ComponentParameter cp: mComponentParams){
                if (cp.getParameter().equals(parameter.getParameter())){
                    Log.wtf(TAG,cp.getValue()+" "+
                            cp.getProfile()+" "+cp.getParameter()+" "+cp.getName());
                    ComponentParameter componentParameter = new ComponentParameter();
                    componentParameter.setProfile(cp.getProfile());
                    componentParameter.setValue(cp.getValue());
                    componentParameter.setParameter(cp.getParameter());
                    componentParameter.setType(cp.getType());

                    if (componentParameter.getParameter().endsWith(SUFFIX_X_AXIS)){
                        valueText.setTag(SUFFIX_X_AXIS);
                        mValueX = Float.valueOf(cp.getValue());
                        mComponentX = componentParameter.clone();

                    }else if (componentParameter.getParameter().endsWith(SUFFIX_Y_AXIS)){
                        valueText.setTag(SUFFIX_Y_AXIS);
                        mValueY = Float.valueOf(cp.getValue());
                        mComponentY = componentParameter.clone();
                    }else{
                        continue;
                    }
                    valueText.setText(String.valueOf(Float.valueOf(cp.getValue())));
                    if (mListener != null){
                        mListener.onValueChanged(componentParameter);
                    }
                    break;
                }
            }
        }
    }

    public void setmListener(JoystickValueUpdateListener mListener) {
        this.mListener = mListener;
    }
}
