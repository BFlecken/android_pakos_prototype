package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.Report;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ReportApi {

    @GET("de.vda.Report")
    Observable<List<Report>> getReportList();

    @POST("de.vda.Report")
    Completable addReport(@Body Report report);

    @GET("de.vda.Report/{id}")
    Single<Report> getReportById(@Path("id") String id);

    @HEAD("de.vda.Report/{id}")
    Completable checkReport(@Path("id") String id);

    @PUT("de.vda.Report/{id}")
    Completable updateReport(@Path("id") String id, @Body Report report);

    @DELETE("de.vda.Report/{id}")
    Completable deleteReport(@Path("id") String id);
}
