package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class ReportLogEntry extends BasePOJO{

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @Nullable
    @SerializedName("tenant")
    private Object tenant;

    @Nullable
    @SerializedName("owner")
    private Object owner;

    @NonNull
    @SerializedName("event")
    private String event;

    @NonNull
    @SerializedName("reportMessage")
    private String reportMessage;

    @NonNull
    @SerializedName("timestamp")
    private String timestamp;

    @Nullable
    @SerializedName("id")
    private String id;

    public ReportLogEntry( @Nullable String reportMessage,
                          @Nullable String timestamp, @Nullable String eventDetails) {
        this.event = eventDetails;
        this.vehicle = "vehicle";
        this.reportMessage = reportMessage;
        this.timestamp = timestamp;
    }

    @NonNull
    public Object getVehicle() {
        return vehicle;
    }

    @Nullable
    public Object getTenant() {
        return tenant;
    }

    @Nullable
    public Object getOwner() {
        return owner;
    }

    @NonNull
    public String getEvent() {
        return event;
    }

    @NonNull
    public String getReportMessage() {
        return reportMessage;
    }

    @NonNull
    public String getTimestamp() {
        return timestamp;
    }

    @Nullable
    public String getId() {
        return id;
    }
}
