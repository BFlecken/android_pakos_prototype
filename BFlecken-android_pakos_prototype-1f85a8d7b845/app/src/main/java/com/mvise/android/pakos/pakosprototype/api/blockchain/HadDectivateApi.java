package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.Had;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface HadDectivateApi {

    @GET("de.vda.deactivateHAD")
    Observable<List<Had>> getHadDeactivatedList();

    @POST("de.vda.deactivateHAD")
    Completable addHadDeactivate(@Body Had had);

    @GET("de.vda.deactivateHAD/{id}")
    Single<Had> getHadDeactivatedById(@Path("id")String id);
}
