package com.mvise.android.pakos.pakosprototype.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.mvise.android.pakos.pakosprototype.R;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();

    private EditText mUsernameEditText;
    private EditText mPasswordEditText;
    private EditText mPasswordConfirmEditText;
    private EditText mNameEditText;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        findViews();

        mAuth = FirebaseAuth.getInstance();
    }

    private void findViews() {
        mUsernameEditText = findViewById(R.id.edit_username);
        mPasswordEditText = findViewById(R.id.edit_password);
        mPasswordConfirmEditText = findViewById(R.id.edit_confirm_pass);
        mNameEditText = findViewById(R.id.edit_name);
    }

    public void onRegister(View view) {
        if (TextUtils.isEmpty(mNameEditText.getText().toString())){
            Toast.makeText(this,
                    R.string.message_empty_fullname,
                    Toast.LENGTH_SHORT).show();
            mNameEditText.setHintTextColor(getResources().getColor(android.R.color.holo_red_dark));
        }else if (TextUtils.isEmpty(mUsernameEditText.getText().toString())){
            Toast.makeText(this,
                    R.string.message_empty_mail,
                    Toast.LENGTH_SHORT).show();
            mUsernameEditText.setHintTextColor(getResources().getColor(android.R.color.holo_red_dark));
        }else if (TextUtils.isEmpty(mPasswordEditText.getText().toString())){
            Toast.makeText(this,
                    R.string.message_empty_password,
                    Toast.LENGTH_SHORT).show();
            mPasswordEditText.setHintTextColor(getResources().getColor(android.R.color.holo_red_dark));
        }else if (TextUtils.isEmpty(mPasswordConfirmEditText.getText().toString())){
            Toast.makeText(this,
                    R.string.message_empty_password_confirmation,
                    Toast.LENGTH_SHORT).show();
            mPasswordConfirmEditText.setHintTextColor(getResources().getColor(android.R.color.holo_red_dark));
        }else if (mPasswordEditText.getText().toString().length() < 6){
            Toast.makeText(this,
                    R.string.message_short_password,
                    Toast.LENGTH_SHORT).show();
            mPasswordEditText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        } else if (!isValidEmail(mUsernameEditText.getText())){
            Toast.makeText(this,
                    R.string.message_invalid_mail,
                    Toast.LENGTH_SHORT).show();
            mUsernameEditText.setHintTextColor(getResources().getColor(android.R.color.holo_red_dark));
        } else{
            if (!mPasswordConfirmEditText.getText().toString().equals(mPasswordEditText.getText().toString())){
                Toast.makeText(this,
                        R.string.message_mismatch_password,
                        Toast.LENGTH_SHORT).show();
                mPasswordEditText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                mPasswordConfirmEditText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }else{
                signUp(mUsernameEditText.getText().toString(), mPasswordEditText.getText().toString());
            }

        }
    }

    private void signUp(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "createUserWithEmail:success");
                        Toast.makeText(RegisterActivity.this,
                                R.string.message_auth_success,
                                Toast.LENGTH_SHORT).show();
                        finish();

                    } else {
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(RegisterActivity.this,
                                R.string.message_auth_fail,
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
