package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.UpdateDrivingProfile;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UpdateDrivingProfileApi {

    @GET("de.vda.updateDrivingProfile")
    Observable<List<UpdateDrivingProfile>> getUpdateDrivingProfileList();

    @POST("de.vda.updateDrivingProfile")
    Completable addUpdateDrivingProfile(@Body UpdateDrivingProfile updateDrivingProfile);

    @GET("de.vda.updateDrivingProfile/{id}")
    Single<UpdateDrivingProfile> getUpdateDrivingProfileById(@Path("id") String id);

}
