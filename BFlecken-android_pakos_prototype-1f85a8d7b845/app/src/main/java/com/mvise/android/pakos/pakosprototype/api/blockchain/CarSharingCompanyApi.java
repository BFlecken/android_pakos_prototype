package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.Company;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CarSharingCompanyApi {

    @GET("de.pakos.lifecycle.CarSharingCompany")
    Observable<List<Company>> getCarSharingCompanyList();

    @POST("de.pakos.lifecycle.CarSharingCompany")
    Completable addCarSharingCompany(@Body Company company);

    @GET("de.pakos.lifecycle.CarSharingCompany/{id}")
    Single<Company> getCarSharingCompanyById(@Path("id") String id);

    @HEAD("de.pakos.lifecycle.CarSharingCompany/{id}")
    Completable checkCarSharingCompany(@Path("id") String id);

    @PUT("de.pakos.lifecycle.CarSharingCompany/{id}")
    Completable updateCarSharingCompany(@Path("id") String id, @Body Company company);

    @DELETE("de.pakos.lifecycle.CarSharingCompany/{id}")
    Completable deleteCarSharingCompany(@Path("id") String id);
}
