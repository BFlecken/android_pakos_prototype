package com.mvise.android.pakos.pakosprototype.views.base;

public interface ProfilesActivationListener {
    void onRefreshProfilesActivation();
}
