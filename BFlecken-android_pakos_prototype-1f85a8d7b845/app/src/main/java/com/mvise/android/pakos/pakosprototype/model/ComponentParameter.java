package com.mvise.android.pakos.pakosprototype.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ComponentParameter extends RealmObject implements Parcelable, Cloneable {
    @PrimaryKey
    private int id;

    private String name;
    private String value;
    private String type;
    private int profile;//0-basic, 1-snow, 2-work, new profiles use next values as profile id
    private String parameter;

    public ComponentParameter(int id, String name, String value, String type, int profile, String parameter) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.type = type;
        this.profile = profile;
        this.parameter = parameter;
    }

    public ComponentParameter() {
    }

    protected ComponentParameter(Parcel in) {
        id = in.readInt();
        name = in.readString();
        value = in.readString();
        type = in.readString();
        profile = in.readInt();
        parameter = in.readString();
    }

    public static final Creator<ComponentParameter> CREATOR = new Creator<ComponentParameter>() {
        @Override
        public ComponentParameter createFromParcel(Parcel in) {
            return new ComponentParameter(in);
        }

        @Override
        public ComponentParameter[] newArray(int size) {
            return new ComponentParameter[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(value);
        dest.writeString(type);
        dest.writeInt(profile);
        dest.writeString(parameter);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public ComponentParameter clone()  {
        ComponentParameter cp = new ComponentParameter();
        cp.setValue(value);
        cp.setType(type);
        cp.setParameter(parameter);
        cp.setProfile(profile);
        cp.setName(name);
        cp.setId(id);
        return cp;
    }
}
