package com.mvise.android.pakos.pakosprototype.api.POJO;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Report extends BasePOJO {

    @NonNull
    @SerializedName("reportId")
    private String reportId;

    @NonNull
    @SerializedName("reportStatus")
    private String reportStatus;

    @NonNull
    @SerializedName("reportMessage")
    private String reportMessage;

    @NonNull
    @SerializedName("reportTitle")
    private String reportTitle;

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @Nullable
    @SerializedName("tenant")
    private Object tenant;

    @Nullable
    @SerializedName("owner")
    private Object owner;

    @Nullable
    @SerializedName("mediafile")
    private Mediafile mediafile;

    @Nullable
    @SerializedName("reportLogEntries")
    private List<ReportLogEntry> reportLogEntries;

    @NonNull
    public String getReportId() {
        return reportId;
    }

    public Report(@NonNull String reportId, @NonNull String reportStatus, @NonNull String reportMessage) {
        this.reportId = reportId;
        this.reportStatus = reportStatus;
        this.reportMessage = reportMessage;
        this.vehicle = "vehicle";
    }

    public Report(@NonNull String reportId, @NonNull String reportStatus, @NonNull String reportMessage, @NonNull String reportTitle) {
        this.reportId = reportId;
        this.reportStatus = reportStatus;
        this.reportMessage = reportMessage;
        this.reportTitle = reportTitle;
        this.vehicle = "vehicle";
    }

    public Report(@NonNull String reportId, @NonNull String reportStatus, @NonNull String reportMessage, @NonNull String reportTitle, @Nullable Mediafile mediafile) {
        this.reportId = reportId;
        this.reportStatus = reportStatus;
        this.reportMessage = reportMessage;
        this.reportTitle = reportTitle;
        this.mediafile = mediafile;
        this.vehicle = "vehicle";
    }

    public Report(@NonNull String reportId, @NonNull String reportStatus,
                  @NonNull String reportMessage, @NonNull String reportTitle,
                  @Nullable List<ReportLogEntry> reportLogEntries) {
        this.reportId = reportId;
        this.reportStatus = reportStatus;
        this.reportMessage = reportMessage;
        this.reportTitle = reportTitle;
        this.reportLogEntries = reportLogEntries;
        this.vehicle = "vehicle";
    }

    @NonNull
    public String getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(@NonNull String reportStatus) {
        this.reportStatus = reportStatus;
    }

    @NonNull
    public String getReportMessage() {
        return stringFormat(reportMessage) ;
    }

    @NonNull
    public String getReportTitle() {
        return stringFormat(reportTitle);
    }

    @Nullable
    public Mediafile getMediafile() {
        return mediafile;
    }

    public void addInReportLogEntries(@Nullable ReportLogEntry reportLogEntry) {
        if(reportLogEntries == null){
            reportLogEntries = new ArrayList<>();
        }
        this.reportLogEntries.add(reportLogEntry);
    }

    @Nullable
    public List<ReportLogEntry> getReportLogEntries() {
        if(reportLogEntries == null){
            reportLogEntries = new ArrayList<>();
        }
        return reportLogEntries;
    }
}
