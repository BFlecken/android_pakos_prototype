package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Suspicious extends BasePOJO{

    @NonNull
    @SerializedName("message")
    private String message;

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @NonNull
    @SerializedName("transactionId")
    private String transactionId;

    @NonNull
    @SerializedName("timestamp")
    private String timestamp;
}
