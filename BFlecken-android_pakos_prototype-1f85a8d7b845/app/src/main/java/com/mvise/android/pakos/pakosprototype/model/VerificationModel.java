package com.mvise.android.pakos.pakosprototype.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class VerificationModel {

    public static final float DEFAULT_POSITIVE_SCORE = 0.78f;
    public static final int VERIFICATION_FAILS_LIMIT = 3;

    @SerializedName("models")
    @Expose
    private List<String> models = null;

    @SerializedName("scores")
    @Expose
    private List<String> scores = null;
    @SerializedName("adapt")
    @Expose
    private Boolean adapt;
    @SerializedName("error")
    @Expose
    private String error;

    public List<String> getModels() {
        return models;
    }

    public void setModels(List<String> models) {
        this.models = models;
    }

    public void addModel(String model) {
        if(this.models == null) {
            this.models = new ArrayList<>();
        }
        this.models.add(model);
    }

    public List<String> getScores() {
        return scores;
    }

    public void setScores(List<String> scores) {
        this.scores = scores;
    }

    public Boolean getAdapt() {
        return adapt;
    }

    public void setAdapt(Boolean adapt) {
        this.adapt = adapt;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
