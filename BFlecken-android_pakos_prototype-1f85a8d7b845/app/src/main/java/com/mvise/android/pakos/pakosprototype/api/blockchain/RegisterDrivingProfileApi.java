package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.RegisterDrivingProfile;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RegisterDrivingProfileApi {

    @GET("de.vda.registerDrivingProfile")
    Observable<List<RegisterDrivingProfile>> getRegisterDrivingProfileList();

    @POST("de.vda.registerDrivingProfile")
    Completable addRegisterDrivingProfile(@Body RegisterDrivingProfile registerDrivingProfile);

    @GET("de.vda.registerDrivingProfile/{id}")
    Single<RegisterDrivingProfile> getRegisterDrivingProfileById(@Path("id") String id);
}
