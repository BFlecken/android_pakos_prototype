package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Tenant extends CarOwner {

    @Nullable
    @SerializedName("drivingProfile")
    private ArrayList<DrivingProfile> drivingProfile;

    public Tenant(@NonNull String personId) {
        super(personId);
    }

    public Tenant(@NonNull String personID,
                  @Nullable String title,
                  @Nullable String firstName,
                  @Nullable String lastName,
                  @Nullable ArrayList<String> middleName,
                  @Nullable String gender,
                  @Nullable ArrayList<String> nationalities,
                  @Nullable ContactDetails contactDetails,
                  @Nullable BirthDetails birthDetails,
                  @Nullable String id,
                  @Nullable ArrayList<DrivingProfile> drivingProfile) {
        super(personID, title, firstName, lastName, middleName, gender, nationalities, contactDetails, birthDetails, id);
        this.drivingProfile = drivingProfile;
    }


}
