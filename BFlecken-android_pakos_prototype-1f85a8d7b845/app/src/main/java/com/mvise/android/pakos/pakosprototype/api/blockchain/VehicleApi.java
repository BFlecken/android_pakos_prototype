package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.Vehicle;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface VehicleApi {

    @GET("de.vda.Vehicle")
    Observable<List<Vehicle>> getVehicleList();

    @POST("de.vda.Vehicle")
    Completable addVehicle(@Body Vehicle vehicle);

    @GET("de.vda.Vehicle/{id}")
    Single<Vehicle> getVehicleById(@Path("id") String id);

    @HEAD("de.vda.Vehicle/{id}")
    Completable checkVehicle(@Path("id") String id);

    @PUT("de.vda.Vehicle/{id}")
    Completable updateVehicle(@Path("id") String id, @Body Vehicle vehicle);

    @DELETE("de.vda.Vehicle/{id}")
    Completable deleteVehicle(@Path("id") String id);
}
