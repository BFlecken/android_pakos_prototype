package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

;

public class VehicleDetails extends BasePOJO {

    @Nullable
    @SerializedName("make")
    private String make;

    @Nullable
    @SerializedName("modelType")
    private String modelType;

    @Nullable
    @SerializedName("colour")
    private String colour ;

    @Nullable
    @SerializedName("licencePlate")
    private String licencePlate;

    @Nullable
    @SerializedName("v5c")
    private String v5c;

    @Nullable
    @SerializedName("modelVariant")
    private String modelVariant ;

    @Nullable
    @SerializedName("modelVersion")
    private String modelVersion;

    @Nullable
    @SerializedName("bodyType")
    private String bodyType;

    @SerializedName("revenueWeight")
    private int revenueWeight;

    @SerializedName("cylinderCapacity")
    private int cylinderCapacity;

    @SerializedName("co2")
    private int co2;

    @Nullable
    @SerializedName("typeOfFuel")
    private String typeOfFuel;

    @SerializedName("numberOfSeats")
    private int numberOfSeats;

    @SerializedName("numberOfStandingPlaces")
    private int numberOfStandingPlaces;

    @Nullable
    @SerializedName("wheelPlan")
    private String wheelPlan;

    @Nullable
    @SerializedName("vehicleCategory")
    private String vehicleCategory;

    @Nullable
    @SerializedName("typeApprovalNumber")
    private String typeApprovalNumber;

    @SerializedName("maxNetPower")
    private int maxNetPower;

    @Nullable
    @SerializedName("engineNumber")
    private String engineNumber;

    @SerializedName("maxPermissibleMass")
    private int maxPermissibleMass;

    @SerializedName("massInService")
    private int massInService;

    @SerializedName("powerWeightRatio")
    private int powerWeightRatio;

    @Nullable
    @SerializedName("trailerDetails")
    private TrailerDetails trailerDetails;

    @Nullable
    @SerializedName("soundDetails")
    private SoundDetails soundDetails;

    @Nullable
    @SerializedName("exhaustEmissions")
    private ExhaustEmissions exhaustEmissions;

    @Nullable
    @SerializedName("id")
    private String id;


}
