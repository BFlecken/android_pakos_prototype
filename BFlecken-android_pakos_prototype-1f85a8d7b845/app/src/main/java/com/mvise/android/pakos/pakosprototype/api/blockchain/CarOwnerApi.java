package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.CarOwner;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CarOwnerApi {

    @GET("de.pakos.lifecycle.CarOwner")
    Observable<List<CarOwner>> getCarOwnerList();

    @POST("de.pakos.lifecycle.CarOwner")
    Completable addCarOwner(@Body CarOwner carOwner);

    @GET("de.pakos.lifecycle.CarOwner/{id}")
    Single<CarOwner> getCarOwnerById(@Path("id")String id);

    @HEAD("de.pakos.lifecycle.CarOwner/{id}")
    Completable checkCarOwner(@Path("id")String id);

    @PUT("de.pakos.lifecycle.CarOwner/{id}")
    Completable updateCarOwner(@Path("id")String id, @Body CarOwner carOwner);

    @DELETE("de.pakos.lifecycle.CarOwner/{id}")
    Completable deleteCarOwner(@Path("id")String id);
}
