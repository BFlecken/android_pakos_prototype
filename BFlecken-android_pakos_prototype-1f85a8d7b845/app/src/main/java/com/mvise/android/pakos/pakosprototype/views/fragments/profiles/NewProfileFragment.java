package com.mvise.android.pakos.pakosprototype.views.fragments.profiles;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.Model;
import com.mvise.android.pakos.pakosprototype.model.Profile;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;
import com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment;
import com.mvise.android.pakos.pakosprototype.views.custom.ProfileScrollView;

import java.io.File;
import java.util.Objects;

public class NewProfileFragment extends BaseProfileFragment {
    private Profile mProfile;
    private EditText mNameEditText;
    private Profile mProfileData;
    private ImageView mProfilePictureImageView;
    private TextView titleTextView;
    private Toolbar mToolbar;
    private ProfileScrollView mScrollView;

    private static final String BUNDLE_IMAGE_PATH = "IMAGE_PATH";
    private static final String ARG_PROFILE = "profile";

    private String mCurrentImagePath = "";

    public NewProfileFragment(){
    }

    public static NewProfileFragment newInstance(Profile profile){
        NewProfileFragment newProfileFragment = new NewProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_PROFILE, profile);
        newProfileFragment.setArguments(bundle);
        return newProfileFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        int profile;
        if (getArguments() != null){
            mProfile = getArguments().getParcelable(ARG_PROFILE);
        }

        if (mProfile!=null){
            profile = RealmController.with().getProfileId(mProfile.getName());
            Log.wtf("param-val-profile", profile +" profile id extra");
        }else {
            profile = Objects.requireNonNull(RealmController.with().getProfiles().last()).getId() + 1;
        }
        super.setProfileId(profile+"");
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        if (rootView != null) {
            findViews(rootView);
        }
        mToolbar.setTitle("");
        if (getActivity() != null){
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
            Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        titleTextView.setText( mProfile.getName() != null ? mProfile.getName() : "New profile");

        mNameEditText.setEnabled(true);
        if (mProfile!=null){
            profile = RealmController.with().getProfileId(mProfile.getName());
            Log.wtf("param-val-profile", profile +" profile id extra");
            mProfileData = RealmController.with().getProfile(profile +"");
            mNameEditText.setText(mProfile.getName());
        }else {
            profile = Objects.requireNonNull(RealmController.with().getProfiles().last()).getId() + 1;
        }
        mNameEditText.clearFocus();
        mNameEditText.requestFocus();

        if (savedInstanceState == null){
            super.populateUi(profile+"");
            if (RealmController.with().getProfile(profile+"") != null){
                mCurrentImagePath = RealmController.with().getProfile(profile+"")
                        .getImagePath();
                Log.wtf("path0504", mCurrentImagePath+" path");
                if (!TextUtils.isEmpty(mCurrentImagePath)){
                    Glide.with(this)
                            .load(new File(mCurrentImagePath))
                            .into(mProfilePictureImageView);
                }
            }

        }else{
            mCurrentImagePath = savedInstanceState.getString(BUNDLE_IMAGE_PATH);
            if (!TextUtils.isEmpty(mCurrentImagePath)){
                Glide.with(this)
                        .load(new File(mCurrentImagePath))
                        .into(mProfilePictureImageView);
            }
        }

        mScrollView.setOnTouchListener((View v, MotionEvent event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                v.performClick();
            }
            if (mNameEditText.hasFocus()){
                mNameEditText.clearFocus();
            }
            return false;
        });
        return rootView;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_new_profile;
    }

    @Override
    public void onSaveProfile(View view, String profileId){

        String profileName = (mNameEditText.getText()==null || TextUtils.isEmpty(mNameEditText.getText().toString()))
                ? mNameEditText.getHint().toString()
                : mNameEditText.getText().toString();
        int existentProfileId = RealmController.with()
                .getProfileId(profileName);
        if (existentProfileId == -1 || mProfileData != null){
            insertProfile();
            super.saveChanges(profileId);
        } else {
            Toast.makeText(getContext(), R.string.message_profile_name_exists, Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    protected void setProfilePictureFromCar()  {
        String modelString = SharedPrefsUtil.getStringPreference(getContext(),
                SharedPrefsUtil.KEY_MODEL);
        if (!TextUtils.isEmpty(modelString)) {
            Model currentModel = new Gson().fromJson(modelString, Model.class);
            if (!TextUtils.isEmpty(currentModel.getThumbnail())) {
                if (currentModel.getThumbnail().endsWith("\\u003d")){
                    String thumbnail = currentModel.getThumbnail().replace("\\u003d","");
                    currentModel.setThumbnail(thumbnail);
                }
                byte[] decodedString = Base64.decode(currentModel.getThumbnail(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,
                        decodedString.length);
                mProfilePictureImageView.setImageBitmap(decodedByte);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String path = super.getmCurrentPhotoPath();
        if (!TextUtils.isEmpty(path)){
            Glide.with(this)
                    .load(new File(super.getmCurrentPhotoPath()))
                    .into(mProfilePictureImageView);
            mCurrentImagePath = super.getmCurrentPhotoPath();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(BUNDLE_IMAGE_PATH, mCurrentImagePath);
        super.onSaveInstanceState(outState);
    }

    private void findViews(View rootView) {
        mNameEditText = rootView.findViewById(R.id.edit_profile_name);
        mToolbar = rootView.findViewById(R.id.toolbar_basic);
        mProfilePictureImageView = rootView.findViewById(R.id.image_profile_pic);
        mScrollView = rootView.findViewById(R.id.scrollView);
        titleTextView = rootView.findViewById(R.id.profile_title);
    }

    private void insertProfile() {
        Profile profile = new Profile();
        profile.setDetails("Subprofile");
        String profileName = (mNameEditText.getText()==null || TextUtils.isEmpty(mNameEditText.getText().toString()))
                ? mNameEditText.getHint().toString()
                : mNameEditText.getText().toString();

        if (mProfileData == null){
            profile.setName(profileName);
            profile.setOriginalName(profileName);
            int id = Objects.requireNonNull(RealmController.with().getProfiles().last()).getId();
            profile.setId(id+1);
            profile.setImagePath(super.getmCurrentPhotoPath());
            RealmController.with().insertProfile(profile);
        }else{
            RealmController.with()
                    .updateProfile(mProfile.getOriginalName(),
                            profileName, super.getmCurrentPhotoPath());


        }
    }

    public void onTakePhoto(View view) {
        super.displayPopupChooseImage();
    }

}
