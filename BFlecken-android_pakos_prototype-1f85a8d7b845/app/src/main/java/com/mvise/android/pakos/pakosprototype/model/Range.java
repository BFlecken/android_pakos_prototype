package com.mvise.android.pakos.pakosprototype.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Range implements Parcelable {
    @SerializedName("min")
    private double min;

    @SerializedName("max")
    private double max;

    @SerializedName("increment")
    private double increment;

    @SerializedName("start")
    private double start;

    public Range() {
    }

    public Range(int min, int max, double increment, double start) {
        this.min = min;
        this.max = max;
        this.increment = increment;
        this.start = start;
    }

    protected Range(Parcel in) {
        min = in.readDouble();
        max = in.readDouble();
        increment = in.readDouble();
        start = in.readDouble();
    }

    public static final Creator<Range> CREATOR = new Creator<Range>() {
        @Override
        public Range createFromParcel(Parcel in) {
            return new Range(in);
        }

        @Override
        public Range[] newArray(int size) {
            return new Range[size];
        }
    };

    public double getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public double getIncrement() {
        return increment;
    }

    public void setIncrement(double increment) {
        this.increment = increment;
    }

    public double getStart() {
        return start;
    }

    public void setStart(double start) {
        this.start = start;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(min);
        parcel.writeDouble(max);
        parcel.writeDouble(increment);
        parcel.writeDouble(start);
    }
}
