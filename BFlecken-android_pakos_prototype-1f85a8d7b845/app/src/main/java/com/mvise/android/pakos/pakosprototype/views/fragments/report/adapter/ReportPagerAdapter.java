package com.mvise.android.pakos.pakosprototype.views.fragments.report.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.mvise.android.pakos.pakosprototype.api.POJO.Report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReportPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> mTitles = new ArrayList<>(Arrays.asList("OPEN", "IN PROGRESS", "FIXED"));
    private List<String> allCriteria = new ArrayList<>(Arrays.asList("OPEN", "IN_PROGRESS", "DONE"));
    private List<Report> reports;
    private ReportAdaptor openAdaptor;
    private ReportAdaptor inProgressAdaptor;
    private ReportAdaptor doneAdaptor;
    private Fragment mCurrentFragment;

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    public ReportPagerAdapter(FragmentManager fm) {
        super(fm);
        notifyDataSetChanged();
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    private List<Report> extractReports(int index){
        List<Report> sortedList = new ArrayList<>();
        String filterCriteria = allCriteria.get(index);

        if(filterCriteria == null){
            filterCriteria = allCriteria.get(0);
        }

        if(reports != null){
            for(Report report : reports){
                if(filterCriteria.toLowerCase().equals(report.getReportStatus().toLowerCase())){
                    sortedList.add(report);
                }
            }
        }
        return sortedList;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = ((Fragment) object);
        }
        if(mCurrentFragment == openAdaptor) openAdaptor.setReports(extractReports(0));
        if(mCurrentFragment == inProgressAdaptor) inProgressAdaptor.setReports(extractReports(1));
        if(mCurrentFragment == doneAdaptor) doneAdaptor.setReports(extractReports(2));
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case (0):{
                openAdaptor = new ReportAdaptor();
                return openAdaptor;
            }
            case (1):{
                inProgressAdaptor = new ReportAdaptor();
                return inProgressAdaptor;
            }
            case (2):{
                doneAdaptor = new ReportAdaptor();
                return doneAdaptor;
            }
        }
        ReportAdaptor adaptor = new ReportAdaptor();
        return adaptor;
    }

    @Override
    public int getCount() {
        if (mTitles == null) {
            return 0;
        }
        return mTitles.size();
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return mTitles.get(position);
    }
}
