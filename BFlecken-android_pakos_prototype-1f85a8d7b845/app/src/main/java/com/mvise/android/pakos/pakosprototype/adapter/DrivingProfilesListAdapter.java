package com.mvise.android.pakos.pakosprototype.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.Profile;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;

import java.io.File;
import java.lang.ref.WeakReference;

import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;

public class DrivingProfilesListAdapter extends RealmBaseAdapter<Profile> {

    private WeakReference<Context> mContext;
    private RealmResults<Profile> mProfiles;
    private int mActiveProfileId;
    private DrivingProfileListener mProfileListener;

    public interface DrivingProfileListener{
        void onSelected(String profileId, Context context);
    }

    private static class ViewHolder {
        TextView profileName;
        TextView profileDetails;
        ImageView profileIcon;
        ImageView deleteProfile;
        RadioButton activeRadio;
        LinearLayout profileContainer;
        ImageView activationSourceImage;
    }

    public DrivingProfilesListAdapter(RealmResults<Profile> drivingProfiles,
                                      Context context, DrivingProfileListener listener) {
        super( drivingProfiles);
        this.mProfiles = drivingProfiles;
        mContext = new WeakReference<>(context);
        this.mProfileListener = listener;

        mActiveProfileId = SharedPrefsUtil.getIntegerPreference(context, SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, 1);
        notifyDataSetChanged();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Profile profile = getItem(position);
        ViewHolder viewHolder;

        mActiveProfileId = SharedPrefsUtil.getIntegerPreference(mContext.get(), SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, 1);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext.get());
            convertView = inflater.inflate(R.layout.row_driving_profiles_list, parent, false);

            viewHolder.profileName =  convertView.findViewById(R.id.text_profile_name);
            viewHolder.profileDetails =  convertView.findViewById(R.id.text_profile_details);
            viewHolder.profileIcon =  convertView.findViewById(R.id.profile_icon);
            viewHolder.deleteProfile =  convertView.findViewById(R.id.delete_profie_btn);
            viewHolder.activeRadio = convertView.findViewById(R.id.radio_active);
            viewHolder.activationSourceImage = convertView.findViewById(R.id.image_activation_source);
            viewHolder.profileIcon.setClipToOutline(true);
            viewHolder.profileContainer = convertView.findViewById(R.id.container_profile_row);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (profile != null){
            if ( profile.getImagePath() != null && !profile.getImagePath().isEmpty()) {
                Glide.with(mContext.get())
                        .load(new File(profile.getImagePath()))
                        .into(viewHolder.profileIcon);
            }else {
                switch (profile.getId()+""){
                    case Constants.ID_BASIC_PROFILE:
                        Glide.with(mContext.get())
                                .load(R.drawable.vis_profile_basic_2)
                                .into(viewHolder.profileIcon);
                        break;
                    case Constants.ID_SNOW_PROFILE:
                        Glide.with(mContext.get())
                                .load(R.drawable.vis_profile_snow)
                                .into(viewHolder.profileIcon);
                        break;
                    case Constants.ID_WORK_PROFILE:
                        Glide.with(mContext.get())
                                .load(R.drawable.vis_profile_work)
                                .into(viewHolder.profileIcon);
                        break;
                    default:
                        //new profile
                        Glide.with(mContext.get())
                                .load(R.drawable.vis_profile_new)
                                .into(viewHolder.profileIcon);
                        break;
                }
            }
        }

        viewHolder.deleteProfile.setVisibility(View.GONE);
        viewHolder.activeRadio.setVisibility(View.VISIBLE);
        viewHolder.activeRadio.setTag(profile != null ? profile.getId()+ "" : "null");
        if (mActiveProfileId != 0 && profile != null && mActiveProfileId == profile.getId()
                || profile != null && Integer.parseInt(Constants.ID_BASIC_PROFILE) == profile.getId() && mActiveProfileId == 0 ){
            viewHolder.activeRadio.setChecked(true);
            Glide.with(mContext.get())
                    .load(SharedPrefsUtil.getIntegerPreference(mContext.get(), SharedPrefsUtil.KEY_LAST_ACTIVATION_SOURCE, Constants.ACTIVATION_SOURCE_USER) == Constants.ACTIVATION_SOURCE_CAR ?
                            R.drawable.baseline_directions_car_black_48 :
                            R.drawable.baseline_person_outline_black_48)
                    .into(viewHolder.activationSourceImage);
            viewHolder.activationSourceImage.setVisibility(View.VISIBLE);
        }else{
            viewHolder.activeRadio.setChecked(false);
            viewHolder.activationSourceImage.setVisibility(View.GONE);
        }
        viewHolder.activeRadio.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()){
                String tag = compoundButton.getTag().toString();
                mActiveProfileId = Integer.parseInt(tag);
                int previouslyActivatedProfileId = SharedPrefsUtil.getIntegerPreference(mContext.get(), SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, 1);
                if (previouslyActivatedProfileId != mActiveProfileId){
                    mProfileListener.onSelected(tag, mContext.get());
                    SharedPrefsUtil.setIntegerPreference(mContext.get(), SharedPrefsUtil.KEY_ACTIVATED_PROFILE_ID, mActiveProfileId);
                    notifyDataSetChanged();
                }
            }
        });
        viewHolder.activeRadio.setOnClickListener(view -> {
            if (((RadioButton)view).isChecked()){
                SharedPrefsUtil.setIntegerPreference(mContext.get(), SharedPrefsUtil.KEY_LAST_ACTIVATION_SOURCE, Constants.ACTIVATION_SOURCE_USER);
            }
        });

        if (profile != null){
            viewHolder.profileName.setText(profile.getName());
            viewHolder.profileDetails.setText(profile.getDetails());
            viewHolder.deleteProfile.setOnClickListener(v -> RealmController.with().deleteProfile(profile.getName()));
        }

        viewHolder.activeRadio.setEnabled(true);
        return convertView;
    }

    @Override
    public int getCount() {
        return mProfiles.size();
    }


}
