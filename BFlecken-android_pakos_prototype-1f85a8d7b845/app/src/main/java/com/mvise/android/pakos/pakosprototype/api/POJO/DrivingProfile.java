package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DrivingProfile extends BasePOJO{

    @NonNull
    @SerializedName("drivingProfileId")
    private String drivingProfileId;

    @NonNull
    @SerializedName("profileName")
    private String profileName;

    @SerializedName("defaultProfile")
    private boolean defaultProfile;

    @NonNull
    @SerializedName("parameter")
    private List<Parameter> parameters;

    @NonNull
    @SerializedName("drivingProfileRegister")
    private Object drivingProfileRegister;

    public DrivingProfile(@NonNull String drivingProfileId,
                          @NonNull String profileName,
                          boolean defaultProfile,
                          @NonNull List<Parameter>  parameters,
                          @NonNull String drivingProfileRegister) {
        this.drivingProfileId = drivingProfileId;
        this.profileName = profileName;
        this.defaultProfile = defaultProfile;
        this.parameters = parameters;
        this.drivingProfileRegister = drivingProfileRegister;
    }

    @NonNull
    public String getDrivingProfileId() {
        return drivingProfileId;
    }

    @NonNull
    public String getProfileName() {
        return profileName;
    }

    public boolean isDefaultProfile() {
        return defaultProfile;
    }

    @NonNull
    public Object getDrivingProfileRegister() {
        return drivingProfileRegister;
    }

    @NonNull
    public List<Parameter> getParameters() {
        return parameters;
    }
}
