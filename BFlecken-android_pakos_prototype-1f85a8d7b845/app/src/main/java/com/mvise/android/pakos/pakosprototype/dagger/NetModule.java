package com.mvise.android.pakos.pakosprototype.dagger;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    private static final String BASE_URL = "http://ec2-35-159-18-11.eu-central-1.compute.amazonaws.com:3000/api/";

    @Provides
    @Singleton
    Retrofit provideRetrofit() {

        return new Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build();
    }

//    @Provides
//    Observable<List<Had>> getHadActiveata(Retrofit retrofit) {
//        return retrofit
//                .create(HadActivateApi.class)
//                .getHadActivatedList();
//    }

}
