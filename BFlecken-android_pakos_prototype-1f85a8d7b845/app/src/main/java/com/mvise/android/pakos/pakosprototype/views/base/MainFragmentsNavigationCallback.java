package com.mvise.android.pakos.pakosprototype.views.base;

import com.mvise.android.pakos.pakosprototype.api.POJO.Report;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.model.Profile;

import java.util.ArrayList;

public interface MainFragmentsNavigationCallback {
    void onProfileListItemLongClick(Profile profile);
    void onProfileSelected(Profile profile);
    void onJoystickScreenLaunch(ArrayList<JsonParameter> parameters, String pagerGroup, String profileId);
    void onSeatScreenLaunch(ArrayList<JsonParameter> cp, String profileId);
    void onEntertainmentScreenLaunch(ArrayList<JsonParameter> cp, String profileId);
    void onReportsScreenLaunch();
    void onCreateReportScreenLaunch();
    void onReportDetailsScreenLaunch(Report report);
    void onReportUpdateScreenLaunch(Report report);
}
