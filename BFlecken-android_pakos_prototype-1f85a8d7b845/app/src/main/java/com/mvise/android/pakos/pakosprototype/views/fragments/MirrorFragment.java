package com.mvise.android.pakos.pakosprototype.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.adapter.JoystickPagerAdapter;
import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.utils.JoystickValueUpdateListener;
import com.mvise.android.pakos.pakosprototype.views.custom.StationaryViewPager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MirrorFragment extends Fragment implements JoystickValueUpdateListener {
    public static final String ARG_PARAMETERS = "PARAMETERS";
    public static final String ARG_GROUP_TITLE = "GROUP_TITLE";
    public static final String ARG_PROFILE_ID = "PROFILE_ID";
    private static final String TAG = MirrorFragment.class.getSimpleName();

    private List<JsonParameter> mJsonParameters;
    private List<ComponentParameter> mComponentParameters;
    private List<String> mPagerGroups = new ArrayList<>();
    private String mTitle;
    private String mProfileId;

    private StationaryViewPager mViewPager;
    private TextView mTitleTextView;
    private TabLayout mTabLayout;
    private Toolbar mToolbar;

    public MirrorFragment(){
    }

    public static MirrorFragment newInstance(List<JsonParameter> parameters, String title, String profileId){
        MirrorFragment mirrorFragment = new MirrorFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ARG_PARAMETERS, (ArrayList<JsonParameter>)parameters);
        bundle.putString(ARG_GROUP_TITLE, title);
        bundle.putString(ARG_PROFILE_ID, profileId);
        mirrorFragment.setArguments(bundle);
        return mirrorFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            mJsonParameters = getArguments().getParcelableArrayList(ARG_PARAMETERS);
            mTitle = getArguments().getString(ARG_GROUP_TITLE);
            mProfileId = getArguments().getString(ARG_PROFILE_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mirror, container, false);
        findViews(rootView);

        if (mJsonParameters != null){
            mComponentParameters  = new ArrayList<>();
            List<ComponentParameter> allCompParams = RealmController.with().getParamValues();
            for (JsonParameter parameter: mJsonParameters){
                ComponentParameter cp = null;
                if (allCompParams != null){
                    cp = getCompFromList(allCompParams, parameter.getParameter());
                }
                if (cp == null){
                    cp = new ComponentParameter();
                    cp.setName(parameter.getName());
                    cp.setParameter(parameter.getParameter());
                    cp.setProfile(Integer.valueOf(mProfileId));
                    cp.setValue("0");
                }

                mComponentParameters.add(cp);
                if (!isExistentPagerGroup(parameter.getPagerGroup())){
                    mPagerGroups.add(parameter.getPagerGroup());
                }
            }
        }

        if (!TextUtils.isEmpty(mTitle)){
            mTitleTextView.setText(mTitle);
        }

        JoystickPagerAdapter adapter = new JoystickPagerAdapter(
                getChildFragmentManager(),
                mPagerGroups,
                mJsonParameters,
                mProfileId,
                this,
                mComponentParameters
                );
        // Set the adapter onto the view pager
        mViewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        mTabLayout.setupWithViewPager(mViewPager);

        mToolbar.setTitle("");
        if (getActivity() != null){
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
            Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        if (getActivity() != null){
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
            Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
        }


        return rootView;
    }

    private void findViews(View rootView) {
        mViewPager = rootView.findViewById(R.id.viewpager);
        mTitleTextView = rootView.findViewById(R.id.text_toolbar);
        mTabLayout = rootView.findViewById(R.id.sliding_tabs);
        mToolbar = rootView.findViewById(R.id.toolbar);
    }

    private ComponentParameter getCompFromList(List<ComponentParameter> allCompParams, String parameter) {
        for (ComponentParameter cp: allCompParams){
            if (cp.getParameter().equals(parameter) && cp.getProfile() == Integer.valueOf(mProfileId)){
                return cp;
            }
        }
        return null;
    }

    private boolean isExistentPagerGroup(String pagerGroup) {
        if (mPagerGroups == null) {
            return false;
        }
        for (String group: mPagerGroups){
            if (group.equals(pagerGroup)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onValueChanged(ComponentParameter value) {
       updateComponentDbValue(value);
    }

    private void updateComponentDbValue(ComponentParameter componentParameter) {
        Log.wtf(TAG,"updating parameter in database, new value: " + "id: "+ componentParameter.getProfile()+", "+componentParameter.getParameter()
                +", "+componentParameter.getValue());

        RealmController.with().updateComponentParameter(
                componentParameter.getName(),
                componentParameter.getValue(), componentParameter.getType(),
                componentParameter.getParameter(),
                Integer.parseInt(mProfileId));
    }
}