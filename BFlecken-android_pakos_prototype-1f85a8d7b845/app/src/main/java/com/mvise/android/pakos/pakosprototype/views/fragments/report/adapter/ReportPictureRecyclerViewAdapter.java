package com.mvise.android.pakos.pakosprototype.views.fragments.report.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mvise.android.pakos.pakosprototype.App;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Mediafile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ReportPictureRecyclerViewAdapter extends RecyclerView.Adapter<ReportPictureRecyclerViewAdapter.ViewHolder> {

    private List<Mediafile> mValues;
    private boolean viewMode;

    public ReportPictureRecyclerViewAdapter(Mediafile mediafile, boolean viewMode) {
        mValues = new ArrayList<>();
        if(mediafile != null ) mValues.add(mediafile);
        this.viewMode = viewMode;
    }

    public ReportPictureRecyclerViewAdapter(List<Mediafile> mediaFiles, boolean viewMode) {
        mValues = new ArrayList<>();
        if(mediaFiles != null ) mValues = mediaFiles;
        this.viewMode = viewMode;
    }

    public List<Mediafile> getMediafiles() {
        return mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_fragment_picture_item, parent, false);
        return new ViewHolder(view);
    }

    public void addElement(Mediafile mediafile){
        if(mediafile != null){
            mValues.add(mediafile);
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.mContentView.setText(holder.mItem.getLocation().substring(holder.mItem.getLocation().lastIndexOf("/") + 1));

        if("".equals(holder.mItem.getHash())){
            Glide.with(App.getAppContext())
                    .load(new File(holder.mItem.getLocation()))
                    .into(holder.mImageView);
        }
        else{
            Glide.with(App.getAppContext())
                    .load(android.util.Base64.decode(holder.mItem.getHash(), android.util.Base64.DEFAULT))
                    .asBitmap()
                    .into(holder.mImageView);
        }

        if(viewMode){
            holder.mRemoveItem.setImageDrawable(App.getAppContext().getDrawable(R.drawable.ic_search));
            holder.mRemoveItem.setOnClickListener((View v) ->{
               System.out.println("Implement view mode");
            });
        }
        else{
            holder.mRemoveItem.setOnClickListener((View v) ->{
                mValues.remove(position);
                notifyDataSetChanged();
            });
        }

        holder.mView.setOnClickListener((View v) -> {

            }
        );
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mContentView;
        public final ImageView mRemoveItem;
        public Mediafile mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.imageView4);
            mContentView = view.findViewById(R.id.content);
            mRemoveItem = view.findViewById(R.id.removeItem);
        }

       }
}
