package com.mvise.android.pakos.pakosprototype.views.fragments.report.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReportAdaptor extends Fragment {

    private List<String> allCriteria = new ArrayList<>(Arrays.asList("OPEN", "IN_PROGRESS", "DONE"));
    private static ReportRecyclerViewAdapter reportRecyclerViewAdapter = new ReportRecyclerViewAdapter();



    public ReportAdaptor() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.report_fragment_list_view, container, false);

        if (view instanceof RecyclerView) {
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setAdapter(reportRecyclerViewAdapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setReports(List<Report> reports) {
        // TODO: Make the loading for the unloaded elements
        if(reports != null){
            reportRecyclerViewAdapter.setmValues(reports);
            reportRecyclerViewAdapter.notifyDataSetChanged();
        }
    }
}
