package com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mvise.android.pakos.pakosprototype.App;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Mediafile;
import com.mvise.android.pakos.pakosprototype.api.POJO.Report;
import com.mvise.android.pakos.pakosprototype.api.POJO.ReportLogEntry;
import com.mvise.android.pakos.pakosprototype.api.blockchain.ReportApi;
import com.mvise.android.pakos.pakosprototype.utils.MediaUtils;
import com.mvise.android.pakos.pakosprototype.utils.ReportUtils;
import com.mvise.android.pakos.pakosprototype.utils.TakePictureUtil;
import com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;
import static com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment.CAMERA_REQUEST_CODE;
import static com.mvise.android.pakos.pakosprototype.views.base.BaseProfileFragment.GALLERY_REQUEST_CODE;


public class ReportUpdateFragment extends Fragment {

    private ImageView cancelUpdateImageView;
    private ImageView editReportButton;
    private Button editReportButtonLower;
    private EditText descriptionEditText;
    private EditText statusChangeButton;
    private EditText reportUpdateCommentEditText;
    private ReportUtils reportUtils;
    private Button uploadNewFileButton;
    private TakePictureUtil takePictureUtil;
    private ReportPictureFragment updateListPicture;
    private Context context;

    private static final String REPORT = "report";
    private Report report;


    @Inject
    Retrofit retrofitInstance;

    public ReportUpdateFragment() {
        // Required empty public constructor
    }

    public static ReportUpdateFragment newInstance(Report report) {
        ReportUpdateFragment fragment = new ReportUpdateFragment();
        Bundle args = new Bundle();
        args.putSerializable(REPORT, report);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            report = (Report) getArguments().getSerializable(REPORT);
        }

        // Initialize the utils used in this class
        reportUtils = new ReportUtils();
        takePictureUtil = new TakePictureUtil(this, getContext());

        App.getDaggerComponent().inject(this);
        context = getContext();
        initFragments();

    }

    private void initFragments(){
        // Init old pictures
        ReportPictureFragment currentListPicture = new ReportPictureFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(currentListPicture.MEDIA_FILE, report.getMediafile());
        bundle.putSerializable(currentListPicture.VIEW_MODE, true);
        currentListPicture.setArguments(bundle);

        // Init what images to be added in comment
        updateListPicture = new ReportPictureFragment();

        // Add the fragments in transaction
        if(getFragmentManager() != null){
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.update_pictures_list, currentListPicture);
            transaction.add(R.id.update_new_files, updateListPicture);
            transaction.commit();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.report_fragment_update, container, false);
        // Inflate the layout for this currentListPicture
        findViews(rootView);
        setView();

        return rootView;
    }

    private void findViews(View rootView) {
        cancelUpdateImageView = rootView.findViewById(R.id.cancelCreate);
        editReportButton = rootView.findViewById(R.id.edit_current_report);
        descriptionEditText = rootView.findViewById(R.id.description_edit_text);
        statusChangeButton = rootView.findViewById(R.id.change_status);
        uploadNewFileButton = rootView.findViewById(R.id.upload_new_pictures);
        editReportButtonLower = rootView.findViewById(R.id.edit_current_report_lower_button);
        reportUpdateCommentEditText = rootView.findViewById(R.id.comment_edit_text);
    }

    private void setView(){
        cancelUpdateImageView.setOnClickListener(v -> MainNavigationActivity.getInstance().onBackPressed());
        editReportButton.setOnClickListener(v -> updateReport());
        editReportButtonLower.setOnClickListener(v -> updateReport());
        descriptionEditText.setText(report.getReportMessage());
        statusChangeButton.setText(reportUtils.getStatusList().get(report.getReportStatus()));
        statusChangeButton.setOnClickListener(v -> reportUtils.showChangeDialog(getContext(), statusChangeButton));
        uploadNewFileButton.setOnClickListener(v -> takePictureUtil.onTakePhoto());
    }

    @SuppressLint("CheckResult")
    public void updateReport(){
        editReportButton.setEnabled(false);
        editReportButtonLower.setEnabled(false);
        try{
            String status = statusChangeButton.getText().toString();
            String commentText = reportUpdateCommentEditText.getText().toString();
            report.setReportStatus(reportUtils.getStatusListReversed(status));
            ReportLogEntry reportLogEntry;
            Gson gson = new Gson();
            try{

                SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US);

                // Add comment with images, text and when
                reportLogEntry = new ReportLogEntry(commentText,parser.format(Calendar.getInstance(TimeZone.getDefault()).getTime()), gson.toJson(updateListPicture.getMediaFiles()));
            }
            catch (Exception e){
                reportLogEntry = new ReportLogEntry(commentText,Calendar.getInstance(TimeZone.getDefault()).getTime().toString(), gson.toJson(updateListPicture.getMediaFiles()));
            }

            report.addInReportLogEntries(reportLogEntry);
            retrofitInstance.create(ReportApi.class)
                    .updateReport(report.getReportId(), report)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe( () ->{
                            try{
                                Toast.makeText(getContext(), R.string.success, Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e){
                                // Normal error
                            }
                                MainNavigationActivity.getInstance().onBackPressed(true);
                            },
                            (throwable) -> {
                                try{
                                    Toast.makeText(getContext(), R.string.fail, Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                    // Normal error need to be log sometime
                                }

                                throwable.printStackTrace();
                            });


        }
        catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getContext(),R.string.general_error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:{
                if (resultCode == RESULT_OK) {
                    try{
                        String path = takePictureUtil.getPicUri().getPath();
                        if(path != null){
                            Mediafile mediafile = Mediafile.createMediaFile(path);
                            updateListPicture.addMediafile(mediafile);
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                break;
            }
            case GALLERY_REQUEST_CODE:{
                if (resultCode == RESULT_OK) {
                    try{
                        String path = MediaUtils.getPath(context, data.getData());
                        if(path != null){
                            Mediafile mediafile = Mediafile.createMediaFile(path);
                            updateListPicture.addMediafile(mediafile);
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                break;
            }

        }
    }
}
