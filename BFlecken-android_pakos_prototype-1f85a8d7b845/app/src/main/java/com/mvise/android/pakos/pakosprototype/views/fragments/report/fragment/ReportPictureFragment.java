package com.mvise.android.pakos.pakosprototype.views.fragments.report.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Mediafile;
import com.mvise.android.pakos.pakosprototype.views.fragments.report.adapter.ReportPictureRecyclerViewAdapter;

import java.util.List;

public class ReportPictureFragment extends Fragment {
    private ReportPictureRecyclerViewAdapter adapter;
    public String MEDIA_FILE = "mediaFile";
    public String VIEW_MODE = "viewMode";

    public ReportPictureFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.report_fragment_picture_list, container, false);

            if (view instanceof RecyclerView) {
                RecyclerView recyclerView = (RecyclerView) view;
                Mediafile mediafile = null;
                boolean viewMode = false;

                try{
                     mediafile = (Mediafile) getArguments().getSerializable(MEDIA_FILE);
                     viewMode = getArguments().getBoolean(VIEW_MODE);
                }
                catch (Exception e){
                    // Normal error
                }

                adapter = new ReportPictureRecyclerViewAdapter(mediafile, viewMode);

                recyclerView.setAdapter(adapter);
            }

        return view;
    }

    public List<Mediafile> getMediaFiles(){
        return adapter.getMediafiles();
    }

    public void addMediafile (Mediafile mediafile){
        adapter.addElement(mediafile);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
