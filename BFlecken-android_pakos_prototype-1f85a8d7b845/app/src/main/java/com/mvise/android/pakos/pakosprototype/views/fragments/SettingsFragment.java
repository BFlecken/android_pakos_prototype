package com.mvise.android.pakos.pakosprototype.views.fragments;

import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mvise.android.pakos.pakosprototype.App;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.FaceIDApiClient;
import com.mvise.android.pakos.pakosprototype.api.FaceIDApiInterface;
import com.mvise.android.pakos.pakosprototype.jobs.JobSchedulerUtil;
import com.mvise.android.pakos.pakosprototype.model.Model;
import com.mvise.android.pakos.pakosprototype.utils.DrivingProfileSync;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;
import com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity;

import java.util.Objects;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity.SETTINGS_FRAGMENT_TAG;

public class SettingsFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    private static final String TAG = SettingsFragment.class.getSimpleName();

    private EditText mMeasurementPcIp;
    private EditText mMeasurementPcPort;
    private EditText mCamIp;
    private EditText mCamPort;
    private SwitchCompat mCycleVerificationSwitch;
    private SwitchCompat mCyclicAdaptationSwitch;
    private SwitchCompat automatedSyncToCloud;
    private TextView mTvModelAvailability;
    private Button mInitiateFaceModelBtn;
    private Button syncCloudButton;
    private Button restoreCloudButton;
    private EditText mEditIntervalVal;
    private String mModelJson;
    private DrivingProfileSync drivingProfileSync;

    private Button mSaveMeasurementPcDetails;
    private Button mSaveCameraDetails;

    private Button mSaveReceivingDetails;
    private EditText mReceivingPortEditText;
    private Toolbar mToolbar;

    public SettingsFragment(){
    }

    @Inject
    Retrofit retrofitInstance;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        findViews(rootView);
        if (getActivity() != null){
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
            Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        App.getDaggerComponent().inject(this);
        drivingProfileSync = new DrivingProfileSync(retrofitInstance, getContext(), this);

        mInitiateFaceModelBtn.setOnClickListener(v -> initFaceModel());
        getSavedSettings();
        mSaveCameraDetails.setOnClickListener(v -> verifyCameraData());
        mSaveMeasurementPcDetails.setOnClickListener(v -> verifyPcData());
        mSaveReceivingDetails.setOnClickListener(view -> verifyReceivingDetails());
        syncCloudButton.setOnClickListener(v -> onSyncCloudButton());
        restoreCloudButton.setOnClickListener(v -> onRestoreCloudButton());
        automatedSyncToCloud.setOnCheckedChangeListener((buttonView, isChecked) ->{
            if(isChecked){
                new AlertDialog.Builder(getContext())
                        .setTitle("Automated sync.")
                        .setMessage("A backup of your driving profiles will be synchronized to the cloud on a regular basis.")
                        .setCancelable(true)
                        .setNegativeButton("OK", (dialog,which) -> dialog.cancel())
                        .show();
            }

            SharedPrefsUtil.setBooleanPreference(getContext(), SharedPrefsUtil.KEY_AUTOMATIC_SYNC_TO_CLOUD, isChecked);
        } );

        return rootView;
    }

    private void onSyncCloudButton(){

        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle("Sync. Driving profiles")
                .setMessage("A backup of your diving profiles will be synchronized to the cloud. Existing backup will be overwritten. Continue?")
                .setCancelable(true)
                .setPositiveButton("SYNC.",null)
                .setNegativeButton("CANCEL", (dialog,which) -> dialog.cancel())
                .show();

        Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(view -> drivingProfileSync.syncToCloud(alertDialog));
    }

    private void onRestoreCloudButton(){
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setMessage("Existing driving profiles will be replaced by the last backup. All changes to current profiles will be lost. Continue?")
                .setTitle("Restore driving profiles")
                .setCancelable(true)
                .setPositiveButton("RESTORE", null)
                .setNegativeButton("CANCEL", (dialog,which) -> dialog.cancel())
                .show();

        Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(view -> drivingProfileSync.restoreFromCloud(alertDialog));
    }

    public void refreshFragment(){
        MainNavigationActivity.getInstance().onBackPressed();
        MainNavigationActivity.getInstance().createSettingsFragment();
        MainNavigationActivity.getInstance().getmNavigationView().setSelectedItemId(R.id.action_settings);
    }

    private void getSavedSettings() {
        boolean isCylicVerificationEnabled = SharedPrefsUtil.getBooleanPreference(getContext(),
                SharedPrefsUtil.KEY_PERIODIC_VERIFICATION, false);
        boolean isAdaptionEnabled = SharedPrefsUtil.getBooleanPreference(getContext(),
                SharedPrefsUtil.KEY_ADAPTION_ENABLED, false);

        int interval = SharedPrefsUtil.getIntegerPreference(getContext(), SharedPrefsUtil.KEY_VERIFICATION_INTERVAL,
                getResources().getInteger(R.integer.interval_default));

        mEditIntervalVal.append(String.valueOf(interval));
        mCycleVerificationSwitch.setChecked(isCylicVerificationEnabled);
        mCyclicAdaptationSwitch.setEnabled(isCylicVerificationEnabled);
        mCyclicAdaptationSwitch.setChecked(isCylicVerificationEnabled && isAdaptionEnabled);

        mCycleVerificationSwitch.setOnCheckedChangeListener(this);
        mCyclicAdaptationSwitch.setOnCheckedChangeListener(this);

        String value = SharedPrefsUtil.getStringPreference(getContext(), SharedPrefsUtil.KEY_MEASUREMENT_PC_IP_ADDRESS);
        if (value!=null && !value.equals("")){
            mMeasurementPcIp.setText(value);
        }

        value = SharedPrefsUtil.getStringPreference(getContext(), SharedPrefsUtil.KEY_MEASUREMENT_PC_PORT);
        if (value!=null && !value.equals("")){
            mMeasurementPcPort.setText(value);
        }

        String url = SharedPrefsUtil.getStringPreference(getContext(), SharedPrefsUtil.KEY_CAM_URL);
        value = SharedPrefsUtil.getStringPreference(getContext(), SharedPrefsUtil.KEY_CAMERA_IP_ADDRESS);
        if (!TextUtils.isEmpty(value)){
            mCamIp.setText(value);
        }else if (!TextUtils.isEmpty(url)){
            mCamIp.setText(url);
        }else{
            mCamIp.setText(FaceIDApiClient.BASE_URL);
        }

        value = SharedPrefsUtil.getStringPreference(getContext(), SharedPrefsUtil.KEY_CAMERA_PORT);
        mCamPort.setText(value);

        mModelJson = SharedPrefsUtil.getStringPreference(getContext(), SharedPrefsUtil.KEY_MODEL);
        if (TextUtils.isEmpty(mModelJson)) {
            mTvModelAvailability.setText(getString(R.string.model_data_availability, getString(R.string.not_available)));
        } else {
            mTvModelAvailability.setText(getString(R.string.model_data_availability, getString(R.string.available)));
        }

        value = SharedPrefsUtil.getStringPreference(getContext(), SharedPrefsUtil.KEY_RECEIVING_PORT);
        mReceivingPortEditText.setText(value);
        automatedSyncToCloud.setChecked(SharedPrefsUtil.getBooleanPreference(getContext(), SharedPrefsUtil.KEY_AUTOMATIC_SYNC_TO_CLOUD, false));
    }

    private void findViews(View rootView) {
        mMeasurementPcIp =rootView.findViewById(R.id.measurement_pc_ip);
        mMeasurementPcPort =  rootView.findViewById(R.id.edit_measurement_port);
        mCamIp =  rootView.findViewById(R.id.edit_cam_ip);
        mCamPort = rootView.findViewById(R.id.edit_cam_port);
        mSaveCameraDetails =  rootView.findViewById(R.id.btn_save_cam_details);
        mSaveMeasurementPcDetails =  rootView.findViewById(R.id.btn_save_pc_details);
        mTvModelAvailability = rootView.findViewById(R.id.model_data_availability);
        mCycleVerificationSwitch = rootView.findViewById(R.id.cyclic_user_verification);
        mCyclicAdaptationSwitch =  rootView.findViewById(R.id.cyclic_model_adaptation);
        mInitiateFaceModelBtn = rootView.findViewById(R.id.button_init_face_model);
        mEditIntervalVal = rootView.findViewById(R.id.edit_interval_value);
        mSaveReceivingDetails = rootView.findViewById(R.id.btn_save_receive_details);
        mReceivingPortEditText = rootView.findViewById(R.id.edit_receive_port);
        mToolbar = rootView.findViewById(R.id.toolbar);
        automatedSyncToCloud = rootView.findViewById(R.id.automate_sync_to_cloud_check_box);
        syncCloudButton = rootView.findViewById(R.id.button_sync_cloud);
        restoreCloudButton = rootView.findViewById(R.id.button_restore_from_cloud);


    }

    private void verifyPcData() {
        if (mMeasurementPcIp.getText().toString().equals("") ||
                mMeasurementPcPort.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please complete both the PC IP and Port!",
                    Toast.LENGTH_LONG).show();
        }else{
            saveData();
        }
    }

    private void verifyCameraData() {
        if (mCamIp.getText().toString().equals("") ||
                (mCamPort.getText().toString().equals("") &&
                        Patterns.IP_ADDRESS.matcher(mCamIp.getText().toString()).matches())) {
            Toast.makeText(getContext(), "Please complete both the Camera IP and Port, or Camera URL!",
                    Toast.LENGTH_LONG).show();
        }else{
            saveData();
        }
    }

    private void verifyReceivingDetails() {
        if (mReceivingPortEditText.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please complete the Port for Data Receiving!",
                    Toast.LENGTH_LONG).show();
        }else{
            saveData();
        }
    }

    private void applySettings() {
        if (!TextUtils.isEmpty(mModelJson)) {
            JobSchedulerUtil.enableCyclicJob(getContext(), mCycleVerificationSwitch.isChecked());
        }
    }

    private boolean isValidInterval() {
        boolean isCyclicVerificationEnabled = SharedPrefsUtil.getBooleanPreference(getContext(),
                SharedPrefsUtil.KEY_PERIODIC_VERIFICATION, false);
        if (!isCyclicVerificationEnabled) {
            return true;
        }

        String intervalValue = mEditIntervalVal.getText().toString();
        if (!TextUtils.isEmpty(intervalValue)) {
            int interval = Integer.valueOf(intervalValue);
            if (interval >= 120 && interval <= 300) {
                saveInterval(interval);
                return true;
            }
        }

        Toast.makeText(getContext(), getString(R.string.interval_validation_rules),
                Toast.LENGTH_LONG).show();
        return false;
    }


    private void initFaceModel() {
        String baseUrl ;

        baseUrl = CarConnectionFragment.makeBaseUrl(getContext());
        FaceIDApiInterface faceIDApiService =
                FaceIDApiClient.getClient(baseUrl)
                        .create(FaceIDApiInterface.class);

        Call<Model> call = faceIDApiService.enroll(FaceIDApiClient.getApiKey());
        call.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(@NonNull Call<Model> call, @NonNull Response<Model> response) {
                Model modelResponse = response.body();
                Log.d(TAG, "faceid enroll response: " + modelResponse);
                if (modelResponse != null) {
                    if (!TextUtils.isEmpty(modelResponse.getModel())) {
                        Toast.makeText(getContext(),
                                R.string.message_success_face_model_enroll, Toast.LENGTH_LONG).show();
                        SharedPrefsUtil.setStringPreference(getContext(),
                                SharedPrefsUtil.KEY_MODEL, new Gson().toJson(modelResponse));
                        mTvModelAvailability.setText(getString(R.string.model_data_availability,
                                getString(R.string.available)));
                    } else if (!TextUtils.isEmpty(modelResponse.getError())) {
                        Toast.makeText(getContext(),
                                getString(R.string.error_retrieving_model)
                                + modelResponse.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(),
                            getString(R.string.error_retrieving_model_null), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model> call, @NonNull Throwable error) {
                Log.e(TAG, "initializing model error: "+ error.toString());
                Toast.makeText(getContext(),
                        getString(R.string.error_retrieving_model) + error.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int idSwitch = (buttonView.getId());
        switch (idSwitch) {
            case R.id.cyclic_user_verification:
                enablePeriodicVerificationOption(isChecked);
                break;

            case R.id.cyclic_model_adaptation:
                enableModelAdaptionOption(isChecked);
                break;

            default:
                break;
        }
    }

    private void enablePeriodicVerificationOption(boolean isChecked) {
        mCyclicAdaptationSwitch.setEnabled(isChecked);
        SharedPrefsUtil.setBooleanPreference(getContext(),
                SharedPrefsUtil.KEY_PERIODIC_VERIFICATION, isChecked);
        if (!isChecked) {
            enableModelAdaptionOption(false);
        }
    }

    private void enableModelAdaptionOption(boolean isChecked) {
        SharedPrefsUtil.setBooleanPreference(getContext(),
                SharedPrefsUtil.KEY_ADAPTION_ENABLED, isChecked);
    }

    private void saveInterval(int interval) {
        SharedPrefsUtil.setIntegerPreference(getContext(),
                SharedPrefsUtil.KEY_VERIFICATION_INTERVAL, interval);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            saveData();
            if (isValidInterval()) {
                applySettings();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveData() {
        Toast.makeText(getContext(), R.string.message_connection_settings_saved, Toast.LENGTH_SHORT).show();
        String value = mMeasurementPcIp.getText().toString();
        SharedPrefsUtil.setStringPreference(getContext(), SharedPrefsUtil.KEY_MEASUREMENT_PC_IP_ADDRESS, value);

        value = mMeasurementPcPort.getText().toString();
        SharedPrefsUtil.setStringPreference(getContext(), SharedPrefsUtil.KEY_MEASUREMENT_PC_PORT, value);

        value = mCamIp.getText().toString();
        if (Patterns.IP_ADDRESS.matcher(value).matches()){
            SharedPrefsUtil.setStringPreference(getContext(), SharedPrefsUtil.KEY_CAMERA_IP_ADDRESS, value);
            SharedPrefsUtil.setStringPreference(getContext(), SharedPrefsUtil.KEY_CAM_URL, "");
        }else{
            SharedPrefsUtil.setStringPreference(getContext(), SharedPrefsUtil.KEY_CAM_URL, value);
            SharedPrefsUtil.setStringPreference(getContext(), SharedPrefsUtil.KEY_CAMERA_IP_ADDRESS, "");
        }

        value = mCamPort.getText().toString();
        SharedPrefsUtil.setStringPreference(getContext(), SharedPrefsUtil.KEY_CAMERA_PORT, value);

        value = mReceivingPortEditText.getText().toString();
        SharedPrefsUtil.setStringPreference(getContext(),SharedPrefsUtil.KEY_RECEIVING_PORT, value);
    }

}