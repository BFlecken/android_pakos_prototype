package com.mvise.android.pakos.pakosprototype.views.base;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.model.Profile;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.udp.UdpUtils;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

import static com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment.DURATION_BETWEEN_PROFILES;
import static com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment.RC_VERIFICATION;
import static com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment.lcs;
import static com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment.personaParams;

public abstract class UdpCommunicationActivity extends AppCompatActivity
        implements UdpCommunicationListener, CarConnectionListener {
    private static final String TAG = UdpCommunicationActivity.class.getSimpleName() ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        boolean isSignedIn = SharedPrefsUtil.getBooleanPreference(
                this,
                SharedPrefsUtil.KEY_SIGNED_IN,
                false);
        Log.wtf(TAG, "SIGNED IN = "+isSignedIn);
        switch (id){
            case R.id.send:
                if (isSignedIn) {
                    sendAllProfilesWithDelay();
                }
                return true;
            case R.id.sign_in:
                if (!isSignedIn) {
                    loadDriverVerification();
                    invalidateOptionsMenu();
                }else{
                    showSignOutDialog(false);
                }
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void sendAllProfilesWithDelay() {
        RealmResults<Profile> profiles = RealmController.with().getProfiles();
        for (Profile profile : profiles) {
            List<JsonParameter> paramsList = (profile.getId() + "").equals(Constants.ID_PERSONA) ? personaParams : lcs;
            Log.wtf(TAG, "loading profile with id = "+profile.getId());
            if (paramsList == null || paramsList.size() == 0) {
                Toast.makeText(this,
                        R.string.message_wait_send_all,
                        Toast.LENGTH_SHORT).show();
            } else {
                List<String> profileDataToSend =
                        loadProfileData(profile.getId()+"");
                Handler handler = new Handler();
                handler.postDelayed(() -> UdpUtils.sendBroadcast(profileDataToSend,
                        UdpCommunicationActivity.this,
                        UdpCommunicationActivity.this), DURATION_BETWEEN_PROFILES);
            }
        }
    }

    public abstract void loadDriverVerification();

    private static List<String> loadProfileData(String profile) {

        List<String> tempProfileToSend= new ArrayList<>();
        tempProfileToSend.add(profile);

        List<JsonParameter> params = profile.equals(Constants.ID_PERSONA)?personaParams:lcs;
        for (JsonParameter param: params){
            String name= param.getParameter();
            if (param.getType().equals(Constants.PARAM_TYPE_BOOLEAN)){
                ComponentParameter componentParameter = RealmController.with()
                        .getComponentParameter(name,Integer.parseInt(profile));
                if (componentParameter == null){
                    componentParameter = RealmController.with()
                            .getComponentParameter(name,Integer.parseInt(profile));
                    if (componentParameter == null){
                        tempProfileToSend.add("0");
                    }else{
                        if (componentParameter.getValue().equals(true+"")|| componentParameter.getValue().equals(false+"")){
                            tempProfileToSend.add(componentParameter.getValue().equals(true+"")?1+"":0+"");
                        }else tempProfileToSend.add(0+"");
                    }
                }else{
                    tempProfileToSend.add(componentParameter.getValue().equals(true+"")?1+"":0+"");
                }

            }else if (param.getType().equals(Constants.PARAM_TYPE_INT)){
                ComponentParameter componentParameter = RealmController.with()
                        .getComponentParameter(name, Integer.parseInt(profile));
                if (componentParameter == null || componentParameter.getValue() == null){
                    tempProfileToSend.add("0");
                }else{
                    try{
                        double testD = Double.parseDouble(componentParameter.getValue());
                        tempProfileToSend.add(testD+"");
                    }catch (NumberFormatException e){
                        e.printStackTrace();
                        tempProfileToSend.add("0");
                    }
                }
            }
        }

        Log.wtf(TAG, "temp data to be sent - list size - "+tempProfileToSend.size());

        return tempProfileToSend;

    }

    public void showSignOutDialog(final boolean exitingApp){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(R.string.title_sign_out)
                .setMessage(R.string.message_sign_out)
                .setPositiveButton(R.string.action_yes, (dialog, which) -> {
                    onSignOut();
                    if (exitingApp) {
                        finish();
                    }
                    invalidateOptionsMenu();
                })
                .setNegativeButton(R.string.action_no, (dialog, which) -> {
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_VERIFICATION) {
            if (resultCode == RESULT_OK) {
                sendAllProfilesWithDelay();
                invalidateOptionsMenu();

            }
        }
    }

    public void onVerificationSuccess(){
        sendAllProfilesWithDelay();
        invalidateOptionsMenu();
    }


}
