package com.mvise.android.pakos.pakosprototype.udp;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

class UdpReceiverUtils {

    private static final String TAG = "UdpReceiverUtils";
    private Handler mHandler;
    private Context mContext;
    private UdpReceiveAsync mReceiverAsync;

    UdpReceiverUtils(Handler handler, Context context){
        mHandler = handler;
        mContext = context;
    }

    void execute() {
        Log.wtf(TAG,"Starting server here to receive messages!");
        if (mReceiverAsync == null){
            mReceiverAsync = new UdpReceiveAsync();
        }
        if (!mReceiverAsync.getStatus().equals(AsyncTask.Status.RUNNING)){
            mReceiverAsync.setmHandler(mHandler);
            mReceiverAsync.execute(mContext);
        }

    }

    void stop(){
        if (mReceiverAsync != null && !mReceiverAsync.isCancelled()){
            mReceiverAsync.cancel(true);
        }
        mReceiverAsync = null;
    }
}
