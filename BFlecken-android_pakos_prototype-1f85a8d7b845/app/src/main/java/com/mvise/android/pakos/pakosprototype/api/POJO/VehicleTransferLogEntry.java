package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class VehicleTransferLogEntry extends BasePOJO {

    @NonNull
    @SerializedName("vehicle")
    private Object vehicle;

    @NonNull
    @SerializedName("buyer")
    private Object buyer;

    @NonNull
    @SerializedName("seller")
    private Object seller;

    @NonNull
    @SerializedName("timestamp")
    private String timestamp;

    @Nullable
    @SerializedName("id")
    private String id;

}
