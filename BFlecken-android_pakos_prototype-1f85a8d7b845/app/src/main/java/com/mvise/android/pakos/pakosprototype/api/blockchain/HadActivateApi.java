package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.Had;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface HadActivateApi {

    @GET("de.vda.activateHAD")
    Observable<List<Had>> getHadActivatedList();

    @POST("de.vda.activateHAD")
    Completable addHadActivate(@Body Had had);

    @GET("de.vda.activateHAD/{id}")
    Single<Had> getHadActivatedById(@Path("id")String id);
}
