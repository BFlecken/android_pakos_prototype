package com.mvise.android.pakos.pakosprototype.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class JsonParameter implements Parcelable {

    @SerializedName("name")
    private String name;

    @SerializedName("dataType")
    private String type;

    @SerializedName("group")
    private boolean isGroup;

    @SerializedName("groupName")
    private String groupName;

    @SerializedName("range")
    private Range range;

    @SerializedName("parameter")
    private String parameter;

    @SerializedName("headline")
    private String headline;

    @SerializedName("joystick")
    private boolean joystick;

    @SerializedName("pagerGroup")
    private String pagerGroup;

    @SerializedName("displayLabel")
    private boolean displayLabel;//don't display the name as label [used for a seekbar : seat value]

    @SerializedName("screen")
    private String screen;//for parameters handled in different screens; e.g. seat

    public JsonParameter() {
    }

    public JsonParameter(String name, String type, boolean isGroup, String groupName, Range range,
                         String parameter, String headline, boolean joystick, String pagerGroup, boolean displayLabel, String screen) {

        this.name = name;
        this.type = type;
        this.isGroup = isGroup;
        this.groupName = groupName;
        this.range = range;
        this.parameter = parameter;
        this.headline = headline;
        this.joystick = joystick;
        this.pagerGroup = pagerGroup;
        this.displayLabel = displayLabel;
        this.screen = screen;
    }

    protected JsonParameter(Parcel in) {
        name = in.readString();
        type = in.readString();
        isGroup = in.readByte() != 0;
        groupName = in.readString();
        range = in.readParcelable(Range.class.getClassLoader());
        parameter = in.readString();
        headline = in.readString();
        joystick = in.readByte() != 0;
        pagerGroup = in.readString();
        displayLabel = in.readByte() != 0;
        screen = in.readString();
    }

    public static final Creator<JsonParameter> CREATOR = new Creator<JsonParameter>() {
        @Override
        public JsonParameter createFromParcel(Parcel in) {
            return new JsonParameter(in);
        }

        @Override
        public JsonParameter[] newArray(int size) {
            return new JsonParameter[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public boolean isJoystick() {
        return joystick;
    }

    public void setJoystick(boolean joystick) {
        this.joystick = joystick;
    }

    public String getPagerGroup() {
        return pagerGroup;
    }

    public void setPagerGroup(String pagerGroup) {
        this.pagerGroup = pagerGroup;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(type);
        parcel.writeByte((byte) (isGroup ? 1 : 0));
        parcel.writeString(groupName);
        parcel.writeParcelable(range, i);
        parcel.writeString(parameter);
        parcel.writeString(headline);
        parcel.writeByte((byte) (joystick ? 1 : 0));
        parcel.writeString(pagerGroup);
        parcel.writeByte((byte) (displayLabel ? 1 : 0));
        parcel.writeString(screen);
    }

    public boolean isDisplayLabel() {
        return displayLabel;
    }

    public void setDisplayLabel(boolean displayLabel) {
        this.displayLabel = displayLabel;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public JsonParameter clone() {
        return new JsonParameter(name, type, isGroup,groupName, range,
                parameter, headline, joystick, pagerGroup, displayLabel, screen);
    }
}
