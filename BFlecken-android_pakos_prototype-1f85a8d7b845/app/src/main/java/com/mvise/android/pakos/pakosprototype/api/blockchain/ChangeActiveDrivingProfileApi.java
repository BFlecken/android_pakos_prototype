package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.ActiveDrivingProfile;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ChangeActiveDrivingProfileApi {

    @GET("de.vda.changeActiveDrivingProfile")
    Observable<List<ActiveDrivingProfile>> getActiveDrivingProfileList();

    @POST("de.vda.activeDrivingProfile")
    Completable addActiveDrivingProfile(@Body ActiveDrivingProfile activeDrivingProfile);

    @GET("de.vda.changeActiveDrivingProfile/{id}")
    Single<ActiveDrivingProfile> getActiveDrivingProfileById(@Path("id")String id);
}
