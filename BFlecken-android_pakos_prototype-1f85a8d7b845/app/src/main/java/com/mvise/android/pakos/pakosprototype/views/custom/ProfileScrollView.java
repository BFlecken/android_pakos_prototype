package com.mvise.android.pakos.pakosprototype.views.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class ProfileScrollView extends ScrollView {

    public ProfileScrollView(Context context) {
        super(context);
    }

    public ProfileScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProfileScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean performClick() {
        super.performClick();
        return true;
    }
}
