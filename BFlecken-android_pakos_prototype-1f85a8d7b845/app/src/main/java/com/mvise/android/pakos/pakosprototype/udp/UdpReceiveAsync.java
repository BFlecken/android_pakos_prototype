package com.mvise.android.pakos.pakosprototype.udp;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class UdpReceiveAsync extends AsyncTask<Context, String, String> {

    private static final String TAG = "UdpReceiveAsync";
    static final String BUNDLE_RESULT_RECEIVED = "RESULT_RECEIVED" ;
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    private Handler mHandler;
    private DatagramSocket s = null;

    private List<Double> mReceivedValues = new CopyOnWriteArrayList<>();

    @Override
    protected String doInBackground(Context... params) {

        Context context = params[0];

        byte[] message = new byte[512*8];
        DatagramPacket p;

        String actualMessage;
        try {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            while (!isCancelled()) {
                mReceivedValues = new CopyOnWriteArrayList<>();

                int server_port = UdpUtils.getReceivingPort(context);
                if (s == null){
                    s = new DatagramSocket(server_port);
                    s.setBroadcast(false);
                }

                s.setReuseAddress(true);
                s.setSoTimeout(8*1000);// timeout
                Log.wtf(TAG, "Waiting to Receive Data...");
                try {

                    message = new byte[512*8];//8bytes x n params for a profile
                    p = new DatagramPacket(message, message.length);

                    s.receive(p);

                    actualMessage = bytesToHex(message);
                    extractParamsFromHex(actualMessage);

                    Log.wtf(TAG, "Received String " + actualMessage);
                    StringBuilder doubleValues = new StringBuilder();
                    if (mReceivedValues != null) {
                        for (Double d : mReceivedValues) {
                            if (d.intValue() == d) {
                                doubleValues.append(d.intValue()).append(" ");
                            } else {
                                doubleValues.append(d).append(" ");
                            }

                        }
                    }
                    publishProgress(doubleValues.toString());

                } catch (SocketTimeoutException e) {
                    publishProgress("exception0: "+e.getMessage());
                    closeSocket();
                    return "";
                }catch (IOException e) {
                    Log.wtf(TAG, "IOException is" + e);
                    e.printStackTrace();
                    publishProgress("exception1: "+e.getMessage());
                    closeSocket();
                }

            }
        }
        catch(Exception e){
            Log.wtf(TAG,"Exception is:"+e);
            publishProgress("exception2: "+e.getMessage());
            closeSocket();
            e.printStackTrace();
        }
        publishProgress(new String(message));
        closeSocket();
        return new String(message);
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        String doubleValues = values[0];
        Bundle resultStr = new Bundle();
        resultStr.putString(BUNDLE_RESULT_RECEIVED, doubleValues);
        if (mHandler != null){
            Message msg = mHandler.obtainMessage(1);
            msg.setData(resultStr);
            mHandler.sendMessage(msg);
        }
    }

    void setmHandler(Handler mHandler) {
        this.mHandler = mHandler;
    }

    private void closeSocket(){
        if (s!=null){
            s.close();
        }
    }

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private void extractParamsFromHex(String actualResult) {
        StringBuilder binaryText = new StringBuilder();
        StringBuilder reversedGroupsText = new StringBuilder();
        for (int i=0; i<11; i++){
            String str = actualResult.substring(i*16, (i+1)*16);
            while (!TextUtils.isEmpty(str)){
                reversedGroupsText.append(str.substring(str.length() - 2));
                str = str.substring(0, str.length()-2);
            }
        }

        for (int i=0; i<175; i+=2){
            binaryText.append(hexToBinary(reversedGroupsText.substring(i, i + 2)));
        }

        while (!TextUtils.isEmpty(binaryText.toString()) && binaryText.length()>=64){
            double doubleValue = Double.longBitsToDouble(new BigInteger(binaryText.substring(0, 64), 2).longValue());
            mReceivedValues.add(doubleValue);
            binaryText = new StringBuilder(binaryText.substring(64));
        }

    }

    private String hexToBinary(String parameterHex) {
        StringBuilder preBin = new StringBuilder(new BigInteger(parameterHex, 16).toString(2));
        int length = preBin.length();
        if (length < 8) {
            for (int i = 0; i < 8 - length; i++) {
                preBin.insert(0, "0");
            }
        }
        return preBin.toString();
    }

}