package com.mvise.android.pakos.pakosprototype.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.mvise.android.pakos.pakosprototype.udp.UdpUtils;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;
import com.mvise.android.pakos.pakosprototype.utils.TempPersistedSignIn;
import com.mvise.android.pakos.pakosprototype.views.MainNavigationActivity;

import java.util.ArrayList;
import java.util.List;

public class AppClosingService extends Service {

    private static final String TAG = AppClosingService.class.getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.wtf(TAG, "closed app");

        if (SharedPrefsUtil.getBooleanPreference(getApplicationContext(), SharedPrefsUtil.KEY_SIGNED_IN, false)){
            SharedPrefsUtil.setBooleanPreference(this,
                    SharedPrefsUtil.KEY_SIGNED_IN, false);
            TempPersistedSignIn.getInstance().setSignedIn(false);
            List<String> emptyMessageList = new ArrayList<>();

            emptyMessageList.add(Constants.ID_EMPTY_PACKAGE_SIGN_OUT);
            Log.wtf(TAG,"sending sign out package");
            UdpUtils.sendBroadcast(emptyMessageList,
                    this, new MainNavigationActivity());
        }

        stopSelf();
    }
}
