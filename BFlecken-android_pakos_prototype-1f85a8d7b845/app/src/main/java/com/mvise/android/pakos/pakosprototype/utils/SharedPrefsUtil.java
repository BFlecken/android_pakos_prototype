package com.mvise.android.pakos.pakosprototype.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.Map;


/**
 * A pack of helpful getter and setter methods for reading/writing to {@link SharedPreferences}.
 */
final public class SharedPrefsUtil {

    public static final String KEY_MODEL = "key_model";
    public static final String KEY_POSITIVE_VALUE = "key_positive_value";
    public static final String KEY_USER_VERIFIED = "key_user_verified";
    public static final String KEY_COUNTER_VERIFICATION_FAILED = "key_counter_verification_failed";

    public static final String KEY_ADAPTION_ENABLED = "key_adaption_enabled";
    public static final String KEY_PERIODIC_VERIFICATION = "key_periodic_verification";
    public static final String KEY_VERIFICATION_INTERVAL = "key_periodic_verification_interval";

    public static final String KEY_CAMERA_IP_ADDRESS = "camera_ip_address";
    public static final String KEY_CAMERA_PORT = "camera_port";
    public static final String KEY_MEASUREMENT_PC_IP_ADDRESS = "pc_ip_address";
    public static final String KEY_MEASUREMENT_PC_PORT = "pc_port";
    public static final String KEY_AUTOMATIC_SYNC_TO_CLOUD = "automatic_sync_to_cloud";

    public static final String KEY_VERIFICATION = "verification";
    public static final String MANUAL_VERIFICATION = "manually";
    public static final String CAM_VERIFICATION = "by-cam";

    public static final String KEY_ACTIVATED_PROFILE_ID = "ACTIVATED_PROFILE_ID";
    public static final String KEY_CAM_URL = "CAM_URL";

    public static final String KEY_SIGNED_IN = "SIGNED_IN";
    public static final String KEY_USERNAME = "USERNAME";

    public static final String KEY_PERSONA_PIC = "PERSONA_PIC";
    public static final String KEY_RECEIVING_PORT = "RECEIVING_PORT";
    public static final String KEY_LAST_ACTIVATION_SOURCE = "LAST_ACTIVATION_SOURCE";
    public static final String KEY_TEMP_ACTIVATED_PROFILE = "TEMP_ACTIVATED_PROFILE";

    public static final String KEY_CAR_ID = "CAR_ID";
    public static final String KEY_MILEAGE = "MILEAGE";

    public static final String KEY_HAD_ACTIVE = "HAD_ACTIVE";

    public static final String KEY_RECEIVED_DETAILS_AFTER_SIGNOUT = "RECEIVED_DETAILS_AFTER_SIGNOUT";

    public static final String TYPE = "type";
    public static final String STRING_VALUE = "value";
    public static final String NAME = "name";

    private SharedPrefsUtil() {}

    /**
     * Helper method to retrieve a String value from {@link SharedPreferences}.
     *
     * @param context a {@link Context} object.
     * @return The value from shared preferences, or null if the value could not be read.
     */
    public static String getStringPreference(Context context, String key) {
        String value = null;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences != null) {
            value = preferences.getString(key, null);
        }
        return value;
    }

    public static JSONObject getPreferenceValue(Context context, String key){
        JSONObject value = null ;
        String[] supportedTypes = new String[]{"string", "integer", "boolean", "float"};

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences != null) {
            for(String type : supportedTypes){
                value = getFromPreferences(preferences, key, type);
                if(value != null){
                    break;
                }
            }
        }
        return value;
    }

    private static JSONObject createStandardItem(String value, String key, String type) throws Exception{
        if(value != null && !"0".equals(value) && !"".equals(value)){
            JSONObject object = new JSONObject();

            object.put(NAME, key);
            object.put(TYPE, type);
            object.put(STRING_VALUE,value);

            return object;
        }
        else{
            return null;
        }
    }

    private static JSONObject getFromPreferences(SharedPreferences preferences, String key, String type) {
        try{
            Map<String ,?> all = preferences.getAll();
            if(all.get(key) == null){
                return null;
            }
            switch (type){
                case "integer":
                    return  createStandardItem(String.valueOf(preferences.getInt(key, 0)), key, type);
                case "float":
                    return  createStandardItem(String.valueOf(preferences.getFloat(key, 0f)), key, type);
                case "string":
                    return  createStandardItem(preferences.getString(key, null), key, type);
                case "boolean":
                    return  createStandardItem(String.valueOf(preferences.getBoolean(key,false)), key, type);
            }
        }
        catch (Exception e){
            // normal Exception
        }

        return null;
    }

    /**
     * Helper method to write a String value to {@link SharedPreferences}.
     *
     * @param context a {@link Context} object.
     */
    public static void setStringPreference(Context context, String key, String value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences != null && !TextUtils.isEmpty(key)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    /**
     * Helper method to retrieve a float value from {@link SharedPreferences}.
     *
     * @param context a {@link Context} object.
     * @param defaultValue A default to return if the value could not be read.
     * @return The value from shared preferences, or the provided default.
     */
    public static float getFloatPreference(Context context, String key, float defaultValue) {
        float value = defaultValue;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences != null) {
            value = preferences.getFloat(key, defaultValue);
        }
        return value;
    }

    /**
     * Helper method to write a float value to {@link SharedPreferences}.
     *
     * @param context a {@link Context} object.
     */
    public static void setFloatPreference(Context context, String key, float value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putFloat(key, value);
            editor.apply();
        }
    }

    /**
     * Helper method to retrieve an integer value from {@link SharedPreferences}.
     *
     * @param context a {@link Context} object.
     * @param defaultValue A default to return if the value could not be read.
     * @return The value from shared preferences, or the provided default.
     */
    public static int getIntegerPreference(Context context, String key, int defaultValue) {
        int value = defaultValue;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences != null) {
            value = preferences.getInt(key, defaultValue);
        }
        return value;
    }

    /**
     * Helper method to write an integer value to {@link SharedPreferences}.
     *
     * @param context a {@link Context} object.
     */
    public static void setIntegerPreference(Context context, String key, int value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();//immediately update with commit might be needed [required for active profile in adapter]
    }

    /**
     * Helper method to retrieve a boolean value from {@link SharedPreferences}.
     *
     * @param context a {@link Context} object.
     * @param defaultValue A default to return if the value could not be read.
     * @return The value from shared preferences, or the provided default.
     */
    public static boolean getBooleanPreference(Context context, String key, boolean defaultValue) {
        boolean value = defaultValue;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences != null) {
            value = preferences.getBoolean(key, defaultValue);
        }
        return value;
    }

    /**
     * Helper method to write a boolean value to {@link SharedPreferences}.
     *
     * @param context a {@link Context} object.
     */
    public static void setBooleanPreference(Context context, String key, boolean value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(key, value);
            editor.apply();
        }
    }
}