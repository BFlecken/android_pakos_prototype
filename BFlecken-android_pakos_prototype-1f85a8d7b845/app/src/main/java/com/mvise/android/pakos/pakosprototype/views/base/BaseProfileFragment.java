package com.mvise.android.pakos.pakosprototype.views.base;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.model.ComponentParameter;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.model.Model;
import com.mvise.android.pakos.pakosprototype.realm.RealmController;
import com.mvise.android.pakos.pakosprototype.udp.UdpUtils;
import com.mvise.android.pakos.pakosprototype.utils.Constants;
import com.mvise.android.pakos.pakosprototype.utils.MediaUtils;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

import static android.app.Activity.RESULT_OK;
import static com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment.lcs;
import static com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment.personaParams;

public abstract class BaseProfileFragment extends JsonBasedFragment {

    public static final int CAMERA_REQUEST_CODE = 0;
    public static final int GALLERY_REQUEST_CODE =1 ;
    public static final int READ_WRITE_CAMERA_REQUEST_CODE=10;
    public static final int READ_WRITE_REQUEST_CODE=11;
    private static final String TAG_EDIT_TEXT = "edit_text";
    private static final String TAG_CHECKBOX = "checkbox";
    private static final String TAG_RADIO_BUTTON = "radiobutton";
    private static final String TAG_TEXT_VIEW_GENERAL = "text";
    private static final String TAG = BaseProfileFragment.class.getSimpleName();

    private LinearLayout mContainerRootViews;

    private String mCurrentPhotoPath;
    private Uri picUri;
    private static String profileId;
    private Context mContext;
    private Activity mActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutResourceId(), container, false);
        mContainerRootViews = rootView.findViewById(R.id.container_rootviews);
        mContext = getActivity();
        mActivity = getActivity();
        generateViewsFromJson(profileId);
        restoreData(savedInstanceState);

        return rootView;
    }

    @Override
    public String getProfileId() {
        return profileId;
    }

    @Override
    public LinearLayout getRootViewsContainer() {
        return mContainerRootViews;
    }

    @Override
    public void setProfileId(String profileId) {
        BaseProfileFragment.profileId = profileId;
    }

    @Override
    public List<JsonParameter> getJsonParams() {
        return profileId.equals(Constants.ID_PERSONA) ? personaParams : lcs;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    mCurrentPhotoPath = picUri.getPath();
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    mCurrentPhotoPath = MediaUtils.getPath(mContext, data.getData());
                }
                break;
        }
    }

    private List<ComponentParameter> getMirrorCompValues(String tag, RealmResults<ComponentParameter> componentParameters){
        List<JsonParameter> paramsForTitle = new ArrayList<>();
        for (JsonParameter parameter: (profileId.equals(Constants.ID_PERSONA) ? personaParams : lcs)){
            if (parameter.getPagerGroup() != null && tag.contains(parameter.getPagerGroup())){
                paramsForTitle.add(parameter);
            }
        }

        List<ComponentParameter> currentCompParams = new ArrayList<>();
        if (componentParameters != null){
            for (JsonParameter param: paramsForTitle){
                for (ComponentParameter cp: componentParameters){
                    if (cp.getParameter().equals(param.getParameter())){
                        currentCompParams.add(cp);
                        break;
                    }
                }
            }
        }

        return currentCompParams;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case READ_WRITE_CAMERA_REQUEST_CODE:{
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2]==PackageManager.PERMISSION_GRANTED){
                    afterCameraPermission();
                }
                break;
            }
            case READ_WRITE_REQUEST_CODE:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                    afterGalleryPermissions();
                }
                break;
        }
    }

    public abstract int getLayoutResourceId();

    protected abstract void setProfilePictureFromCar();

    public abstract void onTakePhoto(View view);

    public void sendAllProfileData(String profileId) {
        List<String> valuesToSend = new ArrayList<>();
        valuesToSend.add(profileId);
        Log.wtf(TAG,"sending parameters of profile with id = "+profileId);
        for (View view: getGeneratedViews()){
            if (view instanceof TextView && view.getTag().toString().contains(TAG_TEXT_VIEW_GENERAL)){
                valuesToSend.add(((TextView)view).getText().toString());
                Log.wtf(TAG,"parameter being sent : text "+((TextView)view).getText().toString()+" tag "+view.getTag());
            }else if(view instanceof RadioButton
                    && view.getTag().toString().contains(TAG_RADIO_BUTTON)){
                valuesToSend.add(((RadioButton)view).isChecked()?"1":"0");
                Log.wtf(TAG, "parameter being sent : radio "+ (((RadioButton)view).isChecked()?"1":"0"));
            }else if(view instanceof CheckBox
                    && view.getTag().toString().contains(TAG_CHECKBOX)){
                valuesToSend.add(((CheckBox)view).isChecked()?"1":"0");
                Log.wtf(TAG,"parameter being sent : check "+(((CheckBox)view).isChecked()?"1":"0"));
            }else if (view instanceof Button && !view.getTag().toString().contains(Constants.SCREEN_SEAT) && !view.getTag().toString().contains(Constants.SCREEN_ENTERTAINMENT)){
                RealmResults<ComponentParameter> componentParameters = RealmController.with().getParamValues();
                String tag = view.getTag().toString();
                List<ComponentParameter> utilComponents = getMirrorCompValues(tag, componentParameters);

                if (utilComponents != null){
                    for (ComponentParameter componentParameter: utilComponents){
                        valuesToSend.add(componentParameter != null ? componentParameter.getValue()+"" : "0");
                        Log.wtf(TAG,"parameter being sent : "+((componentParameter !=null)?
                                (componentParameter.getName()+" "+componentParameter.getValue()): "null"));
                    }
                }
            } else if (view instanceof Button && (view.getTag().toString().equals(Constants.SCREEN_SEAT) || view.getTag().toString().equals(Constants.SCREEN_ENTERTAINMENT))){
                valuesToSend.addAll(getValuesToSendForScreen(view.getTag().toString()));
            }
        }
        UdpUtils.sendBroadcast(valuesToSend, mContext, mActivity);
    }

    private List<String> getValuesToSendForScreen(String screen) {
        List<String> valuesToSend = new ArrayList<>();
        RealmResults<ComponentParameter> componentParameters = RealmController.with().getParamValues();
        for (JsonParameter jsonParameter: (profileId.equals(Constants.ID_PERSONA) ? personaParams : lcs)){
            if (!TextUtils.isEmpty(jsonParameter.getScreen()) && jsonParameter.getScreen().equals(screen)){
                String value = null;
                for (ComponentParameter cp: componentParameters){
                    if (cp.getParameter().equals(jsonParameter.getParameter())){
                        if (cp.getType().equals(Constants.PARAM_TYPE_BOOLEAN)){
                            value = cp.getValue().equals(true+"") ? 1+"" : 0+"";
                        }else{
                            value = cp.getValue()+"";
                        }
                    }
                }
                if (value == null){
                    value = "0";
                }
                valuesToSend.add(value);
            }
        }
        return valuesToSend;
    }

    private void sendPersonaData() {
        List<String> valuesToSend = new ArrayList<>();
        valuesToSend.add(Constants.ID_PERSONA);
        for (View view: getGeneratedViews()){
            if (view instanceof EditText && view.getTag().toString().contains(TAG_EDIT_TEXT)){
                EditText editText = (EditText)view;
                if (!TextUtils.isEmpty(editText.getText().toString())){
                    valuesToSend.add(((EditText)view).getText().toString());
                }else{
                    valuesToSend.add("0");
                }
            }else if (view instanceof TextView && view.getTag().toString().contains(TAG_TEXT_VIEW_GENERAL)){
                valuesToSend.add(((TextView)view).getText().toString());
            }else if(view instanceof RadioButton && view.getTag().toString().contains(TAG_RADIO_BUTTON)){
                valuesToSend.add(((RadioButton)view).isChecked()?"1":"0");
            }else if(view instanceof CheckBox && view.getTag().toString().contains(TAG_CHECKBOX)){
                valuesToSend.add(((CheckBox)view).isChecked()?"1":"0");
            }else if (view instanceof Button && !view.getTag().toString().contains(Constants.SCREEN_SEAT) && !view.getTag().toString().contains(Constants.SCREEN_ENTERTAINMENT)){
                RealmResults<ComponentParameter> componentParameters = RealmController.with().getParamValues();
                String tag = view.getTag().toString();
                List<ComponentParameter> utilComponents = getMirrorCompValues(tag, componentParameters);

                if (utilComponents != null){
                    for (ComponentParameter componentParameter: utilComponents){
                        valuesToSend.add(componentParameter != null ? componentParameter.getValue()+"" : "0");
                    }
                }
            }else if (view instanceof Button && (view.getTag().toString().equals(Constants.SCREEN_SEAT) || view.getTag().toString().equals(Constants.SCREEN_ENTERTAINMENT))){
                valuesToSend.addAll(getValuesToSendForScreen(view.getTag().toString()));
            }
        }
        UdpUtils.sendBroadcast(valuesToSend, mContext, mActivity);
        Toast.makeText(mContext, R.string.message_input_saved_and_send, Toast.LENGTH_SHORT).show();
    }

    public void onSaveProfile(View view, String profileId){
        saveChanges(profileId);
        switch (profileId) {
            case Constants.ID_PERSONA:
                SharedPrefsUtil.setStringPreference(mContext, SharedPrefsUtil.KEY_PERSONA_PIC, mCurrentPhotoPath);
                RealmController.with()
                        .updateProfile(getString(R.string.persona), getString(R.string.persona), mCurrentPhotoPath);
                break;
            case Constants.ID_BASIC_PROFILE:
                RealmController.with()
                        .updateProfile(getString(R.string.profile_basic), getString(R.string.profile_basic), mCurrentPhotoPath);
                break;
            case Constants.ID_SNOW_PROFILE:
                RealmController.with()
                        .updateProfile(getString(R.string.profile_snow), getString(R.string.profile_snow), mCurrentPhotoPath);

                break;
            case Constants.ID_WORK_PROFILE:
                RealmController.with()
                        .updateProfile(getString(R.string.profile_work), getString(R.string.profile_work), mCurrentPhotoPath);

                break;
        }

        boolean isSignedIn = SharedPrefsUtil.getBooleanPreference(mContext, SharedPrefsUtil.KEY_SIGNED_IN, false);
        if (isSignedIn) {
            if (profileId.equals(Constants.ID_PERSONA)){
                sendPersonaData();
            }else{
                sendAllProfileData(profileId);
            }
        }
    }

    public void displayPopupChooseImage() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_choose_pic);

        Button mCamerabtn =  dialog.findViewById(R.id.cameradialogbtn);
        Button mGallerybtn = dialog.findViewById(R.id.gallerydialogbtn);
        Button mCarbtn = dialog.findViewById(R.id.cardialogbtn);

        mCamerabtn.setOnClickListener(v -> {
            if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT
                    && (ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.CAMERA) !=PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA},
                        READ_WRITE_CAMERA_REQUEST_CODE);
            }else{
                afterCameraPermission();
            }

            dialog.cancel();
        });
        mGallerybtn.setOnClickListener(v -> {
            if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT
                    && (ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        },
                        READ_WRITE_REQUEST_CODE);
            }else{
                afterGalleryPermissions();
            }
            dialog.cancel();
        });
        String modelString = SharedPrefsUtil.getStringPreference(mContext,
                SharedPrefsUtil.KEY_MODEL);
        boolean hasPhotoFromCar = false;
        if (!TextUtils.isEmpty(modelString)) {
            Model currentModel = new Gson().fromJson(modelString, Model.class);
            if (!TextUtils.isEmpty(currentModel.getThumbnail())) {
                if (currentModel.getThumbnail().endsWith("\\u003d")){
                    String thumbnail = currentModel.getThumbnail().replace("\\u003d","");
                    currentModel.setThumbnail(thumbnail);
                }
                hasPhotoFromCar = true;
            }
        }
        boolean isSignedIn = SharedPrefsUtil.getBooleanPreference(
                mContext,
                SharedPrefsUtil.KEY_SIGNED_IN,
                false);
        if (isSignedIn && hasPhotoFromCar) {
            mCarbtn.setEnabled(true);
            mCarbtn.setTextColor(getResources().getColor(android.R.color.black));
            mCarbtn.setCompoundDrawablesWithIntrinsicBounds(null,null,null,
                    getResources().getDrawable(R.drawable.baseline_directions_car_white_48));
        }else {
            mCarbtn.setEnabled(false);
            mCarbtn.setTextColor(getResources().getColor(android.R.color.darker_gray));
            mCarbtn.setCompoundDrawablesWithIntrinsicBounds(null,null,null,
                    getResources().getDrawable(R.drawable.disabled_car));

        }

        mCarbtn.setOnClickListener(view -> {
            dialog.dismiss();
            displayPopupPreviewCarImage();
        });
        dialog.show();
    }

    public void displayPopupPreviewCarImage() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_car_cam_preview);

        Button mDiscardBtn = dialog.findViewById(R.id.btn_discard_preview);
        Button mSaveBtn  = dialog.findViewById(R.id.btn_save_preview);

        mDiscardBtn.setOnClickListener(view -> dialog.cancel());
        mSaveBtn.setOnClickListener(view -> {
            setProfilePictureFromCar();
            dialog.cancel();
        });

        ImageView previewImg = dialog.findViewById(R.id.image_cam_preview);

        String modelString = SharedPrefsUtil.getStringPreference(mContext,
                SharedPrefsUtil.KEY_MODEL);
        if (!TextUtils.isEmpty(modelString)) {
            Model currentModel = new Gson().fromJson(modelString, Model.class);
            if (!TextUtils.isEmpty(currentModel.getThumbnail())) {
                if (currentModel.getThumbnail().endsWith("\\u003d")){
                    String thumbnail = currentModel.getThumbnail().replace("\\u003d","");
                    currentModel.setThumbnail(thumbnail);
                }
                byte[] decodedString = Base64.decode(currentModel.getThumbnail(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,
                        decodedString.length);
                previewImg.setImageBitmap(decodedByte);
            }
        }

        dialog.show();
    }

    private void afterGalleryPermissions() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    private void afterCameraPermission(){
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file=MediaUtils.getOutputMediaFile(mContext);
        picUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,picUri);
        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }

    public String getmCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }


}
