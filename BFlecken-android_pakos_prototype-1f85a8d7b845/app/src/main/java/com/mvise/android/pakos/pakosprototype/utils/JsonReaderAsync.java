package com.mvise.android.pakos.pakosprototype.utils;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mvise.android.pakos.pakosprototype.model.JsonParameter;
import com.mvise.android.pakos.pakosprototype.model.OrderedJsonObjectList;
import com.mvise.android.pakos.pakosprototype.views.base.UdpCommunicationListener;
import com.mvise.android.pakos.pakosprototype.views.fragments.DrivingProfilesListFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public class JsonReaderAsync extends AsyncTask<Void, List<JsonParameter>, List<JsonParameter>> {

    private static final String TAG = JsonReaderAsync.class.getSimpleName();

    private List<JsonParameter> mPersonaParams;
    private List<JsonParameter> mProfileParams = new ArrayList<>();
    private Exception mException;
    private List<OrderedJsonObjectList> mJsonObjects = new ArrayList<>();
    private UdpCommunicationListener mCommunicationListener;
    private WeakReference<Activity> mWeakActivity;
    private Activity activity;

    public JsonReaderAsync(UdpCommunicationListener listener, Activity activity) {
        mCommunicationListener = listener;
        mWeakActivity = new WeakReference<>(activity);
        this.activity = activity;
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    @Override
    protected List<JsonParameter> doInBackground(Void... voids) {
        StringBuilder text = new StringBuilder();
        List<JsonParameter> lcs = new ArrayList<>();
        try {
            BufferedReader br;
            try{
                br = new BufferedReader(new FileReader(new File
                        (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                                Constants.JSON_FILENAME)));
            }
            catch (Exception e){

                br = new BufferedReader(new InputStreamReader(activity.getAssets().open(Constants.JSON_FILENAME), StandardCharsets.UTF_8) );
            }
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
//            Log.wtf(TAG, "read json file content: "+text.toString());
            br.close();

            //parse json, subprofile--------------------------------
            JSONObject rootObject = new JSONObject(text.toString());
            JSONObject subprofileObject = rootObject.getJSONObject(Constants.JSON_KEY_SUBPROFILE);
//            Log.wtf(TAG, "extracted subprofile json : "+ subprofileObject.toString());
            Iterator<String> iter1 = subprofileObject.keys();
            mProfileParams = new ArrayList<>();
            while (iter1.hasNext()) {
                String key = iter1.next();
                try {
                    JSONObject jsonObjectParam = subprofileObject.getJSONObject(key);
                    parseProfile(jsonObjectParam);
                } catch (JSONException e) {
                    e.printStackTrace();
                    mException = e;
                }
            }
            //---persona------------------------
            mPersonaParams = new ArrayList<>();
            JsonObject rootObject2 = new Gson()
                    .fromJson(text.toString(), JsonObject.class);
            String subprofileObject2 = rootObject2.getAsJsonObject(Constants.JSON_KEY_PERSONA).toString();
            JsonObject jsonObject = new Gson().fromJson(subprofileObject2,
                    JsonObject.class);
            bigJsonIteration(jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
            mException = e;
        }
        return lcs;
    }

    @Override
    protected void onPostExecute(List<JsonParameter> lcs) {
        if (mCommunicationListener != null){
            mCommunicationListener.initializeParameters(mProfileParams, mPersonaParams, mException);

            if (DrivingProfilesListFragment.sFirstTimeSent) {
                DrivingProfilesListFragment.sFirstTimeSent = false;
                if (SharedPrefsUtil.getBooleanPreference(mWeakActivity.get(), SharedPrefsUtil.KEY_SIGNED_IN, false)){
                    mCommunicationListener.sendAllProfilesWithDelay();
                }
            }
        }
    }

    private void parseProfile(JSONObject jsonObjectParam) {
        boolean continueParsing = jsonObjectParam.has("dataType");
        if (continueParsing) {
            //base object
            Gson gson = new Gson();
            Type paramIdType = new TypeToken<JsonParameter>() {}.getType();
            JsonParameter value = gson.fromJson(jsonObjectParam.toString(), paramIdType);
            mProfileParams.add(value);
        } else {
            //base object is on 2nd level in json
            Iterator<String> subIterator = jsonObjectParam.keys();
            while (subIterator.hasNext()) {
                String subKey = subIterator.next();
                JSONObject subObjectParam;
                try {
                    subObjectParam = jsonObjectParam.getJSONObject(subKey);
                    parseProfile(subObjectParam);
                } catch (JSONException e) {
                    e.printStackTrace();
                    mException = e;
                }
            }
        }
    }

    private void bigJsonIteration(JsonObject jsonObject) {
        int position = -1;
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            position++;
            if (entry.getValue().isJsonObject()) {
                iterateJsonObj(entry.getValue().getAsJsonObject(), position);
            } else {
                JsonObject checkResult = checkForDataType(jsonObject);
                if (checkResult != null) {
                    mJsonObjects.add(new OrderedJsonObjectList(checkResult, position));
                }
            }
        }
        LinkedHashSet<JsonObject> jsonObjects = new LinkedHashSet<>();
        for (OrderedJsonObjectList jsonObjectList : mJsonObjects) {
            jsonObjects.add(jsonObjectList.getJsonObjects());
        }
        List<JsonObject> result = new ArrayList<>(jsonObjects);
        List<JsonObject> aux = new ArrayList<>(result);
        int removedCount = 0;
        for (int i = 0; i < aux.size(); i++) {
            JsonObject jsonObject1 = aux.get(i);
            if (i > 0 && aux.get(i - 1).equals(aux.get(i))) {
                result.remove(i - removedCount);
                removedCount++;
            }
            JsonParameter value = new Gson()
                    .fromJson(jsonObject1, JsonParameter.class);
            mPersonaParams.add(value);
        }
    }

    private void iterateJsonObj(JsonObject jsonObject, int position) {
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            if (entry.getValue().isJsonObject()) {
                iterateJsonObj(entry.getValue().getAsJsonObject(), position);
            } else {
                JsonObject paramValue = checkForDataType(jsonObject);
                if (paramValue != null) {
                    mJsonObjects.add(new OrderedJsonObjectList(paramValue, position));
                }
            }
        }
    }

    private JsonObject checkForDataType(JsonObject jsonObject) {
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            if (entry.getKey().equals(Constants.JSON_KEY_PARAM_TYPE)) {
                return jsonObject;
            }
        }
        return null;
    }
}