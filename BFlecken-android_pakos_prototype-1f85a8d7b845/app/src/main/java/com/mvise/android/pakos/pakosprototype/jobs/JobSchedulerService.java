package com.mvise.android.pakos.pakosprototype.jobs;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.mvise.android.pakos.pakosprototype.api.FaceIDApiClient;
import com.mvise.android.pakos.pakosprototype.api.FaceIDApiInterface;
import com.mvise.android.pakos.pakosprototype.model.Model;
import com.mvise.android.pakos.pakosprototype.model.VerificationModel;
import com.mvise.android.pakos.pakosprototype.utils.NotificationUtils;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;
import com.mvise.android.pakos.pakosprototype.views.fragments.CarConnectionFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobSchedulerService extends JobService {
    public final static int JOB_ID_VERIFY_MODULE = 1;
    private static final String TAG = JobSchedulerService.class.getSimpleName();

    Call<VerificationModel> call;

    @Override
    public boolean onStartJob(JobParameters params) {
        //Method should return true if service needs to process the work (on a separate thread).
        // False if there's no more work to be done for this job.
        String modelString = SharedPrefsUtil.getStringPreference(this, SharedPrefsUtil.KEY_MODEL);
        Model currentModel = new Gson().fromJson(modelString, Model.class);
        if (currentModel == null) {
            return false;
        }

        String baseUrl;
        baseUrl = CarConnectionFragment.makeBaseUrl(this);
        FaceIDApiInterface faceIDApiService = FaceIDApiClient.getClient(baseUrl).create(FaceIDApiInterface.class);

        final boolean isAdaptionEnabled = SharedPrefsUtil.getBooleanPreference(this, SharedPrefsUtil.KEY_ADAPTION_ENABLED, false);
        VerificationModel verificationModelPOJO = new VerificationModel();

        verificationModelPOJO.addModel(currentModel.getModel());
        verificationModelPOJO.setAdapt(isAdaptionEnabled);

        call = faceIDApiService.verifyModel(FaceIDApiClient.getApiKey(), verificationModelPOJO);
        call.enqueue(new Callback<VerificationModel>() {
            @Override
            public void onResponse(@NonNull Call<VerificationModel> call, @NonNull Response<VerificationModel> response) {
                VerificationModel modelResponse = response.body();
                Log.v(TAG, "verify_model response: " + modelResponse);
                boolean verificationPassed = false;
                if (modelResponse != null) {
                    if (TextUtils.isEmpty(modelResponse.getError()) && modelResponse.getScores() != null && modelResponse.getScores().size() > 0) {
                        String score = modelResponse.getScores().get(0);
                        try {
                            float scoreFloat = Float.parseFloat(score);
                            float compareLimit = SharedPrefsUtil.getFloatPreference(JobSchedulerService.this,
                                    SharedPrefsUtil.KEY_POSITIVE_VALUE, VerificationModel.DEFAULT_POSITIVE_SCORE);

                            if (scoreFloat < compareLimit) {
                                verificationPassed = false;
                            } else {
                                verificationPassed = true;
                                String encodedModel = modelResponse.getModels() != null && modelResponse.getModels().size() > 0 ?
                                        modelResponse.getModels().get(0) : null;
                                setUserVerified(JobSchedulerService.this, true, isAdaptionEnabled ? encodedModel : null);
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (!verificationPassed) {
                    setUserVerified(JobSchedulerService.this, false, null);
                }

                JobSchedulerUtil.rescheduleJobForAndroidN(JobSchedulerService.this);
            }

            @Override
            public void onFailure(@NonNull Call<VerificationModel> call, @NonNull Throwable error) {
                Log.v("TAG", error.toString());
                setUserVerified(JobSchedulerService.this, false, null);
                JobSchedulerUtil.rescheduleJobForAndroidN(JobSchedulerService.this);
            }
        });

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if (call != null && !call.isCanceled()) {
            call.cancel();
        }
        return true;
    }

    synchronized private void setUserVerified(final Context context, final boolean verified, final String encodedModel) {
        int failedCounter = SharedPrefsUtil.getIntegerPreference(context, SharedPrefsUtil.KEY_COUNTER_VERIFICATION_FAILED, 0);
        failedCounter = verified ? 0 : ++failedCounter;

        SharedPrefsUtil.setBooleanPreference(context, SharedPrefsUtil.KEY_USER_VERIFIED, verified);
        if (!TextUtils.isEmpty(encodedModel) && verified) {
            String moduleJson = SharedPrefsUtil.getStringPreference(context, SharedPrefsUtil.KEY_MODEL);
            if (null != moduleJson) {
                Gson gson = new Gson();
                Model currentModel = gson.fromJson(moduleJson, Model.class);
                if (currentModel != null) {
                    currentModel.setModel(encodedModel);
                    SharedPrefsUtil.setStringPreference(context, SharedPrefsUtil.KEY_MODEL, gson.toJson(currentModel));
                }
            }
        }
        if (failedCounter >= VerificationModel.VERIFICATION_FAILS_LIMIT) {
            failedCounter = 0;
            NotificationUtils.notifyUserToLookIntoCamera(context);
        }

       SharedPrefsUtil.setIntegerPreference(context, SharedPrefsUtil.KEY_COUNTER_VERIFICATION_FAILED,
               failedCounter);
    }


}
