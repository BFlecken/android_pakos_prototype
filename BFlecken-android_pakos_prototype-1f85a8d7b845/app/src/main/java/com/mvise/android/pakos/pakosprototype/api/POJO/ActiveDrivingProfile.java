package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class ActiveDrivingProfile extends BasePOJO {

    @NonNull
    @SerializedName("drivingProfileRegister")
    private Object drivingProfileRegister;

    @NonNull
    @SerializedName("tenant")
    private Object tenant;

    @NonNull
    @SerializedName("activeDrivingProfile")
    private String activeDrivingProfile;

    @Nullable
    @SerializedName("transactionId")
    private String transactionId;

    @Nullable
    @SerializedName("timestamp")
    private String timestamp;


}
