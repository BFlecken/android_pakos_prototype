package com.mvise.android.pakos.pakosprototype.api.blockchain;

import com.mvise.android.pakos.pakosprototype.api.POJO.SendDamageReport;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SendDamageReportApi {

    @GET("de.vda.sendDamageReport")
    Observable<List<SendDamageReport>> getSendDamageReportList();

    @POST("de.vda.sendDamageReport")
    Completable addSendDamageReport(@Body SendDamageReport sendDamageReport);

    @GET("de.vda.sendDamageReport/{id}")
    Single<SendDamageReport> getSendDamageReportById(@Path("id") String id);

}
