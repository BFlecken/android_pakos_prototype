package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Company extends BasePOJO {

    @NonNull
    @SerializedName("companyId")
    private String companyId;

    @Nullable
    @SerializedName("headquarters")
    private Address headquarters;

    @Nullable
    @SerializedName("name")
    private String name;
}
