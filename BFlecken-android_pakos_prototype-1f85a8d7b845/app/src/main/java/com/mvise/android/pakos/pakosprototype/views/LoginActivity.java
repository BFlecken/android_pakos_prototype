package com.mvise.android.pakos.pakosprototype.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mvise.android.pakos.pakosprototype.App;
import com.mvise.android.pakos.pakosprototype.R;
import com.mvise.android.pakos.pakosprototype.api.POJO.Had;
import com.mvise.android.pakos.pakosprototype.utils.SharedPrefsUtil;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText mUsernameEditText;
    private EditText mPasswordEditText;

    private FirebaseAuth mAuth;
    private StorageReference mStorageRef;

    @Inject
    Retrofit retrofitInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        App.getDaggerComponent().inject(this);

        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        mUsernameEditText = findViewById(R.id.edit_username);
        mPasswordEditText = findViewById(R.id.edit_password);

        Had hadModel = new Had(
                 "de.pakos.lifecycle.Tenant#1",
                "resource:de.vda.Vehicle#1"
             );
//        retrofitInstance.create(HadActivateApi.class).addHadActivate(hadModel)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(() -> Log.e("Tinker_1", "done upload"),
//                        throwable ->  Log.e("Tinker_1", throwable.getMessage()));
//
////
//        retrofitInstance
//                .create(TenantApi.class)
//                .getTenantList()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(response ->
//                    Log.e("Tinker_2", response.size()+""),
//                        error ->  Log.e("Tinker_2_err", error.getMessage()));

//        retrofitInstance.create(HadActivateApi.class).getHadActivatedById("6dcf0b4f86dbfc356b0880bda1a1e1fbc1aae9478151ba5493458f188fd5dbac")
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(item -> Log.e("Tinker_1", item.timestamp),
//                        throwable ->  Log.e("Tinker_1", throwable.getMessage()));


    }


    public void onLogin(View view) {
        if (TextUtils.isEmpty(mUsernameEditText.getText().toString())){
            Toast.makeText(LoginActivity.this, R.string.message_empty_mail,
                    Toast.LENGTH_SHORT).show();
            mUsernameEditText.setHintTextColor(getResources().getColor(android.R.color.holo_red_dark));
        }else if (TextUtils.isEmpty(mPasswordEditText.getText().toString())){
            Toast.makeText(LoginActivity.this, R.string.message_empty_password,
                    Toast.LENGTH_SHORT).show();
            mPasswordEditText.setHintTextColor(getResources().getColor(android.R.color.holo_red_dark));
        }else {
            login(mUsernameEditText.getText().toString(), mPasswordEditText.getText().toString());
        }

    }

    public void onSignUp(View view) {
        Intent openRegisterActivity = new Intent(this, RegisterActivity.class);
        startActivity(openRegisterActivity);
    }

    private void login(String mail, String password){
        mAuth.signInWithEmailAndPassword(mail, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "signInWithEmail:success");
                        SharedPrefsUtil.setStringPreference(LoginActivity.this,
                                SharedPrefsUtil.KEY_USERNAME,
                                TextUtils.isEmpty(mUsernameEditText.getText().toString())
                                        ? "" : mUsernameEditText.getText().toString()
                        );

                        // Handle any errors
                        mStorageRef.child("profile-pics/"+mUsernameEditText.getText().toString())
                                .getDownloadUrl().addOnSuccessListener(uri -> {
                                    String generatedFilePath = uri.toString();
                                    SharedPrefsUtil.setStringPreference(LoginActivity.this,
                                            SharedPrefsUtil.KEY_PERSONA_PIC,
                                            generatedFilePath);
                                }).addOnFailureListener(Throwable::printStackTrace);

                        navigateToProfilesList();
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(LoginActivity.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void navigateToProfilesList() {
        Intent openDrivingProfilesActivity = new Intent(LoginActivity.this,
                MainNavigationActivity.class);
        startActivity(openDrivingProfilesActivity);
        finish();
    }

    public void onSkip(View view) {
        navigateToProfilesList();
    }
}
