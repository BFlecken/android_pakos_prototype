package com.mvise.android.pakos.pakosprototype.api.POJO;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class UpdateDrivingProfile extends BasePOJO{

    @NonNull
    @SerializedName("drivingProfile")
    private Object drivingProfile;

    @NonNull
    @SerializedName("tenant")
    private Object tenant;

    @Nullable
    @SerializedName("timestamp")
    private String transactionId;

    @Nullable
    @SerializedName("timestamp")
    private String timestamp;
}
