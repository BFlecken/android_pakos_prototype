package com.mvise.android.pakos.pakosprototype.views.base;

import com.mvise.android.pakos.pakosprototype.model.JsonParameter;

import java.util.List;

public interface UdpCommunicationListener {
    void initializeParameters(List<JsonParameter> profile, List<JsonParameter> persona, Exception e);
    void sendAllProfilesWithDelay();
    void onChangedParamsReceived(String profileId);
    void onFeedbackRequested(int feedbackParameter);//lanechange [1] | overtake [2]
    void onProfileActivationRequested();
    void onCarParamsReceived(double[] dValues);
    void onHadActivationChanged(double dValue);
    void onHadActivityReceived(double dValue);
}
