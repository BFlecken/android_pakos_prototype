package com.mvise.android.pakos.pakosprototype.views.base;

public interface CarConnectionListener {
    void onSignIn();
    void onSignOut();
}
