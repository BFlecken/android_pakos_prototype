import socket
import struct
from time import sleep

UDP_IP = "192.168.3.210"
UDP_PORT = 8080
sock = socket.socket(socket.AF_INET,  # Internet
socket.SOCK_DGRAM)  # UDP
sock.bind(('192.168.3.210', UDP_PORT))
print("ready to receive messages")
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
while True:
    data, addr = sock.recvfrom(512)  # buffer size is 1024 bytes
    print()
    var = "received message:", data
    if var != "received message:":
        try:
            print(var)
            # \x00\x00\x00\x00\x00\x00\x8f@ for package id 992
            client_socket.sendto(struct.pack('!qqq',
                                             0x0000000000008f40,
                                             0x000000000000f03f,
                                             0x000000000001f03f
                                             ),
                                 ("192.168.3.165", 4500))
        except Exception as msg:
            print(msg)