from tkinter import *
import socket
import struct
import thread
import threading
import time

root = Tk()
root.geometry("800x500")
root.title("PAKoS Car Simulator")

defaultPcIp = "192.168.3.210"
defaultPcPort = 8080
defaultPhoneIp = "192.168.3.74"
defaultPhonePort = 4500

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# --------------------------------------------------------------------------------------------

frameConfig = Frame(root, bg="orange")

labelHAD = Label(frameConfig, text="Configuration", font=("Helvetica", 16))
labelHAD.config(bg='orange', fg='black')
labelHAD.pack(side=TOP, padx=8, pady=(8, 0))

framePc = Frame(frameConfig, bg="orange")
framePc.pack(side=TOP, fill=X, padx=8, pady=8)
labelPc = Label(framePc, text="PC:", font=("Helvetica", 10))
labelPc.config(bg='orange', fg='black')
labelPc.pack(side=LEFT, padx=(0, 28))
entryPcIp = Entry(framePc, width=16)
entryPcIp.insert(0, defaultPcIp)
entryPcIp.pack(side=LEFT)
entryPcPort = Entry(framePc, width=8)
entryPcPort.insert(0, defaultPcPort)
entryPcPort.pack(side=LEFT, padx=8)

framePhone = Frame(frameConfig, bg="orange")
framePhone.pack(side=TOP, fill=X, padx=8, pady=(0, 8))
labelPhone = Label(framePhone, text="Phone:", font=("Helvetica", 10))
labelPhone.config(bg='orange', fg='black')
labelPhone.pack(side=LEFT, padx=(0, 8))
entryPhoneIp = Entry(framePhone, width=16)
entryPhoneIp.insert(0, defaultPhoneIp)
entryPhoneIp.pack(side=LEFT)
entryPhonePort = Entry(framePhone, width=8)
entryPhonePort.insert(0, defaultPhonePort)
entryPhonePort.pack(side=LEFT, padx=8)

# buttonSaveConfig = Button(frameConfig, text="Save", width=13)
# buttonSaveConfig.pack(side=LEFT, padx=(62, 0), pady=8)

frameConfig.pack(side=TOP, fill=X, padx=8, pady=8)

# --------------------------------------------------------------------------------------------

frameHad = Frame(root, bg="green")

labelHAD = Label(frameHad, text="Request 995 - HAD", font=("Helvetica", 16))
labelHAD.config(bg='green', fg='black')
labelHAD.pack(side=TOP, anchor=W, padx=8, pady=(8, 0))

buttonEnableHAD = Button(frameHad, text="Enable HAD")
buttonEnableHAD.pack(side=LEFT, padx=8, pady=8)
buttonDisableHAD = Button(frameHad, text="Disable HAD")
buttonDisableHAD.pack(side=LEFT, padx=8, pady=8)

frameHad.pack(side=TOP, fill=X)

# --------------------------------------------------------------------------------------------

frameFeedback = Frame(root, bg="cyan")

labelFeedback = Label(frameFeedback, text="Request 997 - HAD action", font=("Helvetica", 16))
labelFeedback.config(bg='cyan', fg='black')
labelFeedback.pack(side=TOP, anchor=W, padx=8, pady=(8, 0))

buttonLaneChange = Button(frameFeedback, text="Change Lane")
buttonLaneChange.pack(side=LEFT, padx=8, pady=8)
buttonOvertake = Button(frameFeedback, text="Overtake")
buttonOvertake.pack(side=LEFT, padx=8, pady=8)

frameFeedback.pack(side=TOP, fill=X)

# --------------------------------------------------------------------------------------------

frameReceive = Frame(root, bg="yellow")
labelFeedbackTitle = Label(frameReceive, text="Received data", font=("Helvetica", 16))
labelFeedbackTitle.config(bg='yellow', fg='black')
labelFeedbackTitle.pack(side=TOP, anchor=W, padx=8, pady=(8, 0))

labelFeedback = Label(frameReceive, text="empty", font=("Helvetica", 10))
labelFeedback.config(bg='yellow', fg='black')
labelFeedback.pack(side=TOP, anchor=W, padx=8, pady=(8, 0))

frameReceive.pack(side=TOP, fill=X)

# --------------------------------------------------------------------------------------------

def eventEnableHAD(event):
    try:
        client_socket.sendto(struct.pack('!qq',
                                         0x0000000000188f40,
                                         0x000000000000f03f
                                         ),
                             (entryPhoneIp.get(), int(entryPhonePort.get())))

        client_socket.sendto(struct.pack('!qq',
                                         0x0000000000108f40,
                                         0x000000000000f03f
                                         ),
                             (entryPhoneIp.get(), int(entryPhonePort.get())))
    except Exception as msg:
        print(msg)


def eventDisableHAD(event):
    try:
        client_socket.sendto(struct.pack('!qq',
                                         0x0000000000188f40,
                                         0x0000000000000000
                                         ),
                             (entryPhoneIp.get(), int(entryPhonePort.get())))
    except Exception as msg:
        print(msg)


def eventFeedbackChangeLane(event):
    try:
        client_socket.sendto(struct.pack('!qq',
                                         0x0000000000288f40,
                                         0x000000000000f03f
                                         ),
                             (entryPhoneIp.get(), int(entryPhonePort.get())))
    except Exception as msg:
        print(msg)


def eventFeedbackOvertake(event):
    try:
        client_socket.sendto(struct.pack('!qq',
                                         0x0000000000288f40,
                                         0x0000000000000040
                                         ),
                             (entryPhoneIp.get(), int(entryPhonePort.get())))
    except Exception as msg:
        print(msg)


buttonEnableHAD.bind("<Button-1>", eventEnableHAD)
buttonDisableHAD.bind("<Button-1>", eventDisableHAD)
buttonLaneChange.bind("<Button-1>", eventFeedbackChangeLane)
buttonOvertake.bind("<Button-1>", eventFeedbackOvertake)

# print("dsdfssg")
# label.config(text='change the value')


UDP_IP = entryPcIp.get()
UDP_PORT = int(entryPcPort.get())
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))
print("ready to receive messages")


def print_time(arg):
    while True:
        data, addr = sock.recvfrom(512)  # buffer size is 1024 bytes
        print()
        var = "received message:", data
        print(var)
        #labelFeedback.config(text=var)


try:
    t = thread.start_new_thread(print_time, ("ThreadReceiveData",))
except:
    print "Error: unable to start thread"


root.mainloop()
