from socket import *
from time import sleep
import struct

while True:
    try:
        client_socket = socket(AF_INET, SOCK_DGRAM)
        # data = "\\x9a\\x99\\x99\\x99\\x99\\x99\\xf1?"#1.1   \\x00\\x00\\x00\\x00\\x00\\x00&@"  # 11

        # data = "\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?" \
        #        "\\x00\\" \
        #        "x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x" \
        #        "00\\x00\\x00\\x00\\x00@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x9a\\x99\\x99\\x99\\x99\\x99" \
        #        "\\xd9?\\x00\\x00\\x00\\x00\\x00\\x00\\xe0?\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00"
        # 10 x8 bytes for persona[from end to start of bytes] -> 0[id] 1 0 0.4 3.0 2.0 0 0 1 1

        # data = "\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?\\x00\\x00\\x00\\x00\\x00\\x00\\xe0?" \
        #        "\\x00\\" \
        #        "x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x" \
        #        "00\\x00\\x00\\x00\\x00@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x9a\\x99\\x99\\x99\\x99\\x99" \
        #        "\\xd9?\\x00\\x00\\x00\\x00\\x00\\x00\\xe0?\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?"
        # 10 x8 bytes for basic profile[from end to start of bytes] -> 1[id] 1 0.5 0.4 3.0 2.0 0 0 0.5 1

        # client_socket.sendto(struct.pack('!qqqqqqqqqq',
        #                                  0x0000000000000840,
        #                                  0x000000000000f03f,
        #                                  0x000000000000e03f,
        #                                  0x0000000000001040,
        #                                  0x0000000000000840,
        #                                  0x0000000000000040,
        #                                  0x0000000000000000,
        #                                  0x0000000000000000,
        #                                  0x000000000000e03f,
        #                                  0x000000000000f03f
        #                                  ),
        #                      ("192.168.3.74", 4500))  # 3f <=> ?

        # send 993 package to activate snow profile
        # client_socket.sendto(struct.pack('!qqqqq',
        #                                  0x0000000000088f40,
        #                                  0x000000000000f03f,
        #                                  0x0000000000000000,
        #                                  0x000000000000f03f,
        #                                  0x0000000000000000
        #                                  ),
        #                      ("192.168.3.74", 4500)) #3f <=> ?

        # byte groups need to be reversed!
        # package 997 for lanechange[1]
        client_socket.sendto(struct.pack('!qq',
                                         0x0000000000288f40,
                                         0x000000000000f03f
                                         ),
                             ("192.168.3.74", 4500))

        # package id = 994 = \x00\x00\x00\x00\x00\x10\x8f@
        # client_socket.sendto(struct.pack('!qqqqq',
        #                                  0x0000000000108f40,
        #                                  0x000000000000f03f,
        #                                  0x000000000000f03f,
        #                                  0x0000000000000000,
        #                                  0x0000000000000000
        #                                  ),
        #                      ("192.168.3.165", 4500))

        # \x00\x00\x00\x00\x00\x00\x8f@ for package id 992
        # client_socket.sendto(struct.pack('!qqq',
        #                                  0x0000000000008f40,
        #                                  0x000000000000f03f,
        #                                  0x000000000001f03f
        #                                  ),
        #                      ("192.168.3.165", 4500))

        # \x00\x00\x00\x00\x00\x18\x8f@ for package id 995
        # client_socket.sendto(struct.pack('!qq',
        #                                  0x0000000000188f40,
        #                                  0x000000000000f03f#,
        #                                  #0x000000000000f03f #had not active
        #                                  ),
        #                      ("192.168.3.74", 4500))

        # "\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?" \
        #        "\\x00\\" \
        #        "x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x" \
        #        "00\\x00\\x00\\x00\\x00@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x9a\\x99\\x99\\x99\\x99\\x99" \
        #        "\\xd9?\\x00\\x00\\x00\\x00\\x00\\x00\\xe0?\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00"
        # 10 x8 bytes for persona[from end to start of bytes] -> 0[id] 1 0 0.4 1 3.0 2.0 0 0 1 1
        # client_socket.sendto(struct.pack('!qqqqqqqqqqq',
        #                                  0x0000000000000000,
        #                                  0x000000000000f03f,
        #                                  0x000000000000e03f,
        #                                  0x000000000000f03f,
        #                                  0x0000000000000040,
        #                                  0x0000000000000000,
        #                                  0x0000000000000000,
        #                                  0x0000000000000840,
        #                                  0x000000000000e03f,
        #                                  0x000000000000f03f,
        #                                  0x000000000000f03f
        #                                  ), ("192.168.3.74", 4500))

        # client_socket.sendto(struct.pack('!qq',
        #                                  0x0000000000108f40,
        #                                  0x0000000000000040
        #                                  ),
        #                      ("192.168.3.74", 4500))  # 3f <=> ?
        sleep(2)
    except Exception as msg:
        print(msg)
